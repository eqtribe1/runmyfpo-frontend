angular.module('fpoApp.controllers') // ,["checklist-model"])
    // .run(function ($rootScope, $templateCache) {
    //   $rootScope.$on('$viewContentLoaded', function () {
    //     $templateCache.removeAll();
    //   });
    // })
    .controller('FpoDetailsUpdateCtrl', ['$scope', '$stateParams',  '$filter', 'QueryService', 'MessageService',
        function($scope, $stateParams,  $filter, QueryService, MessageService) {
            
            console.log($stateParams.fpo_details);

            $("#date_incorporation").datetimepicker({
                showClear: true,
                keepOpen: false,
                focusOnShow: false,
                format: 'DD-MM-YYYY',
                ignoreReadonly: true,
                maxDate: moment(),
                minDate: new Date("01/01/1990")
            });
            
            //$scope.fpo_details_old = JSON.parse($stateParams.fpo_details);
            $scope.fpo_details = JSON.parse($stateParams.fpo_details);
            console.log($scope.fpo_details);
            var fpoId = $scope.fpo_details.id;

            $scope.fpo_details.ceo = $scope.fpo_details.ceoId;
            $('#date_incorporation').data("DateTimePicker").date(moment($scope.fpo_details.date_incorporation));
            
            //this API will fetch list of all MGMT-USERS from related fpo....
            QueryService.query('GET', '/fpos/' + fpoId + '/roles/' + "FPO-MANAGER").then(function(response) {
                if (response.status == 200) {
                    console.log(response.data);
                    $scope.managers = response.data;
                    angular.forEach($scope.managers, function(value, key) {
                        $scope.managers[key].nameWithEmail = value.personName + " ( " + value.personEmailId + " )";
                    })
                } else {
                    MessageService.error("Unable to fetch list.");
                }
            }, function(error) {
                MessageService.error(error.data);
            })

            $scope.update_details = function() {
                delete $scope.fpo_details.id;
                delete $scope.fpo_details.ceoId;

                $scope.fpo_details.date_incorporation = new Date(moment($('#date_incorporation').val(),"DD-MM-YYYY"));

                console.log($scope.fpo_details);
                QueryService.query('PUT', '/fpos/' + fpoId, {}, $scope.fpo_details).then(function(response) {
                    if (response.status == 200) {
                        //console.log(response);
                        window.history.back();
                    }
                }, function(error) {
                    MessageService.error(error.data);
                })
            }
            //on cancel button press....
            $scope.goBack = function() {
                window.history.back();
            }
            //maxDate for dateIncorporation in addFpo
            $scope.maxDate = new Date();
            return true;

        }
    ])
