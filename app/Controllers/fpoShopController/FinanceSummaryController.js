'use strict';
angular.module('fpoApp.controllers')
    .controller('FinanceSummaryCtrl', ['$scope', '$stateParams', 'QueryService', 'MessageService', finSummary]);

function finSummary($scope, $stateParams, QueryService, MessageService) {
     var fpoId;
            $scope.fpoId = fpoId = window.localStorage.getItem('fpoId');
    QueryService.query('GET', '/fpo/' + $scope.fpoId + '/fincatsummary/all').then(function(response) {
        $scope.finance = response.data;
        $scope.finSummary = ($scope.finance.finSummary ? $scope.finance.finSummary : {});
        $scope.category = response.data.category;
        $scope.membFarmers = _.find($scope.category, { category: "FPO-MEMB" });
        $scope.nonMembFarmers = _.find($scope.category, { category: "FPO-NONMEMB" });
        $scope.otherCustomers = _.find($scope.category, { category: "FPO-CUST" });
        $scope.fpoVendors = _.find($scope.category, { category: "FPO-VEND" });
        $scope.fpoStaff = _.find($scope.category, { category: "FPO-STF" });
    }, function(error) {
        console.error(error.data);
    });

    $scope.getExtraInfo = function(data) {
        QueryService.query('GET', '/fpo/' + $scope.fpoId + '/finsummary/receibables').then(function(response) {
            // $scope[data.type] =   
        }, function(error) {
            console.error(error.data);
        });
    }
};
