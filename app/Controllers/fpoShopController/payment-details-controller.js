angular.module('fpoApp.controllers')
    .controller('paymentDetailsCtrl', ['$scope', '$state', '$mdDialog', '$rootScope', '$mdMedia', '$stateParams', 'fileUpload', 'QueryService', 'MessageService',
        function($scope, $state, $mdDialog, $rootScope, $mdMedia, $stateParams, fileUpload, QueryService, MessageService) {
            $scope.fpoId = $stateParams.fpoId;
            $scope.payInput = {};
            $scope.purchaseId = $stateParams.purchaseId;
            $scope.id = $stateParams.id;
            $scope.getFindata  = function(data){
                QueryService.query('GET', '/fpo/' + $scope.fpoId + '/finaccount/'+ data.type+'/' + data.id , {}, {})
                    .then(function(response) {
                        if (response.data) {
                            $scope.finAccount = (response.data.payable_by_biz - response.data.receivable_by_biz);
                            $scope.sellerAccountBalance = $scope.total_amount + $scope.finAccount;
                        } else {
                            console.error(response.data);
                        }
                    });
            }
            if ($stateParams.type == 'input') {
                $scope.vendorData = JSON.parse(window.sessionStorage.getItem('vendorData'));
                $scope.getFindata({type:'input',id:$scope.id,errMsg:'Unable to retrieve the farmer info!'});
                //Get the farmer findata

            }
            if ($stateParams.type == 'output') {
                $scope.farmerData = JSON.parse(window.sessionStorage.getItem('farmerData'));
                //Get the vendor findata
                $scope.getFindata({type:'output',id:$scope.id,errMsg:'Unable to retrieve the farmer info!'});
            }
            $scope.purchase = JSON.parse(window.sessionStorage.getItem('fpoPurchaseDetails') ? window.sessionStorage.getItem('fpoPurchaseDetails') : '{}');
            $scope.total_amount = (($scope.purchase && (typeof($scope.purchase) == 'object')) ? $scope.purchase.total_amount : 0);
            $scope.sellerAccountBalance = $scope.total_amount;
            $scope.accountId = 1;

            $scope.createPayment = function() {
                // calculate remaning:
                $scope.payInput.amount_paid = (($scope.payInput.cash || 0) + ($scope.payInput.check || 0) + ($scope.payInput.bank_txn || 0));
                $scope.payInput.balance = $scope.total_amount - $scope.payInput.amount_paid;
                $scope.payInput.posting_date = $scope.payInput.date = $scope.purchase.purchase_date;
                $scope.payInput.amount = $scope.total_amount;
                $scope.payInput.id = $scope.id;
                QueryService.query('POST', '/fpo/' + $scope.fpoId + '/purchase/' + $scope.purchaseId + "/" + $stateParams.type + '/payment', {}, $scope.payInput)
                    .then(function(response) {
                        $state.go('list-purchases');
                    });
            };

            //=====================date obj ===========================

            $scope.$watch('payInput.check', function(newValue, oldValue) {
                if (newValue > 0) {
                    $("#checkDate").datetimepicker({
                        showClear: true,
                        keepOpen: false,
                        focusOnShow: false,
                        defaultDate: new Date(),
                        format: 'DD-MM-YYYY',
                        ignoreReadonly: true,
                        minDate: new Date()
                    });
                    $scope.payInput.check_date = $('#checkDate').val();
                }
            });
            $("#balanceDueDate").datetimepicker({
                showClear: true,
                keepOpen: false,
                focusOnShow: false,
                defaultDate: new Date(),
                format: 'DD-MM-YYYY',
                ignoreReadonly: true,
                minDate: new Date()
            });
            $scope.payInput.balance_due_date = $('#balanceDueDate').val();

        }
    ]);
