'use strict';
angular.module('fpoApp.controllers')
    .controller('RecordGoodsReceivedCtrl', ['$scope', '$state', '$rootScope', '$mdMedia', '$mdDialog', '$stateParams', 'QueryService', 'MessageService', 'DialogBoxService',
        function($scope, $state, $rootScope, $mdMedia, $mdDialog, $stateParams, QueryService, MessageService, DialogBoxService) {
            var purchaseId = $stateParams.purchaseId;
            var fpoId = window.localStorage.getItem('fpoId');
            console.log(purchaseId);
            $scope.good_received_items = {};
            $rootScope.receivedItems = [];

            $("#receipt_date").datetimepicker({
                showClear: true,
                keepOpen: false,
                focusOnShow: false,
                defaultDate: new Date(),
                format: 'DD-MM-YYYY',
                ignoreReadonly: true,
                minDate: new Date()

            });

            $scope.field_staff_users = [];
            var rejectedItemStatus = [];

            // get the list of field staff......
            QueryService.query('GET', '/fpos/' + fpoId + '/roles/' + "FPO-FIELD-STAFF", {}, {}).then(function(response) {
                console.log(response.data);
                if (response.status == 200) {
                    $scope.field_staff_users = response.data;
                } else {
                    MessageService.error("Error in fetching data");
                }
            }, function(error) {
                console.error(error.data);
            });

            //get all details of this purchaseId......
            QueryService.query('GET', '/fpo/' + fpoId + '/purchase/' + purchaseId + '/details', {}, {}).then(function(response) {
                if (response.status == 200) {
                    console.log(response.data);
                    $scope.purchaseDetails = response.data;
                    $scope.receivedItems = response.data.purchaseItems;
                    $scope.sellerDetails = response.data.seller[0];
                }
            }); //end of API response... 

            // add item received popup

            $scope.itemReceivedDetails = function(ev, receivedItem) {
                var dialogBoxData = {};
                dialogBoxData.controller = ItemReceivedController;
                dialogBoxData.templateUrl = '/views/fposhop/item_received_popup.html';
                dialogBoxData.item = receivedItem;
                dialogBoxData.ev = ev;
                dialogBoxData.body = document.body;

                DialogBoxService.open($scope, dialogBoxData, function(data) {
                    $scope.receivedItems = _.map($scope.receivedItems, function(param) {
                        (param.item.id == data.id) ? (param.receivedItems = data) : null;
                        return param;
                    });
                });
            };
            // end of add item popup


            function ItemReceivedController($scope, data) {
                $scope.good_received_items = {};
                $scope.good_received_items.name = data.item.name;
                $scope.good_received_items.id = data.item.id;
                // console.log(data);
                $scope.answer = function(good_received_items) {
                    _.merge($scope.good_received_items, good_received_items);
                    $mdDialog.hide($scope.good_received_items);
                    // return rejectedItemStatus.push($scope.good_received_items);
                }
                 $scope.getCalendar = function(){
                $("#delivery_date").datetimepicker({
                showClear: false,
                keepOpen: true,
                focusOnShow: true,
                defaultDate: new Date(),
                format: 'DD-MM-YYYY',
                ignoreReadonly: true,
                minDate: new Date()
            });
                 }
                $scope.goBack = function() {
                    $mdDialog.cancel();
                };
                 
            };

            // add item rejected popup

            $scope.itemRejectedDetails = function(ev, receivedItem) {
                var dialogBoxData = {};
                dialogBoxData.controller = ItemRejectedController;
                dialogBoxData.templateUrl = '/views/fposhop/item_rejected_popup.html';
                dialogBoxData.item = receivedItem;
                dialogBoxData.ev = ev;
                dialogBoxData.itemStatus = $scope.itemStatus;
                dialogBoxData.body = document.body;

                DialogBoxService.open($scope, dialogBoxData, function(data) {
                    $scope.receivedItems = _.map($scope.receivedItems, function(param) {
                        (param.item.id == data.id) ? (param.rejectedItems = data) : null;
                        return param;
                    });
                });
            };
            // end of add item popup

            // end of select vendor popup

            function ItemRejectedController($scope, data,itemStatus) {
                $scope.itemStatus = itemStatus;
                $scope.good_received_items = {};
                $scope.good_received_items.name = data.item.name;
                $scope.good_received_items.id = data.item.id;
                // console.log(data);
                $scope.answer = function(good_received_items) {
                    _.merge($scope.good_received_items, good_received_items);
                    $mdDialog.hide($scope.good_received_items);
                    // return rejectedItemStatus.push($scope.good_received_items);
                }
                $scope.goBack = function() {
                    $mdDialog.cancel();
                };
            };

            // get the list of item status......
            QueryService.query('GET', '/purchase/goods_received/status', {}, {}).then(function(response) {
                if (response.status == 200) {
                    console.log(response.data);
                    $scope.itemStatus = response.data;
                } else {
                    console.error("Error in fetching data");
                }
            }, function(error) {
                console.error(error.data);
            });

            //on button click event call function and post api to send received items details...
            
            $scope.addGoodsReceieved = function() {
                // Todo: Need to validate the input before sending to the API
                $scope.good_received_items.receipt_date = new Date(moment($('#receipt_date').val(), "DD-MM-YYYY"));
                $scope.good_received_items.purchaseItems = [];
                $scope.good_received_items.rejectedItems = [];

                _.each($scope.receivedItems, function(purchaseItems) {
                    var selectedItems = _.clone(purchaseItems);
                    if (purchaseItems.receivedItems || purchaseItems.quantity > 0) {
                        selectedItems.status = "OK";
                        $scope.good_received_items.purchaseItems.push(selectedItems);
                    };
                    if (purchaseItems.rejectedItems || purchaseItems.rejectedQuantity > 0) {
                        selectedItems.status = purchaseItems.status || "RETURNED";
                        $scope.good_received_items.rejectedItems.push(selectedItems);
                    };
                });

                QueryService.query('POST', '/fpo/' + fpoId + '/purchases/' + purchaseId + '/received', {}, $scope.good_received_items)
                    .then(function(response) {
                        console.log(response);
                        if (response.status == 200) {
                            $state.go('shop-dashboard');
                        }
                    }, function(error) {
                        MessageService.error(error.data);
                    });
            }
        }
    ])
