'use strict';
angular.module('fpoApp.controllers')
    .controller('CurrentStockLevelCtrl', ['$scope', '$rootScope', '$stateParams', 'QueryService', 'MessageService', currentStockLeve]);

function currentStockLeve($scope, $rootScope, $stateParams, QueryService, MessageService) {
    var logError = $rootScope.logError;
    $scope.fpoId = $stateParams.fpoId;
    GetStock();

    QueryService.query('GET', '/product/item/category').then(function(response) {
        $scope.product_categories = response.data;
    }, logError);

    function GetStock(category) {
        QueryService.query('GET', '/fpo/' + $scope.fpoId + '/stock' + (category && category.name ? ("?category=" + category.name) : ""), {}, {})
            .then(function(response) {
                $scope.selected_product_categories = response.data;
            }, logError)
    };

    $scope.onCategorySelect = function(category) {
        $scope.categoryData = _.clone(category);
        GetStock(category);
    };
};
