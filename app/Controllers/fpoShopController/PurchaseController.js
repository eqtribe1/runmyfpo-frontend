angular.module('fpoApp.controllers')
    .controller('PurchaseCtrl', ['$scope', '$state', '$mdDialog', '$rootScope', '$mdMedia', '$stateParams', 'fileUpload', 'QueryService', 'MessageService',
        function($scope, $state, $mdDialog, $rootScope, $mdMedia, $stateParams, fileUpload, QueryService, MessageService) {
            'use strict'

            
            $("#purchase_date").datetimepicker({
                showClear: true,
                keepOpen: false,
                focusOnShow: false,
                defaultDate: new Date(),
                format: 'DD-MM-YYYY',
                ignoreReadonly: true,
                maxDate: new Date()

            });
            $('#purchase_date').val(moment(new Date()).format("DD-MM-YYYY"));
             
             // function for calling calendar inside accordion //
             $scope.getCalendar = function(){
                $("#delivery_date").datetimepicker({
                showClear: false,
                keepOpen: true,
                focusOnShow: true,
                defaultDate: new Date(),
                format: 'DD-MM-YYYY',
                ignoreReadonly: true,
                minDate: new Date()
            });

            };

            $rootScope.purchaseItems = [];
            $scope.record_farmer = {};
            $scope.record_vendor = {};
            $scope.input_record_params = {};

            var farmerId = $stateParams.farmerId;
            // $scope.ProduceFromFarmer=true;
            $scope.userName = window.localStorage.getItem('userName');
            $scope.personId = $stateParams.personId || window.localStorage.getItem('personId');
            var userId = $rootScope.loginId;
            // console.log($rootScope.loginId);
            var fpoId;
            $scope.fpoId = fpoId = window.localStorage.getItem('fpoId');
            $scope.fpoName = window.localStorage.getItem('fpoName');
            console.log($scope.fpoId);

            $scope.type = [];
            $scope.input_type = {};
            $scope.field_staff_users = [];


            QueryService.query('GET', '/fpos/' + fpoId + '/farmers', {}, {})
                .then(function(response) {
                    if (response.status == 200) {
                        console.log(response.data);
                        $scope.member_farmers = response.data;
                    }
                }, function(error) {
                    console.error(error.data);
                });

            QueryService.query('GET', '/fpos/' + fpoId + '/roles/' + "FPO-FIELD-STAFF", {}, {}).then(function(response) {
                console.log(response.data);
                if (response.status == 200) {
                    $scope.field_staff_users = response.data;
                } else {
                    MessageService.error("Error in fetching data");
                }
            }, function(error) {
                console.error(error.data);
            });

            $scope.recordPurchase = true;
            $scope.organization = [];
            $scope.person = [];

            $scope.searchVendor = function(type) {
                QueryService.query('GET', '/fpo/' + fpoId + '/vendor/list' + ((type = 'recent') ? "?recent=true" : ""), {}, {})
                    .then(function(response) {
                        console.log(response.data);
                        if (response.status == 200) {
                            var data = _.flatten([response.data.person, response.data.organization]);

                            $scope.vendor_infos = _.map(response.data, function(per) {
                                per.name = per.name + (per.short_address ? (" (" + per.short_address + ")") : "");
                                return per;
                            });
                        } else {
                            MessageService.error("Error in fetching data.");
                        }
                    }, function(error) {
                        MessageService.error(error.data);
                    });

                // selecting phones from dropdown //
                $scope.addSelectedVendor = function(data) {
                    $scope.vendorData = _.clone(data);
                    window.sessionStorage.setItem('vendorData', JSON.stringify(data));
                    // $scope.vendor_infos.id
                }
            };

            /**
             * First Time call searchVendor with recent
             */
            $scope.searchVendor('recent');


            // add item popup
            $scope.status = '  ';
            $scope.customFullscreen = $mdMedia('xs') || $mdMedia('sm');
            $scope.inputItemPopup = function(ev) {
                var useFullScreen = ($mdMedia('xs') || $mdMedia('xs')) && $scope.customFullscreen;

                $mdDialog.show({
                    controller: InputItemController,
                    templateUrl: 'views/fposhop/add_item.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose: true,
                    fullscreen: useFullScreen
                })

                $scope.$watch(function() {
                    return $mdMedia('xs') || $mdMedia('sm');
                }, function(wantsFullScreen) {
                    $scope.customFullscreen = (wantsFullScreen === true);
                });

            };
            // end of add item popup

            // end of select vendor popup
            function InputItemController($scope, $mdDialog) {
                $scope.hide = function() {
                    $mdDialog.hide();
                };
                $scope.input_record_params = { discount: {} };

                $scope.input_record_params.type = "INPUT";

                $scope.initItems = function(extParam) {
                    QueryService.query('GET', '/FPO/' + fpoId + '/purchase/' + $scope.input_record_params.type + '/items' + (extParam ? ("?" + extParam) : ''), {}, {})
                        .then(function(response) {
                            if (response.status == 200) {
                                $scope.items = response.data;
                            } else {
                                MessageService.error("Error in fetching data.");
                            }
                        }, function(error) {
                            MessageService.error(error.data);
                        });
                };

                $scope.initItems();


                $scope.goBack = function() {
                    $mdDialog.cancel();
                };

                $scope.searchItem = function(itemName) {
                    var data = _.find($scope.items, function(item) {
                        if (item.name && itemName) {
                            return item.name.toLowerCase() == itemName.toLowerCase();
                        } else {
                            return false;
                        }
                    });
                    if (!data) {
                        $scope.initItems(itemName ? ("name=" + itemName) : "all=true");
                    };
                };
                //  slecting items from dropdown  //
                $scope.selectedItem = {};
                $scope.addSelectedItemPurchase = function(item) {
                    $scope.input_record_params.rate = item.attributes.MRP || 0;
                    $scope.input_record_params.item = item;
                    $scope.input_record_params.name = _.clone(item.name);
                    $scope.selectedItem.WEIGHT = item.attributes.WEIGHT;
                    $scope.selectedItem.UNIT = item.attributes.UNIT || "KG";
                    $scope.selectedItem.MRP = item.attributes.MRP;
                }



                // closing item selection popup //
                $scope.answer = function(item) {

                    if ($rootScope.purchaseItems.length > 0) {
                        var isAlreadyExist = $rootScope.purchaseItems.every(function(purchItem) {
                            console.log($rootScope.purchaseItems.length);
                            return purchItem.item.id == item.item.id;
                        });
                        if (!isAlreadyExist) {
                            $rootScope.purchaseItems.push(item);
                        }
                    } else {
                        $rootScope.purchaseItems.push(item);
                    }
                    $mdDialog.hide();

                };
                // // $scope.input_record_params.amount = ($scope.input_record_params.quantity * $scope.input_record_params.rate) - (($scope.input_record_params.rate * $scope.input_record_params.discount.percentage) / 100)
                // $scope.input_record_params.discount.amount = (($scope.input_record_params.discount.percentage * ($scope.input_record_params.rate))/100 );
            }; // End DialogController
            $scope.removeRow = function(productIndex) {
                $scope.purchaseItems.splice(productIndex, 1);
            }



            //Toatl amount calculation

            $scope.invoiceCount = 0;
            $scope.invoiceTotal = 0;
            $scope.setTotals = function(input_record_params) {
                if (input_record_params) {
                    if (input_record_params.discount.percentage) {
                        input_record_params.total = ((input_record_params.quantity * input_record_params.rate) - ((input_record_params.rate * input_record_params.discount.percentage) / 100));
                    } else if (input_record_params.discount.amount) {
                        input_record_params.total = ((input_record_params.quantity * input_record_params.rate) - input_record_params.discount.amount);
                    } else {
                        input_record_params.total = (input_record_params.quantity * input_record_params.rate);
                    }
                    $scope.invoiceCount += input_record_params.quantity;
                    $scope.invoiceTotal += input_record_params.total;
                }
            }

            //End of Total amount calculation

            // add purchase record 
            $scope.addRecordPurchase = function() {

                if ($rootScope.purchaseItems.length > 0 && $scope.invoiceTotal > 0) {
                    $scope.input_record_params.type = "INPUT";
                    $scope.input_record_params.purchase_date = new Date(moment($('#purchase_date').val(), "DD-MM-YYYY"));
                    //$scope.input_record_params.purchase_date = $('#purchase_date').val(moment(new Date()).format("DD-MM-YYYY"));
                    $scope.input_record_params.delivery_date = new Date(moment($('#delivery_date').val(), "DD-MM-YYYY"));
                    $scope.input_record_params.seller_entity_type = "VENDOR";
                    $scope.input_record_params.seller_entity_domain = "VENDOR";
                    $scope.input_record_params.seller_entity_id = JSON.parse(sessionStorage.vendorData).vendor_id;
                    $scope.input_record_params.total_amount = $scope.invoiceTotal;
                    console.log($scope.input_record_params);
                    $scope.input_record_params.purchase_items = _.map($rootScope.purchaseItems, function(purchaseItem) {
                        var d = _.clone(purchaseItem);
                        d.id = d.item.id;
                        d.amount = d.total;
                        d.unit = d.item.attributes.UNIT;
                        if (d.unit == null) {
                            d.unit = "KG";
                        }
                        delete d.total;
                        delete d.item;
                        delete d.name;
                        delete d.itemName;
                        return d;
                    });
                    QueryService.query('POST', '/fpo/' + fpoId + '/purchase/' + $scope.input_record_params.type, {}, $scope.input_record_params)
                        .then(function(response) {
                            if (response.status == 200) {
                                window.sessionStorage.setItem('fpoPurchaseDetails', JSON.stringify($scope.input_record_params));
                                $state.go('paymentDetails', { purchaseId: response.data[0].purchase_rec, type: 'input', fpoId: fpoId, id: $scope.input_record_params.seller_entity_id });
                            }
                        }, function(error) {
                            MessageService.error(error.data);
                        });
                } else {
                    if (!$rootScope.purchaseItems.length > 0) {
                        MessageService.info("Please select an item!")
                    } else
                    if (!$scope.invoiceTotal > 0) {
                        MessageService.info("Invoce amount should be grater then Zero!")
                    }
                }
            }

            ///////////////////// Image upload ///////////////////////

            $scope.headerText = "Update profile pic of " + localStorage.getItem('farmerName');
            var personId = $stateParams.personId;
            // $scope.fileUrl = ;

            $scope.uploadFile = function(file) {
                console.log(file);

                var APIUrl = "/images/upload"

                if (file) {
                    fileUpload.uploadFile(file, 'image', APIUrl, function(response) {
                        var url = response.data.url;
                        console.log(url);
                        if (!url) {
                            MessageService.info('Unable to Uploaded Image. Please try again!');
                        } else {
                            $scope.input_record_params.invoice_image = url;
                            MessageService.success('Image Uploaded successfully');
                        }
                    })
                } else {
                    MessageService.info("Please select the file!");
                }
            }

            $scope.photoChanged = function(files) {
                fileUpload.changeFile($scope, files);
            };

            $scope.saveFile = function() {
                if ($scope.selectedFile) {
                    $scope.uploadFile($scope.selectedFile);
                } else {
                    MessageService.info("Please select a file!");
                };
            };

            $scope.removeFile = function(file) {
                $scope.thumbnail.dataUrl = "http://fakeimg.pl/250x100/CCC/"
            };
            $scope.removePreviousProfilePic = function(url) {
                var apiUrl = "/file/delete/";
                fileUpload.removeFile(apiUrl, url, 'PUT', function(res) {
                    if (response.status == 200) {
                        MessageService.success("File Deleted successfully");
                    };
                });
            };


            $scope.addVendor = function() {
                $state.go('addVendor', { fpoId: fpoId });
            }

            $scope.outputRecord = function() {
                $state.go('outputRecord');
            }


            //////////////////////////////////////////////////////////////////////////

            var fpoId = window.localStorage.getItem('fpoId');
            var purchaseId = $stateParams.purchaseId;
            $scope.purchaseId = purchaseId;
            $scope.record_params = {};
            //    $rootScope.purchaseItems = [];

            $scope.record_params.purchase_items = _.map($rootScope.purchaseItems, function(purchaseItem) {
                var d = _.clone(purchaseItem);
                d.id = d.item.id;
                d.amount = d.total;
                d.unit = d.item.attributes.UNIT;
                if (d.unit == null) {
                    d.unit = "KG";
                }
                delete d.total;
                delete d.item;
                delete d.name;
                delete d.itemName;
                return d;
            });

            // $scope.updateRecord = function () {
            //     $scope.purchaseItems = JSON.parse(window.localStorage.getItem('record_params'));
            // }

            // add item popup
            $scope.invoiceTotal = 0;
            $scope.status = '  ';
            $scope.customFullscreen = $mdMedia('xs') || $mdMedia('sm');
            $scope.itemPopup = function(ev, purchaseItems) {
                var useFullScreen = ($mdMedia('xs') || $mdMedia('xs')) && $scope.customFullscreen;
                // $rootScope.recordUpdate = record_param;
                window.sessionStorage.setItem('recordUpdate', JSON.stringify(purchaseItems));
                console.log(purchaseItems);
                // $scope.record_param = _.filter($scope.record_param,function(param){
                //     return param.item.id != record_param.item.id;
                // });
                $mdDialog.show({
                    controller: itemController,
                    templateUrl: 'views/fposhop/update_input_item_popup.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose: true,
                    fullscreen: useFullScreen
                }).then(function(data) {


                    $scope.purchaseItems = _.map($scope.purchaseItems, function(param) {
                        if (param.item.id == data.item.id) {
                            $scope.invoiceTotal += data.total;
                            return data;
                        } else {
                            return param;
                        };
                    });
                });

                $scope.$watch(function() {
                    return $mdMedia('xs') || $mdMedia('sm');
                }, function(wantsFullScreen) {
                    $scope.customFullscreen = (wantsFullScreen === true);
                });
            };
            // $scope.itemPopup 
            // end of add item popup

            // end of select vendor popup
            function itemController($scope, $mdDialog) {

                //console.log( $rootScope.record_params.item.name);
                //$scope.fpos = JSON.parse(window.localStorage.getItem('fpos'));
                $scope.input_record_params = JSON.parse(window.sessionStorage.getItem('recordUpdate'));
                console.log($scope.input_record_params);
                $scope.input_record_params.name = $scope.input_record_params.item.name;
                console.log($scope.input_record_params.item);
                $scope.hide = function() {
                    // $mdDialog.hide();
                };

                //$scope.record_params = { discount: {} };

                $scope.input_record_params.type = "INPUT";

                QueryService.query('GET', '/FPO/' + fpoId + '/purchase/' + $scope.input_record_params.type + '/items', {}, {})
                    .then(function(response) {
                        // console.log(response.data);
                        if (response.status == 200) {
                            $scope.items = response.data;
                        } else {
                            MessageService.error("Error in fetching data.");
                        }
                    }, function(error) {
                        MessageService.error(error.data);
                    });

                $scope.goBack = function() {
                    $mdDialog.cancel();
                };


                //  slecting items from dropdown  //
                $scope.selectedItem = {};
                $scope.addSelectedItemPurchase = function(item) {
                    console.log(item);
                    $scope.record_params.rate = item.attributes.MRP || 0;
                    $scope.record_params.item = item;
                    $scope.record_params.name = _.clone(item.name);
                    $scope.selectedItem.WEIGHT = item.attributes.WEIGHT;
                    $scope.selectedItem.UNIT = item.attributes.UNIT || 'KG';
                    $scope.selectedItem.MRP = item.attributes.MRP;
                }

                // closing item selection popup //
                $scope.answer = function(input_record_params) {
                    $mdDialog.hide(input_record_params);
                };
            };
            // End DialogController
            $scope.removeRow = function(productIndex) {
                $scope.purchaseItems.splice(productIndex, 1);
            }

        }
    ]);
