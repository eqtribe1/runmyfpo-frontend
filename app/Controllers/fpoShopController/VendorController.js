angular.module('fpoApp.controllers')

.controller('AddVendorCtrl', ['$scope','$state','QueryService','MessageService',
  function ($scope,$state,QueryService,MessageService) {

      $scope.fpoName = window.localStorage.getItem('fpoName');
      var fpoId = window.localStorage.getItem('fpoId');
      $scope.vendorType = true;
      $scope.add_vendor = {};
      $scope.addVendorAddress = function () {
          var fpoId = window.localStorage.getItem('fpoId');

          if ($scope.vendorType) {
              $scope.add_vendor.type = "PERSON";
              QueryService.query('POST', '/fpo/' + fpoId + '/vendor', {}, $scope.add_vendor)
                  .then(function (response) {
                      window.localStorage.setItem('vendorName', $scope.add_vendor.name);
                      console.log('add_vendor');
                      if (response.status == 201) {
                          // window.localStorage.setItem('vendorId',add_vendor.id);
                          console.log(response.data);

                          var vendorId = response.data.id;
                          $state.go('addVendorAddress', { vendorId: vendorId });
                      } else {
                          MessageService.error(JSON.stringify(response.data));
                          $state.go('recordPurchase');
                      }
                  }, function (error) {
                      if (error.status == -1) {
                          MessageService.error("Error in saving data...");
                      } else {
                          MessageService.error(error.data);
                      }
                  })
          }
          else {
              $scope.add_vendor.type = "ORGANIZATION";
              QueryService.query('POST', '/fpo/' + fpoId + '/vendor', {}, $scope.add_vendor)
                  .then(function (response) {
                      console.log('add_vendor');
                      window.localStorage.setItem('vendorName', $scope.add_vendor.name);
                      if (response.status == 201) {
                          var vendorId = response.data.id;
                          $state.go('addVendorAddress', { vendorId: vendorId });
                      } else {
                          MessageService.error(JSON.stringify(response.data));
                          $state.go('recordPurchase');
                      }
                  }, function (error) {
                      if (error.status == -1) {
                          MessageService.error("Error in saving data...");
                      } else {
                          MessageService.error(error.data);
                      }
                  })
          }
      }
    }])