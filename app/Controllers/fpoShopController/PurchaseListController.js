angular.module('fpoApp.controllers')
    .controller('PurchaseListCtrl', ['$scope', '$state', '$stateParams', 'QueryService', 'MessageService',
        function($scope, $state, $stateParams, QueryService, MessageService) {

            var fpoId = window.localStorage.getItem('fpoId');

            //call API to get all purchase products list.....
            QueryService.query('GET', '/fpo/' + fpoId + '/purchases/list_purchase', {}, {}).then(function(response) {
                if (response.status == 200) {
                    console.log(response.data);
                    $scope.purchases_list = response.data;
                    //console.log($scope.purchases_list); 
                    $scope.new_purchase_list = _.clone($scope.purchases_list);
                    $scope.new_purchase_list.length = 7;
                    $scope.moreDetails = function(){
                        $scope.new_purchase_list = _.clone($scope.purchases_list);
                    }

                }
            }); //end of API response.... 
            $scope.purchaseDetails = function(purchase) {
                    var purchaseId = purchase.purchaseId;
                    $state.go('purchaseDetails', { purchaseId: purchaseId });
                }
                // window.localStorage.setItem('purchaseId',purchase_list.id);
        }
    ])
    .controller('PurchaseDetailsViewCtrl', ['$scope', '$state', '$mdMedia', '$rootScope', '$mdDialog', '$stateParams', 'QueryService', 'MessageService',
        function($scope, $state, $mdMedia, $rootScope, $mdDialog, $stateParams, QueryService, MessageService) {
            var fpoId = window.localStorage.getItem('fpoId');
            var purchaseId = $stateParams.purchaseId;
            $scope.purchaseId = purchaseId;
            $scope.record_params = {};
            $rootScope.record_param = [];
            $scope.invoiceTotal = 0;
            $scope.record_params.purchase_items = _.map($rootScope.record_param, function(purchaseItem) {
                var d = _.clone(purchaseItem);
                d.id = d.item.id;
                // d.amount = d.total;
                d.amount = ((d.quantity == 0 ? 1 : d.quantity) * d.rate);
                d.unit = d.item.attributes.UNIT;
                if (d.unit == null) {
                    d.unit = "KG";
                }
                delete d.total;
                delete d.item;
                delete d.name;
                delete d.itemName;
                return d;
            });

            $scope.updateRecord = function() {
                    $scope.record_param = JSON.parse(window.localStorage.getItem('record_params'));
                }
                //  call API to get all purchase products list.....

            QueryService.query('GET', '/FPO/' + fpoId + '/purchase/' + purchaseId + '/details', {}, {}).then(function(response) {
                if (response.status == 200) {
                    console.log(response.data);
                    $scope.purchaseDetails = response.data;
                    $scope.purchaseByDetail = response.data.purchased_by;
                    if ($scope.purchaseByDetail) {
                        $scope.purchaseByDetail = _.merge($scope.purchaseByDetail, $scope.purchaseByDetail.personal_details);
                        $scope.purchaserName = $scope.purchaseByDetail.fname + ($scope.purchaseByDetail.mname ? (" " + $scope.purchaseByDetail.mname) + " " : " ") + $scope.purchaseByDetail.lname;
                        window.localStorage.setItem('farmerName', $scope.vendorName);
                        window.localStorage.setItem('farmerProfilePic', ($scope.purchaseByDetail.avtar ? $scope.purchaseByDetail.avtar : ""));
                    }
                    $scope.sellerDetails = response.data.seller[0];
                    $scope.record_param = response.data.purchaseItems;
                }

            }, function(error) {
                MessageService.error(JSON.stringify(error.data));
            });
            //end of API response.... 


            $scope.record_param = $rootScope.record_param;

            // add item popup
            $scope.status = '  ';
            $scope.customFullscreen = $mdMedia('xs') || $mdMedia('sm');
            $scope.itemPopup = function(ev, record_param) {
                var useFullScreen = ($mdMedia('xs') || $mdMedia('xs')) && $scope.customFullscreen;
                // $rootScope.recordUpdate = record_param;
                window.localStorage.setItem('recordUpdate', JSON.stringify(record_param));
                // $scope.record_param = _.filter($scope.record_param,function(param){
                //     return param.item.id != record_param.item.id;
                // });
                $mdDialog.show({
                    controller: itemController,
                    templateUrl: '/views/fposhop/update_item.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose: true,
                    fullscreen: useFullScreen
                }).then(function(data) {

                    $scope.record_param = _.map($scope.record_param, function(param) {
                        if (param.item.id == data.item.id) {
                            $scope.invoiceTotal += data.total;
                            return data;
                        } else {
                            return param;
                        };
                    });
                });

                $scope.$watch(function() {
                    return $mdMedia('xs') || $mdMedia('sm');
                }, function(wantsFullScreen) {
                    $scope.customFullscreen = (wantsFullScreen === true);
                });
            };
            // $scope.itemPopup 
            // end of add item popup

            // end of select vendor popup
            function itemController($scope, $mdDialog) {

                //console.log( $rootScope.record_params.item.name);
                //$scope.fpos = JSON.parse(window.localStorage.getItem('fpos'));
                $scope.record_params = JSON.parse(window.localStorage.getItem('recordUpdate'));
                console.log($scope.record_params);
                $scope.record_params.name = $scope.record_params.item.name;
                console.log($scope.record_params.item.name);
                $scope.hide = function() {
                    // $mdDialog.hide();
                };

                //$scope.record_params = { discount: {} };

                $scope.record_params.type = "INPUT";

                QueryService.query('GET', '/FPO/' + fpoId + '/purchase/' + $scope.record_params.type + '/items', {}, {})
                    .then(function(response) {
                        // console.log(response.data);
                        if (response.status == 200) {
                            $scope.items = response.data;
                        } else {
                            MessageService.error("Error in fetching data.");
                        }
                    }, function(error) {
                        MessageService.error(error.data);
                    });

                $scope.goBack = function() {
                    $mdDialog.cancel();
                };


                //  slecting items from dropdown  //
                $scope.selectedItem = {};
                $scope.addSelectedItemPurchase = function(item) {
                    console.log(item);
                    $scope.record_params.rate = item.attributes.MRP || 0;
                    $scope.record_params.item = item;
                    $scope.record_params.name = _.clone(item.name);
                    $scope.selectedItem.WEIGHT = item.attributes.WEIGHT;
                    $scope.selectedItem.UNIT = item.attributes.UNIT;
                    if ($scope.selectedItem.UNIT == null) {
                        $scope.selectedItem.UNIT = "KG";
                    }
                    $scope.selectedItem.MRP = item.attributes.MRP;
                }



                // closing item selection popup //
                $scope.answer = function(record_params) {
                    $mdDialog.hide(record_params);
                };
            };
            // End DialogController
            $scope.removeRow = function(productIndex) {
                $scope.record_param.splice(productIndex, 1);
            }

            $scope.goodsReceived = function(purchaseDetails) {
                $state.go('purchased-goods-received', { purchase: JSON.stringify(purchaseDetails) });
            }


        }
    ])
