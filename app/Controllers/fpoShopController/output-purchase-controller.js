angular.module('fpoApp.controllers')
    .controller('outputPurchaseCtrl', ['$scope', '$state', '$mdDialog', '$rootScope', '$mdMedia', '$stateParams', 'fileUpload', 'QueryService', 'MessageService',
        function($scope, $state, $mdDialog, $rootScope, $mdMedia, $stateParams, fileUpload, QueryService, MessageService) {

            $("#purchase_date").datetimepicker({
                showClear: true,
                keepOpen: false,
                focusOnShow: false,
                defaultDate: new Date(),
                format: 'DD-MM-YYYY',
                ignoreReadonly: true,
                minDate: new Date(01 - 01 - 1990),
                maxDate: new Date()

            });
            $('#purchase_date').val(moment(new Date()).format("DD-MM-YYYY"));

            $rootScope.purchaseItems = [];
            $scope.output_record_params = {};

            var farmerId = $stateParams.farmerId;
            // $scope.ProduceFromFarmer=true;
            $scope.userName = window.localStorage.getItem('userName');
            $scope.personId = $stateParams.personId || window.localStorage.getItem('personId');
            var userId = $rootScope.loginId;
            // console.log($rootScope.loginId);
            var fpoId;
            $scope.fpoId = fpoId = window.localStorage.getItem('fpoId');
            $scope.fpoName = window.localStorage.getItem('fpoName');
            console.log($scope.fpoId);

            // $scope.type = [];
            $scope.field_staff_users = [];


            QueryService.query('GET', '/fpos/' + fpoId + '/farmers', {}, {})
                .then(function(response) {
                    if (response.status == 200) {
                        console.log(response.data);
                        $scope.farmer_infos = response.data;
                    }
                }, function(error) {
                    MessageService.error(error.data);
                });

            QueryService.query('GET', '/fpos/' + fpoId + '/roles/' + "FPO-FIELD-STAFF", {}, {}).then(function(response) {
                console.log(response.data);
                if (response.status == 200) {
                    $scope.field_staff_users = response.data;
                } else {
                    MessageService.error("Error in fetching data");
                }
            }, function(error) {
                MessageService.error(error.data);
            });

            $scope.recordPurchase = true;
            $scope.organization = [];
            $scope.person = [];
            QueryService.query('GET', '/fpo/' + fpoId + '/vendor/list?recent=true', {}, {})
                .then(function(response) {
                    console.log(response.data);
                    if (response.status == 200) {
                        $scope.farmer_infos = _.map(response.data, function(per) {
                            per.name = per.name + (per.memberShortAddress ? (" (" + per.memberShortAddress + ")") : "");
                            return per;
                        });
                        console.log($scope.person_phones, $scope.person_names, $scope.org_names)

                    } else {
                        MessageService.error("Error in fetching data.");
                    }
                }, function(error) {
                    MessageService.error(error.data);
                });

            // selecting phones from dropdown //
            $scope.addSelectedFarmer = function(data) {
                $scope.farmerData = _.clone(data);
                window.sessionStorage.setItem('FarmerData',JSON.stringify(data));                
                // $scope.vendor_infos.id
            }

            // $scope.searchFarmer = function() {
            //     QueryService.query('GET', '/fpo/' + fpoId + '/vendor/list', {}, {})
            //         .then(function(response) {
            //             console.log(response.data);
            //             if (response.status == 200) {
            //                 var data = _.flatten([response.data.person, response.data.organization]);

            //                 $scope.farmer_infos = _.map(response.data, function(per) {
            //                     per.name = per.name + (per.phone ? (" (" + per.phone + ")") : "");
            //                     return per;
            //                 });
            //                 console.log($scope.person_phones, $scope.person_names, $scope.org_names)

            //             } else {
            //                 MessageService.error("Error in fetching data.");
            //             }
            //         }, function(error) {
            //             MessageService.error(error.data);
            //         });

            //     // selecting phones from dropdown //
            //     $scope.addSelectedFarmer = function(data) {
            //         $scope.farmerData = data;
            //         // $scope.vendor_infos.id
            //     }
            // }

            // add item popup
            $scope.status = '  ';
            $scope.customFullscreen = $mdMedia('xs') || $mdMedia('sm');
            $scope.showAdvanced = function(ev) {
                var useFullScreen = ($mdMedia('xs') || $mdMedia('xs')) && $scope.customFullscreen;

                $mdDialog.show({
                    controller: DialogController,
                    templateUrl: 'views/fposhop/add_output_item.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose: true,
                    fullscreen: useFullScreen
                })

                $scope.$watch(function() {
                    return $mdMedia('xs') || $mdMedia('sm');
                }, function(wantsFullScreen) {
                    $scope.customFullscreen = (wantsFullScreen === true);
                });

            };
            // end of add item popup



            // add vendor popup
            //     $scope.status = '  ';
            //   $scope.customFullscreen = $mdMedia('xs') || $mdMedia('sm');
            //   $scope.showPopup = function(ev) {
            //     var useFullScreen = ($mdMedia('xs') || $mdMedia('xs'))  && $scope.customFullscreen;

            //     $mdDialog.show({
            //       controller: VendorController,
            //       templateUrl: 'views/fposhop/add_vendor.html',
            //       parent: angular.element(document.body),
            //       targetEvent: ev,
            //       clickOutsideToClose:true,
            //       fullscreen: useFullScreen
            //     })


            //    $scope.$watch(function() {
            //       return $mdMedia('xs') || $mdMedia('sm');
            //     }, function(wantsFullScreen) {
            //       $scope.customFullscreen = (wantsFullScreen === true);
            //     });


            //   };
            //   function VendorController($scope, $mdDialog) {
            //   $scope.hide = function() {
            //     $mdDialog.hide();
            //   };
            //              $scope.output_record_params = {};

            //  $scope.output_record_params.type = "output";

            //         QueryService.query('GET', '/FPO/'+fpoId+'/purchases/list?ty'+$scope.output_record_params.type,{},{})
            //             .then(function(response) {
            //               // console.log(response.data);
            //                 if (response.status == 200) {
            //                     $scope.items = response.data;

            //                 }
            //                 else {
            //                     MessageService.error("Error in fetching data.");
            //                 }
            //             }, function(error) {
            //                 MessageService.error(error.data);
            //             });




            //   $scope.cancel = function() {
            //     $mdDialog.cancel();
            //   };
            //   }


            // end of select vendor popup


            function DialogController($scope, $mdDialog) {
                $scope.hide = function() {
                    $mdDialog.hide();
                };
                $scope.output_record_params = { discount: {} };

                $scope.output_record_params.type = "OUTPUT";
                //     $scope.setTotal = function(output_record_params){
                //     if (output_record_params){
                //     if (output_record_params.discount.percentage){
                //     output_record_params.total = ((output_record_params.quantity * output_record_params.rate) - ((output_record_params.rate * output_record_params.discount.percentage) / 100));
                //     }
                //     else if (output_record_params.discount.amount){
                //     output_record_params.total = ((output_record_params.quantity * output_record_params.rate) - output_record_params.discount.amount);  
                //     }
                //     else{
                //         output_record_params.total = (output_record_params.quantity * output_record_params.rate);
                //     }
                //     }
                // }

                QueryService.query('GET', '/FPO/' + fpoId + '/purchase/' + $scope.output_record_params.type + '/items', {}, {})
                    .then(function(response) {
                        // console.log(response.data);
                        $scope.item = { attributes: {} };
                        if (response.status == 200) {
                            $scope.items = response.data;
                        } else {
                            MessageService.error("Error in fetching data.");
                        }
                    }, function(error) {
                        MessageService.error(error.data);
                    });



                $scope.goBack = function() {
                    $mdDialog.cancel();
                };


                //  slecting items from dropdown  //
                $scope.selectedItem = {};
                $scope.addSelectedItemPurchase = function(item) {
                    $scope.output_record_params.rate = item.attributes.MRP || 0;
                    $scope.output_record_params.item = item;
                    $scope.output_record_params.name = _.clone(item.name);
                    $scope.selectedItem.WEIGHT = item.attributes.WEIGHT;
                    $scope.selectedItem.UNIT = item.attributes.UNIT;
                    $scope.selectedItem.MRP = item.attributes.MRP;
                }



                // closing item selection popup //
                $scope.answer = function(item) {

                    if ($rootScope.purchaseItems.length > 0) {
                        var isAlreadyExist = $rootScope.purchaseItems.every(function(purchItem) {
                            console.log($rootScope.purchaseItems.length);
                            return purchItem.item.id == item.item.id;
                        });
                        if (!isAlreadyExist) {
                            $rootScope.purchaseItems.push(item);
                        }
                    } else {
                        $rootScope.purchaseItems.push(item);
                    }
                    $mdDialog.hide();

                };
                // $scope.output_record_params.amount = ($scope.output_record_params.quantity * $scope.output_record_params.rate) - (($scope.output_record_params.rate * $scope.output_record_params.discount.percentage) / 100)
                $scope.output_record_params.discount.amount = (($scope.output_record_params.discount.percentage * ($scope.output_record_params.rate)) / 100);
            }; // End DialogController


            // for removing a row from table //
            $scope.removeRow = function(productIndex) {
                $scope.purchaseItems.splice(productIndex, 1);
            }



            //Total amount calculation

            $scope.invoiceCount = 0;
            $scope.invoiceTotal = 0;
            $scope.setTotals = function(output_record_params) {
                if (output_record_params) {
                    if (output_record_params.discount.percentage) {
                        output_record_params.total = ((output_record_params.quantity * output_record_params.rate) - ((output_record_params.rate * output_record_params.discount.percentage) / 100));
                    } else if (output_record_params.discount.amount) {
                        output_record_params.total = ((output_record_params.quantity * output_record_params.rate) - output_record_params.discount.amount);
                    } else {
                        output_record_params.total = (output_record_params.quantity * output_record_params.rate);
                    }
                    $scope.invoiceCount += output_record_params.quantity;
                    $scope.invoiceTotal += output_record_params.total;
                }
            }

            //End of Total amount calculation

            // add purchase record 
            $scope.addOutputRecordPurchase = function() {
                $scope.output_record_params.type = "output";
                $scope.output_record_params.purchased_by = $rootScope.loginId;
                console.log($rootScope.loginId);
                $scope.output_record_params.purchase_date = new Date(moment($('#purchase_date').val(), "DD-MM-YYYY"));
                // $scope.output_record_params.expected_receipt_date = new Date(moment($('#delivery_date').val(), "DD-MM-YYYY"));
                $scope.output_record_params.seller_entity_type = "FARMER";
                $scope.output_record_params.seller_entity_domain = "FPO";
                $scope.output_record_params.seller_entity_id = $scope.farmerData.memberId;
                $scope.output_record_params.total_amount = $scope.invoiceTotal;
                console.log($scope.output_record_params);
                $scope.output_record_params.purchase_items = _.map($rootScope.purchaseItems, function(purchaseItem) {
                    var d = _.clone(purchaseItem);
                    d.id = d.item.id;
                    d.amount = d.total;
                    d.unit = d.item.attributes.UNIT;
                    delete d.total;
                    delete d.item;
                    delete d.name;
                    delete d.itemName;
                    return d;
                });
                QueryService.query('POST', '/fpo/' + fpoId + '/purchase/' + $scope.output_record_params.type, {}, $scope.output_record_params)
                    .then(function(response) {
                        if (response.status == 200) {
                            window.sessionStorage.setItem('fpoPurchaseDetails', JSON.stringify($scope.output_record_params));
                            $state.go('paymentDetails', {purchaseId:response.data[0].id,type:'output',fpoId:fpoId,id:$scope.output_record_params.seller_entity_id});
                        }
                    });
            }


            ///////////////////// Image upload ///////////////////////

            $scope.headerText = "Update profile pic of " + localStorage.getItem('farmerName');
            var personId = $stateParams.personId;
            // $scope.fileUrl = ;

            $scope.uploadFile = function(file) {
                console.log(file);

                var APIUrl = "/images/upload"

                if (file) {
                    fileUpload.uploadFile(file, 'image', APIUrl, function(response) {
                        var url = response.data.url;
                        console.log(url);
                        if (!url) {
                            MessageService.info('Unable to Uploaded Image. Please try again!');
                        } else {
                            $scope.output_record_params.invoice_image = url;
                            MessageService.success('Image Uploaded successfully');
                        }
                    })
                } else {
                    MessageService.info("Please select the file!");
                }
            }

            $scope.photoChanged = function(files) {
                fileUpload.changeFile($scope, files);
            };

            $scope.saveFile = function() {
                if ($scope.selectedFile) {
                    $scope.uploadFile($scope.selectedFile);
                } else {
                    MessageService.info("Please select a file!");
                };
            };

            $scope.removeFile = function(file) {
                $scope.thumbnail.dataUrl = "http://fakeimg.pl/250x100/CCC/"
            };
            $scope.removePreviousProfilePic = function(url) {
                var apiUrl = "/file/delete/";
                fileUpload.removeFile(apiUrl, url, 'PUT', function(res) {
                    if (response.status == 200) {
                        MessageService.success("File Deleted successfully");
                    };
                });
            };


            $scope.addVendor = function() {
                $state.go('addVendor', { fpoId: fpoId });
            }

            $scope.addMember = function() {
                $state.go('addFpoMember');
            }


        }
    ]);
