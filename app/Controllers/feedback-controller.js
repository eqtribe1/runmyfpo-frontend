// Feedback controller to capture freedback from each screen
// Data captured are user feedbackType and UserPriorityType with a textbox to capture user complete feedback.

angular.module('fpoApp.controllers')

.controller('FeedbackCtrl', ['$scope','$state','QueryService','$stateParams','MessageService','$rootScope','$rootElement',
  function ($scope,$state,QueryService,$stateParams,MessageService,$rootScope,$rootElement) {
      var stateIdentifier = $stateParams.stateIdentifier;
      console.log(stateIdentifier);
	  
	  // To get feedbackType dropdown
       QueryService.query('GET', '/feedback_type' ,{},{}).then(function(response){
        console.log(response.data);
				$scope.feedbacktypes = response.data;
			})
		
     // To get UserPriorityType dropdown
			QueryService.query('GET', '/user_priority_type' ,{},{}).then(function(response){
				$scope.userprioritytypes = response.data;
			})
			
	// When user click on help & feedback tab on header		
        $state.go("feedback");
		
    // when user click on add feedback button
     console.log($rootScope.loginId);
     var userId = $rootScope.loginId;
	 $rootScope.SI = $stateParams.stateIdentifier;
	 $rootScope.ST = $stateParams.screen_title;
	 var appName = $rootElement.attr('ng-app');
	 $scope.addFeedback = function(){
		 var screenId = $stateParams.stateIdentifier;
         $scope.feedback.screen_id= $stateParams.stateIdentifier;
		 //set feedback status manually....
		 $scope.feedback.status = "OPEN";
		 console.log($scope.feedback);
				QueryService.query('POST', '/apps/'+ appName +'/screens/'+screenId+'/feedback',{},$scope.feedback).then(function(response){
				console.log($stateParams.stateIdentifier);
                if(response.status == 200){
					 console.log(response);
                     MessageService.success("Thank you for your feedback!");
						window.history.back();
					}
				},function(error){
					MessageService.error("Sorry, your feedback could not get registered. Please send a mail with details to 'support@eqtribe.com'");
				})
			}
			
			$scope.pastFeedbackList = function(){
			 $state.go("feedbackList",{userId:userId});
			};
			//when user press cancel button.....
	        $scope.goBack = function() {
            	window.history.back();
        	};
			
       		
    }])
	
.controller('FeedbackListCtrl', ['$scope','$state','QueryService','$stateParams','MessageService','$rootScope',
  function ($scope,$state,QueryService,$stateParams,MessageService,$rootScope) {
    var userId = $rootScope.loginId;
	var appName = $stateParams.fpoApp;
// To get past feedback list
			QueryService.query('GET', '/apps/'+ appName + '/feedback/' +userId ,{},{}).then(function(response){
              if (response.status == 200){
                // console.log(response.data);
                $scope.pastFeedbackLists = response.data;
              }
            },function(error){
                MessageService.error(error.data);
            });
			$scope.status = {
				isFirstOpen: true,
			}
			
		
}])	