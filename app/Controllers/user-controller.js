angular.module('fpoApp.controllers')
    .controller('UserRegistrationCtrl', ['$scope', '$rootScope', '$state', '$location', 'otpService', 'QueryService', 'MessageService',
        function($scope, $rootScope, $state, $location, otpService, QueryService, MessageService) {
            $scope.users = [];
            $scope.formData = {};
            $scope.userRegistration = function() {
                if ($scope.myForm.$valid) {
                    QueryService.query('POST', '/register', {}, $scope.formData)
                        .then(function(response) {
                            window.localStorage.removeItem('fpos');
                            if (response.status == 200) {
                                MessageService.success("You have been successfully registered, Please Verify OTP");
                                var data = {};
                                data.email = response.data.loginid;
                                data.mobile = response.data.phone_primary;
                                $rootScope.otpModal = otpService.modal(data);
                                $rootScope.otpModal.closed.then(function(result) {
                                    MessageService.success("OTP verified");
                                    window.history.back();
                                });
                            }
                        }, function(error) {
                            MessageService.error(error.data);
                        })
                }
            }
            $scope.checkPassword = function() {
                $scope.myForm.confirm_password.$error.dontMatch = $scope.formData.password !== $scope.formData.confirm_password;
            };


        }
    ])

.controller('UserListCtrl', ['$scope', '$state', '$stateParams', '$location', 'QueryService', 'MessageService',
        function($scope, $state, $stateParams, $location, QueryService, MessageService) {
            $scope.mgmt_user = [];
            $scope.field_staff_user = [];

            var fpoId = window.localStorage.getItem('fpoId');
            $scope.fpoName = window.localStorage.getItem('fpoName');
            //this method will call everytime when FPO_Owner or any other user who has permission to add/ delete user, wants to see the fpo_users.
            //this method will fetch all the users according to fpo_id.

            QueryService.query('GET', '/fpos/' + fpoId + '/roles/' + "FPO-MANAGER", {}, {}).then(function(response) {
                console.log(response.data);
                if (response.status == 200) {
                    $scope.mgmt_user = response.data;
                } else {
                    MessageService.error("Error in fetching data");
                }

            }, function(error) {
                MessageService.error(error.data);
            });

            QueryService.query('GET', '/fpos/' + fpoId + '/roles/' + "FPO-FIELD-STAFF", {}, {}).then(function(response) {
                console.log(response.data);
                if (response.status == 200) {
                    $scope.field_staff_user = response.data;
                } else {
                    MessageService.error("Error in fetching data");
                }
            }, function(error) {
                MessageService.error(error.data);
            });


            //this method will open update page.....
            $scope.gotoUpdate = function(user) {
                console.log(user);
                $state.go('update-user-profile', { user: user });
            }


            $scope.addUser = function(role) {
                console.log(role);
                $state.go('add-user', { role: role });
            }

        }
    ])
    .controller('UpdateUserProfileCtrl', ['$scope', '$stateParams', '$location', 'QueryService', 'MessageService',
        function($scope, $stateParams, $location, QueryService, MessageService) {
            $scope.formData = {};

            var personId = $stateParams.user;
            console.log(personId);

            //get details of user from persoId.....
            QueryService.query('GET', '/fpos/users/' + personId, {}, {}).then(function(response) {
                if (response.status == 200) {
                    $scope.formData = response.data;
                    $scope.formData.email_personal = response.data.emailId;
                    $scope.formData.phone_primary = response.data.phone;
                }
            }, function(error) {
                MessageService.error("Error in fetching user details.");
            }); //end of API result.....



            //function to update user information....
            $scope.userUpdation = function() {
                //console.log($scope.formData);
                QueryService.query('PUT', '/persons/' + personId, {}, $scope.formData).then(function(response) {
                    if (response.status == 200) {
                        MessageService.success("User information update successfully...");
                        window.history.back();
                    } else {
                        MessageService.success("Error in updating user info.");
                    }
                }, function(error) {
                    //console.log(error.status);
                    MessageService.error(JSON.stringify(error.data));
                })
            }

            //on delete button press.....
            $scope.deleteUser = function() {
                    console.log("User wants to deleted information");
                    QueryService.query('PUT', '/inactivate/' + personId, {}, {}).then(function(response) {
                        if (response.status == 200) {
                            MessageService.success(response.data);
                            window.history.back();
                        }
                    }, function(error) {
                        MessageService.error(error.data);
                    })
                } //end of delete user method....

        }
    ])


.controller('InviteUserCtrl', ['$scope', '$stateParams', '$location', 'QueryService', 'MessageService',
    function($scope, $stateParams, $location, QueryService, MessageService) {

        var fpoId = window.localStorage.getItem('fpoId');
        $scope.role = $stateParams.role;

        $("#joining_date").datetimepicker({
            showClear: true,
            keepOpen: false,
            focusOnShow: false,
            defaultDate: new Date(),
            format: 'DD-MM-YYYY',
            ignoreReadonly: true,
            minDate: new Date(01 - 01 - 1990),
            maxDate: new Date()
        });

        $scope.inviteUser = function() {
            if ($scope.invite_form.$valid) {
                $scope.user.org_roles = [$scope.role];
                QueryService.query('POST', '/fpos/' + fpoId + '/register', {}, $scope.user)
                    .then(function(response) {
                        if (response.status == 200) {
                            window.history.back();
                        }
                    }, function(error) {
                        MessageService.error(error.data);
                    });
            }
        }

    }
])
