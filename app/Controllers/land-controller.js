angular.module('fpoApp.controllers')
    .controller('UpdateLandholdingCtrl', ['$scope', '$stateParams', 'QueryService', 'MessageService',
        function($scope, $stateParams,QueryService, MessageService) {
            // body...
            // $scope.landholding = {};
           var landholding = $scope.landholding = JSON.parse(sessionStorage.getItem('farmerLandHoldingData'));

            // $scope.landholding = landholding;
            $scope.landholding.soil_type = (landholding.soil_type) ? landholding.soil_type.code : "";
            $scope.landholding.irrigation_source = (landholding.irrigation_source) ? landholding.irrigation_source.code : "";

            //http request to get lookup APIs...
            QueryService.query('GET', '/soil_types', {}, {}).then(function(response) {
                console.log(response);
                $scope.soil_types = response.data;
            });
            //http request to get lookup APIs...
            QueryService.query('GET', '/irrigation_sources', {}, {}).then(function(response) {
                console.log(response);
                $scope.irrigation_sources = response.data;
            });
            //update LandHoldings
            $scope.updateLandholding = function() {
                    var landId = $scope.landholding.id;
                    //console.log($scope.family_member);
                    console.log($scope.landholding);
                    delete $scope.landholding.owner;
                    delete $scope.landholding.usages;
                    //call service to update landholding.......
                    QueryService.query('PUT', '/land_ownership/' + landId, {}, $scope.landholding)
                        .then(function(response) {
                            console.log(response.data);
                            console.log($scope.landholding);
                            if (response.status == 200) {
                                window.history.back();
                            } else {
                                MessageService.error("Could'nt save data to backend server..");
                            }
                        }, function(error) {
                            if (error.status == -1) {
                                MessageService.error("Could'nt connect to backend server..");
                            } else {
                                console.log(error.data);
                                //MessageService.error(error.data);
                            }
                        })
                } //end of update landholding....

            // $scope.goBack = function() {
            //     //$state.go('/farmerDetails');
            //     window.history.back();
            // }

            //This function is used for validating cultivate area to be less then total area!
            $scope.compare = function() {
                $scope.success = false;
                $scope.fail = false;
                if ($scope.landholding.area >= $scope.landholding.cultivable_area) {
                    $scope.success = true;
                } else {
                    $scope.fail = true;
                    $scope.fail = "Cultivate area should be less than or equal to total area!";
                }
            }

            //on cancel butoon press....
            $scope.goBack = function() {
                //$state.go('/farmerDetails');
                window.history.back();
            }


            // This method is used for conveting Hectare to Acre!

            $scope.landholding1 = {
                area: 0,
                widthMeters: function() {
                    console.log($scope.landholding);
                    return $scope.landholding.area = $scope.landholding1.area * 2.47;

                }
            }
            $scope.landholding2 = {
                cultivable_area: 0,
                cultivable_acre: function() {
                    return $scope.landholding.cultivable_area = $scope.landholding2.cultivable_area * 2.47;
                }
            }

            $scope.acreCultivable = true;

            $scope.acre = true;

        }
    ])
    .controller('AddLandHoldingCtrl', ['$scope', '$stateParams','QueryService', 'MessageService',
        function($scope, $stateParams,QueryService, MessageService) {

            var farmerId = $stateParams.farmerId;

            //http request to get lookup APIs...
            QueryService.query('GET', '/soil_types', {}, {}).then(function(response) {
                //console.log(response);
                $scope.soil_types = response.data;
            });
            //http request to get lookup APIs...
            QueryService.query('GET', '/irrigation_sources', {}, {}).then(function(response) {
                //console.log(response);
                $scope.irrigation_sources = response.data;
            });
            //http request to get lookup APIs...
            QueryService.query('GET', '/land_area_units', {}, {}).then(function(response) {
                //console.log(response);
                $scope.units = response.data;
            });
            //http request to get lookup APIs...
            QueryService.query('GET', '/land_area_sub_units', {}, {}).then(function(response) {
                //console.log(response);
                $scope.subunits = response.data;
            });





            $scope.addLandholding = function() {
                if ($scope.land_details.$valid) {
                    console.log($scope.landholding);
                    $scope.landholding.unit = "ACRE";
                    QueryService.query('POST', '/farmers/' + farmerId + '/land_ownership', {}, $scope.landholding)
                        .then(function(response) {
                            console.log(response);
                            if (response.status == 200) {
                                window.history.back();
                                MessageService.success("Land Holding added successfully.");
                                //$state.go('/list-landholding/' + farmerId);
                            } else {
                                MessageService.error("Error in saving data...");
                            }
                        }, function(error) {
                            if (error.status == -1) {
                                MessageService.error("Error in saving data...");
                            } else {
                                MessageService.error(error.data);
                            }
                        })
                }
            }



            //This function is used for validating cultivate area to be less then total area!
            $scope.compare = function() {
                $scope.success = false;
                $scope.fail = false;
                if ($scope.landholding.area >= $scope.landholding.cultivable_area) {
                    $scope.success = true;
                } else {
                    $scope.fail = true;
                    $scope.fail = "Cultivate area should be less than or equal to total area!";
                }
            }

            //on cancel butoon press....
            $scope.goBack = function() {
                //$state.go('/farmerDetails');
                window.history.back();
            }


            // This method is used for conveting Hectare to Acre!
            $scope.landholding = {};
            $scope.landholding1 = {
                area: 0,
                widthMeters: function() {
                    console.log($scope.landholding);
                    return $scope.landholding.area = $scope.landholding1.area * 2.47;

                }
            }
            $scope.landholding2 = {
                cultivable_area: 0,
                cultivable_acre: function() {
                    return $scope.landholding.cultivable_area = $scope.landholding2.cultivable_area * 2.47;
                }
            }

            $scope.acreCultivable = true;

            $scope.acre = true;



        }
    ])
    .controller('DetailLandholdingCtrl', ['$scope', '$stateParams','$state',  'QueryService', 'MessageService','$filter',
        function($scope, $stateParams, $state,QueryService, MessageService,$filter) {
            var landholdingId = $stateParams.landholdingId;
            $scope.farmerName = window.localStorage.getItem('farmerName');
            console.log($scope.farmerName);
            $scope.landId = landholdingId;
            //http request to get particular landholding details....
            $scope.landUsage = function(){
            QueryService.query('GET', '/land_ownership/' + landholdingId, {}, {}).then(function(response) {
                if (response.status == 200) {
                    console.log(response.data);
                    window.sessionStorage.setItem("landholding_detail", JSON.stringify(response.data));
                    $scope.landholding_detail = response.data;
                    $scope.landName = response.data.plot_name;
                }
                window.localStorage.setItem('landName', $scope.landName);
            });
            }
             $scope.updateLandholdings = function() {
            window.sessionStorage.setItem('farmerLandHoldingData', JSON.stringify($scope.landholding_detail));
            $state.go('update_landholdings',{'landholding':$scope.landholding_detail});
        }
           $scope.landUsage();

            $scope.add_landUsage = function() {
                $state.go('add_landUsage',{landId:landholdingId});
            }

            $scope.getLandUsageDetail = function(land_usage) {
                //console.log(land_usage);
                $state.go('/land_usage_detail/' + $scope.landName + '/' + JSON.stringify(land_usage));
            }

            //on cancel button click....
            $scope.goBack = function() {
                window.history.back();
            }


           //Delete for land_usage of a farmer
                    $scope.land_details = {};
                    $scope.albumNameArray = [];
                    $scope.removeSelectedItems = function(){
                        angular.forEach($scope.land_details,function(key,value){
                            if(key)
                                $scope.albumNameArray.push(value);
                                console.log(value);
                });

                QueryService.query('PATCH', '/land_usage', {}, {"ids":$scope.albumNameArray})
                  .then(function(response) {
                            // console.log(response.data);
                            // console.log($scope.land_usage);
                            if (response.status == 200) {
                                // $state.reload();
                                $scope.landUsage();

                            } else {
                                MessageService.error("Could'nt save data to backend server..");
                            }
                        }, function(error) {
                            if (error.status == -1) {
                                MessageService.error("Could'nt connect to backend server..");
                            } else {
                                console.log(error.data);
                            }
                        })

            }
              $scope.land_usage_detail = function(usage) {
                $state.go('land-usage-detail',{landName:$scope.landName,'land_usage':JSON.stringify(usage)});
            }
            }
    ])

.controller('LandholdingUsageCtrl', ['$scope', '$stateParams', 'QueryService', 'MessageService',
    function($scope, $stateParams,  QueryService, MessageService) {

        var landId = $stateParams.landId;
         //$scope.landName = $stateParams.landName;
         $scope.landName = window.localStorage.getItem('landName');
         console.log(landId+" and "+ $scope.landName);
        //http request to server to get all seasons....
        QueryService.query('GET', '/season_types', {}, {}).then(function(response) {
            console.log(response.data);
            if (response.status == 200) {
                $scope.season_types = response.data;
            }
        });


        //this method will used to filter integer....
        $scope.filterValue = function($event) {
            if (isNaN(String.fromCharCode($event.keyCode))) {
                $event.preventDefault();
            }
        };

        $scope.addUsage = function() {
            $scope.land_usage.unit = "ACRE";
            $scope.land_usage.land_id = landId;
            if ($scope.land.$valid) {
                console.log($scope.land_usage);
                QueryService.query('POST', '/land_ownership/' + landId + '/usage', {}, $scope.land_usage)
                    .then(function(response) {
                        if (response.status == 200) {
                            console.log(response.data);
                            //$state.go('/land-detail/'+landId);
                            window.history.back();
                        }
                    }, function(error) {
                        MessageService.error(error.data);
                    });
            } else {
                MessageService.error("Please fill valid data...");
            }
        }


        $scope.goBack = function() {
            window.history.back();
        }
        $scope.compare = function() {
            $scope.success = false;
            $scope.fail = false;
            if ($scope.land_usage.cultivated_area >= $scope.land_usage.leased_out_area) {
                $scope.success = true;
            } else {
                $scope.fail = true;
                $scope.fail = "Leased Out area should be less than or equal to total Cultivated area!";

            }
        }

        // This method is used for conveting Hectare to Acre!
        $scope.land_usage = {};
        $scope.land_usage1 = {
            cultivated_area: 0,
            widthMeters: function() {
                //console.log($scope.land_usage);
                return $scope.land_usage.cultivated_area = $scope.land_usage1.cultivated_area * 2.47;

            }
        }
        $scope.land_usage2 = {
            leased_out_area: 0,
            cultivable_acre: function() {
                return $scope.land_usage.leased_out_area = $scope.land_usage2.leased_out_area * 2.47;
            }
        }

        $scope.acreCultivable = true;

        $scope.acre = true;
 // for showing year dropdown range..
   $scope.dates = [];
        var initDates = function() {
            var i;
            for (i = 2014;i <= 2018; i++) {
            $scope.dates.push(i);
                }
            }
                initDates();


    }
])
//-----------------------------------start --------------
.controller('LandUsageDetailCtrl', ['$scope', '$stateParams', 'QueryService', 'MessageService',
        function($scope, $stateParams, QueryService, MessageService) {

            $scope.landholding_detail = JSON.parse(window.sessionStorage.getItem("landholding_detail"));
            $scope.land_usage = {};
            console.log(JSON.parse($stateParams.land_usage));
            $scope.land_usage = JSON.parse($stateParams.land_usage);
            $scope.land_usage.season = $scope.land_usage.season.code;
            $scope.landName = $stateParams.landName;

            //http request to server to get all seasons....
            QueryService.query('GET', '/season_types', {}, {}).then(function(response) {
                console.log(response.data);
                if (response.status == 200) {
                    $scope.season_types = response.data;
                }
            });

            $scope.isvalidArea = function() {
                return (parseInt($scope.landholding_detail.area) >= (parseInt($scope.land_usage.cultivated_area) + parseInt($scope.land_usage.leased_out_area)));
            };

            $scope.updateLandUsage = function() {
                    var landUsageId = $scope.land_usage.id;
                    delete $scope.land_usage.land_id;
                    if ($scope.isvalidArea()) {
                        QueryService.query('PUT', '/land_usage/' + landUsageId, {}, $scope.land_usage)
                            .then(function(response) {
                                if (response.status == 200) {
                                    window.history.back();
                                } else {
                                    MessageService.error("Could'nt save data to backend server..");
                                }
                            }, function(error) {
                                if (error.status == -1) {
                                    MessageService.error("Could'nt connect to backend server..");
                                } else {
                                    MessageService.error(error.data);
                                }
                            })
                    } else {
                        MessageService.error("Cultivable area + leased out area[" + $scope.land_usage.cultivated_area + " + " + $scope.land_usage.leased_out_area + "] should be less then or equal to total area[" + $scope.landholding_detail.area + "]");
                    }
                } //end of update landholding...

            $scope.goBack = function() {
                window.history.back();
                //$state.go('/land-detail/'+landId);
            }
            $scope.compare = function() {
                $scope.success = false;
                $scope.fail = false;
                if ($scope.land_usage.cultivated_area >= $scope.land_usage.leased_out_area) {
                    $scope.success = true;
                } else {
                    $scope.fail = true;
                    $scope.fail = "Leased Out area should be less than or equal to total Cultivated area!";

                }
            }

            // This method is used for conveting Hectare to Acre!

            $scope.land_usage1 = {
                cultivated_area: 0,
                widthMeters: function() {
                   // console.log($scope.land_usage);
                    return $scope.land_usage.cultivated_area = $scope.land_usage1.cultivated_area * 2.47;

                }
            }
            $scope.land_usage2 = {
                leased_out_area: 0,
                cultivable_acre: function() {
                    return $scope.land_usage.leased_out_area = $scope.land_usage2.leased_out_area * 2.47;
                }
            }

            $scope.acreCultivable = true;
            $scope.acre = true;

            // for showing year dropdown range..
            $scope.calendarYears = [];
            var initDates = function() {
                var i;
                for (i = 2014; i <= 2018; i++) {
                    $scope.calendarYears.push(i);
                }
            }
            initDates();
        }
    ])

    .controller('SeasonalCultivationCtrl', ['$scope', '$stateParams','QueryService', 'MessageService',
        function($scope, $stateParams,QueryService, MessageService) {

            var farmerId = $stateParams.farmerId;
            console.log(farmerId);
            //call QueryService to get seasonal_cultivation according to farmerId.....
            QueryService.query('GET', '/farmers/' + farmerId + '/cultivation', {}, {})
                .then(function(response) {
                    console.log(response);
                    if (response.status == 200) {
                        console.log(response.data);
                        $scope.cultivations = response.data;
                    } else {
                        MessageService.error(response.data);
                    }
                });
            //on cancel butoon press....
            $scope.goBack = function() {
                //$state.go('/farmerDetails');
                window.history.back();
            }



        }
    ])

.controller('AddSeasonalCultivationCtrl', ['$scope', '$state', '$stateParams', 'QueryService', 'MessageService',
    function($scope, $state, $stateParams, QueryService, MessageService) {

        $scope.land_usage = {};
        $scope.landholding = {};

        var farmerId = $stateParams.farmerId;
        QueryService.query('GET', '/land_area_units', {}, {}).then(function(response) {
            // console.log(response);
            $scope.units = response.data;
        });
        //http request to get lookup APIs...
        QueryService.query('GET', '/land_area_sub_units', {}, {}).then(function(response) {
            // console.log(response);
            $scope.subunits = response.data;
        });

        $scope.isLandOwened = true;

        $scope.accordion = {
            current: null
        };

        //call QueryService to get seasonal_cultivation according to farmerId.....
        QueryService.query('GET', '/farmers/' + farmerId + '/cultivation', {}, {}).then(function(response) {
            console.log(response);
            if (response.status == 200) {
                console.log(response.data);
                $scope.cultivations = response.data;
            } else {
                MessageService.error(response.data);
            }
        });

        QueryService.query('GET', '/season_types', {}, {}).then(function(response) {
            console.log(response.data);
            if (response.status == 200) {
                $scope.season_types = response.data;
            }
        });


        QueryService.query('GET', '/farmers/' + farmerId + '/land_ownership', {}, {}).then(function(response) {
            console.log(response.data);
            if (response.status == 200) {
                $scope.farmerLands = response.data.plots;
            }
        });


        $scope.compareLandHold = function() {
            $scope.success = false;
            $scope.fail = false;
            if (parseInt($scope.land_usage.cultivated_area) >= parseInt($scope.land_usage.leased_out_area)) {
                $scope.success = true;
            } else {
                $scope.fail = true;
                $scope.fail = "Cultivate area should be less than or equal to total area!";
            }
        }
        $scope.compareLeasedIn = function() {
            $scope.success = false;
            $scope.fail = false;
            if (parseInt($scope.landholding.total_area) >= parseInt($scope.landholding.cultivable_area)) {
                $scope.success = true;
            } else {
                $scope.fail = true;
                $scope.fail = "Cultivate area should be less than or equal to total area!";

            }
        }

        $scope.addSeasonalCultivation = function() {
            $scope.landholding.unit = "ACRE";
            $scope.land_usage.unit = "ACRE";
            var year = $scope.land_usage.year;
            var season = $scope.land_usage.season;
            if ($scope.isLandOwened) {
                QueryService.query('POST', '/land_ownership/' + farmerId + '/usage', {}, $scope.land_usage)
                    .then(function(response) {
                        if (response.status == 200) {
                            console.log(response.data);
                            //$state.go('/land-detail/'+landId);
                            window.history.back();
                        }
                    }, function(error) {
                        MessageService.error(error.data);
                    });
            } else {
                $scope.landholding.year = year;
                $scope.landholding.season = season;
                delete $scope.landholding.owner_name;
                console.log($scope.landholding);
                QueryService.query('POST', '/farmers/' + farmerId + '/land_leased_in', {}, $scope.landholding)
                    .then(function(response) {
                        if (response.status == 200) {
                            console.log(response.data);
                            //$state.go('/land-detail/'+landId);
                            window.history.back();
                        }
                    }, function(error) {
                        MessageService.error(error.data);
                    });
            }
        }
        $scope.goBack = function() {
            //$state.go('/farmerDetails');
            window.history.back();
        }


        // This method is used for conveting Hectare to Acre!
        $scope.landholding = {};
        $scope.landholding1 = {
            total_area: 0,
            widthMeters: function() {
                console.log($scope.landholding);
                return $scope.landholding.total_area = $scope.landholding1.total_area * 2.47;

            }
        }
        $scope.landholding2 = {
            cultivable_area: 0,
            cultivable_acre: function() {
                return $scope.landholding.cultivable_area = $scope.landholding2.cultivable_area * 2.47;
            }
        }

        $scope.acreCultivable = true;
        $scope.acre = true;


        // This method is used for converting Hectare to Acre for land_owned!
        $scope.land_usage = {};
        $scope.land_usage1 = {
            cultivated_area: 0,
            widthMeters: function() {
                console.log($scope.land_usage);
                return $scope.land_usage.cultivated_area = $scope.land_usage1.cultivated_area * 2.47;

            }
        }
        $scope.land_usage2 = {
            leased_out_area: 0,
            cultivable_acre: function() {
                return $scope.land_usage.leased_out_area = $scope.land_usage2.leased_out_area * 2.47;
            }
        }
        $scope.acreCultivable = true;
        $scope.acre = true;

         // for showing year dropdown range..
        //$scope.dates = [];
        // var initDates = function() {
        //     var i;
        //     for (i = 2014;i <= 2018; i++) {
        //     $scope.dates.push(i);
        //         }
        //     }
        //         initDates();



    }
])



.controller('UpdateSeasonalCultivationCtrl', ['$scope', '$stateParams','QueryService', 'MessageService','CommonMethodService',
    function($scope, $stateParams, QueryService, MessageService, CommonMethodService) {

        $scope.land_usage = {};
        $scope.landholding = {};
         var farmerId = $stateParams.farmerId;
         console.log($stateParams.farmerId);
        var landType = $stateParams.landType;
        $scope.land_usage.unit = $scope.landholding.unit = "ACRE";

        $scope.land_usage = $scope.landholding = JSON.parse(sessionStorage.getItem('farmerSeasonalCultivationData'));

        if($stateParams.landType == 'ownedLand'){
        	$scope.isLandOwened = true;
        	$scope.land_usage.land_id = $scope.land_usage.land_id.id;
        }else{
        	$scope.isLandOwened = false;
        }


        QueryService.query('GET', '/land_area_units', {}, {}).then(function(response) {
            $scope.units = response.data;
        });

        //http request to get lookup APIs...
        QueryService.query('GET', '/land_area_sub_units', {}, {}).then(function(response) {
            // console.log(response);
            $scope.subunits = response.data;
        });

        $scope.accordion = {
            current: null
        };

        //call QueryService to get seasonal_cultivation according to farmerId.....
        QueryService.query('GET', '/farmers/' + farmerId + '/cultivation', {}, {}).then(function(response) {
            console.log(response);
            if (response.status == 200) {
                console.log(response.data);
                $scope.cultivations = response.data;
            } else {
                MessageService.error(response.data);
            }
        });

        QueryService.query('GET', '/season_types', {}, {}).then(function(response) {
            console.log(response.data);
            if (response.status == 200) {
                $scope.season_types = response.data;
            }
        });


        QueryService.query('GET', '/farmers/' + farmerId + '/land_ownership', {}, {}).then(function(response) {
            console.log(response.data);
            if (response.status == 200) {
                $scope.farmerLands = response.data.plots;
            }
        });

        $scope.compareLandHold = function() {
            $scope.success = false;
            $scope.fail = false;
            if (parseInt($scope.land_usage.cultivated_area) >= parseInt($scope.land_usage.leased_out_area)) {
                $scope.success = true;
            } else {
                $scope.fail = true;
                $scope.fail = "Cultivate area should be less than or equal to total area!";
            }
        }
        $scope.compareLeasedIn = function() {
            $scope.success = false;
            $scope.fail = false;
            if (parseInt($scope.landholding.total_area) >= parseInt($scope.landholding.cultivable_area)) {
                $scope.success = true;
            } else {
                $scope.fail = true;
                $scope.fail = "Cultivate area should be less than or equal to total area!";

            }
        }

        $scope.updateSeasonalCultivation = function() {
            $scope.landholding.unit = $scope.land_usage.unit = "ACRE";
            if($scope.landholding.subunit || $scope.land_usage.subunit){
	            $scope.landholding.subunit = $scope.land_usage.subunit = ($scope.land_usage.subunit)?
    	        	$scope.land_usage.subunit.code:$scope.landholding.subunit.code;
            }


            var land_ownership_keys = ['id', 'year', 'cultivated_area',
							            'cultivated_area_subunit', 'leased_out_area', 'leased_out_area_subunit',
							            'unit', 'subunit'
							           ];
			var land_leased_in_keys = ['id', 'year', 'season', 'plot_name',
                                'total_area', 'total_area_subunit',
                                'cultivable_area', 'cultivable_area_subunit',
                                'unit', 'subunit'];
            if ($scope.isLandOwened) {
		        var landId = $scope.land_usage.id;
	            $scope.land_ownership_params = CommonMethodService.filterParams($scope.land_usage,land_ownership_keys)
                QueryService.query('PUT', '/land_usage/' + landId, {}, $scope.land_ownership_params)
                    .then(function(response) {
                        if (response.status == 200) {
                            console.log(response.data);
                            //$state.go('/land-detail/'+landId);
                            window.history.back();
                        }
                    }, function(error) {
                        MessageService.error(error.data);
                    });
            } else {
            	var landId = $scope.land_usage.id
	            $scope.land_ownership_params = CommonMethodService.filterParams($scope.land_usage,land_leased_in_keys);
                QueryService.query('PUT', '/land_leased_in/' + landId, {}, $scope.land_ownership_params)
                    .then(function(response) {
                        if (response.status == 200) {
                            console.log(response.data);
                            window.history.back();
                        }
                    }, function(error) {
                        MessageService.error(error.data);
                    });
            }
        }
        $scope.goBack = function() {
            window.history.back();
        }

        // This method is used for conveting Hectare to Acre!
        // $scope.landholding = {};
        $scope.landholding1 = {
            total_area: 0,
            widthMeters: function() {
                console.log($scope.landholding);
                return $scope.landholding.total_area = $scope.landholding1.total_area * 2.47;

            }
        }
        $scope.landholding2 = {
            cultivable_area: 0,
            cultivable_acre: function() {
                return $scope.landholding.cultivable_area = $scope.landholding2.cultivable_area * 2.47;
            }
        }

        $scope.acreCultivable = true;
        $scope.acre = true;


        // This method is used for converting Hectare to Acre for land_owned!
        $scope.land_usage1 = {
            cultivated_area: 0,
            widthMeters: function() {
                console.log($scope.land_usage);
                return $scope.land_usage.cultivated_area = $scope.land_usage1.cultivated_area * 2.47;

            }
        }
        $scope.land_usage2 = {
            leased_out_area: 0,
            cultivable_acre: function() {
                return $scope.land_usage.leased_out_area = $scope.land_usage2.leased_out_area * 2.47;
            }
        }
        $scope.acreCultivable = true;
        $scope.acre = true;

            // for showing year dropdown range..
                $scope.calendarYears = [];
                        var initDates = function() {
                            var i;
                            for (i = 2014;i <= 2018; i++) {
                            $scope.calendarYears.push(i);
                                }
                            }
                                initDates();
    }
])
