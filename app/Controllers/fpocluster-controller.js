angular.module('fpoApp.controllers')

.controller('FpoClusterDetailsCtrl', ['$scope', '$state', '$stateParams', 'QueryService', 'MessageService',
        function($scope, $state, $stateParams, QueryService, MessageService) {
            //this method will fetch villages associated with cluster

            var clusterId = $stateParams.clusterId;

            QueryService.query('GET', '/clusters/details/' + clusterId, {}, {})
                .then(function(response) {
                    if (response.status == 200) {
                        $scope.cluster_detail = response.data;
                        $scope.cluster_villages = response.data.cluster_villages;
                        $state.go('fpoClusterDetail', {
                            clusterId: clusterId
                        })
                    }
                }, function(error) {
                    MessageService.error(error.data);
                });
        }
    ])
    .controller('FpoClusterAddCtrl', ['$scope', '$state', '$stateParams',
        'QueryService', 'MessageService', 'areaMultiSelect',
        function($scope, $state, $stateParams, QueryService, MessageService,
            areaMultiSelect) {

            /* Initialize multi-select for states */

            $scope.selected_states = [];
            $scope.selected_districts = [];
            $scope.selected_villages = [];

            $scope.customStateSettings = areaMultiSelect.customStateSettings;
            $scope.customDistrictSettings = areaMultiSelect.customDistrictSettings;
            $scope.customVillageSettings = areaMultiSelect.customVillageSettings;


            $scope.add_cluster = function(villages) {
                QueryService.query('POST', '/clusters/' + localStorage.fpoId, {}, {
                        "name": $scope.cluster_detail.name,
                        "cluster_villages": $scope.selected_villages
                    })
                    .then(function(response) {
                        if (response.status == 200) {
                            $scope.cluster_detail = response.data;
                            console.log(response.data);
                            $state.go('fpoClusterDetail', {
                                clusterId: response.data[0].cluster
                            })
                        }
                    }, function(error) {
                        MessageService.error(error.data);
                    });
            };
            QueryService.query('GET', '/countries/INDIA/states', {}, {})
                .then(function(response) {
                    if (response.status == 200) {
                        $scope.states = response.data;
                    }
                }, function(error) {
                    MessageService.error(error.data);
                });

            $scope.state_list = {
                onItemSelect: function() {
                    console.log($scope.selected_states);
                    var state_ids = _.map($scope.selected_states, function(state) {
                        return state.id
                    });
                    console.log(state_ids);
                    QueryService.query('PATCH', '/state_districts', {}, {
                            "ids": state_ids
                        })
                        .then(function(response) {
                            if (response.status == 200) {
                                $scope.districts = response.data;
                            }
                        }, function(error) {
                            MessageService.error(error.data);
                        })
                }
            };

            $scope.district_list = {
                onItemSelect: function(item) {
                    var dist_ids = _.map($scope.selected_districts, function(dist) {
                        return dist.id
                    });
                    QueryService.query('PATCH', '/district_villages', {}, {
                            "ids": dist_ids
                        })
                        .then(function(response) {
                            if (response.status == 200) {
                                $scope.villages = response.data;
                                console.log($scope.villages);
                                angular.forEach($scope.villages, function(value, key) {
                                    $scope.villages[key].distVillage = value.village_name + " : " + value.village_district;
                                })
                            }
                        }, function(error) {
                            MessageService.error(error.data);
                        })
                }
            };

            $scope.village_list = {
                onChange: function(item) {
                    var village_ids = _.map($scope.selected_villages, function(village) {
                        return village.id;
                    });
                    $scope.selected_villages = village_ids;
                }
            };

        }
    ])

.controller('FpoClusterDetailsUpdateCtrl', ['$scope', '$state', '$stateParams',
    'QueryService', 'MessageService', 'areaMultiSelect',
    function($scope, $state, $stateParams, QueryService,
        MessageService, areaMultiSelect) {

        /* Initialize multi-select for states */

        $scope.selected_states = [];
        $scope.selected_districts = [];
        $scope.selected_villages = [];

        $scope.customStateSettings = areaMultiSelect.customStateSettings;
        $scope.customStateSettings.externalIdProp = 'state_code';
        $scope.customDistrictSettings = areaMultiSelect.customDistrictSettings;
        $scope.customDistrictSettings.externalIdProp = 'district_code';
        $scope.customVillageSettings = areaMultiSelect.customVillageSettings;
        $scope.customVillageSettings.externalIdProp = 'village_id';

        var selected_villages, selected_villages, selected_districts;

        QueryService.query('GET', '/countries/INDIA/states', {}, {})
            .then(function(response) {
                if (response.status == 200) {
                    $scope.states = response.data;
                }
            }, function(error) {
                MessageService.error(error.data);
            });

        var clusterId = $stateParams.clusterId;
        //this method will fetch villages associated with cluster
        QueryService.query('GET', '/clusters/details/' + clusterId, {}, {})
            .then(function(response) {
                if (response.status == 200) {
                    console.log(response.data);
                    $scope.cluster_detail = response.data;
                    var cluster_villages = response.data.cluster_villages;
                    if (cluster_villages.length > 0) {
                        selected_states = _.map(cluster_villages, function(village) {
                            return { "state_code": village.state };
                        });
                    }
                    $scope.selected_states = _.uniq(selected_states, 'state_code');
                    console.log($scope.selected_states);
                    var state_ids = _.map($scope.selected_states, function(state) {
                        return state.state_code;
                    });
                    QueryService.query('PATCH', '/state_districts', {}, {
                            "ids": state_ids
                        })
                        .then(function(response) {
                            if (response.status == 200) {
                                $scope.districts = response.data;
                                console.log(response.data);
                            }
                        }, function(error) {
                            MessageService.error(error.data);
                        })

                }

                selected_districts = _.map(cluster_villages, function(village) {
                    return { "district_code": village.district };
                });

                $scope.selected_districts = _.uniq(selected_districts, 'district_code');
                var district_ids = _.map(selected_districts, function(district) {
                    return district.district_code;
                });

                selected_villages = _.map(cluster_villages, function(village) {
                    return { "village_id": village.id };
                });
                $scope.selected_villages = _.uniq(selected_villages, 'village_id');

                QueryService.query('PATCH', '/district_villages', {}, {
                        "ids": district_ids
                    })
                    .then(function(response) {
                        if (response.status == 200) {
                            $scope.villages = response.data;
                            angular.forEach($scope.villages, function(value, key) {
                                $scope.villages[key].distVillage = value.village_name + " : " + value.village_district;
                            })
                        }
                    }, function(error) {
                        MessageService.error(error.data);
                    })
            }, function(error) {
                MessageService.error(error.data);
            });

        /* This is to capture events - For now, there is repetition of apis because
         *  of lack of dropdownhide event in multi-select
         */

        $scope.state_list = {
            onItemDeselect: function(item) {
                MessageService.warning("Please first remove the districts \
                      associated with selected state, " + JSON.stringify(item));
                return;
                console.log($scope.selected_states);
            },

            onItemSelect: function(item) {
                var state_ids = _.map($scope.selected_states,
                    function(state) {
                        return state.state_code;
                    });

                QueryService.query('PATCH', '/state_districts', {}, {
                        "ids": state_ids
                    })
                    .then(function(response) {
                        if (response.status == 200) {
                            $scope.districts = response.data;
                        }
                    }, function(error) {
                        MessageService.error(error.data);
                    })
            }
        };

        $scope.district_list = {
            onItemDeselect: function(item) {
                MessageService.warning("Please also remove the villages \
                  associated with selected district, " + JSON.stringify(item));
            },
            onItemSelect: function(item) {
                var dist_ids = _.map($scope.selected_districts, function(dist) {
                    return dist.district_code;
                });
                console.log(dist_ids);
                QueryService.query('PATCH', '/district_villages', {}, {
                        "ids": dist_ids
                    })
                    .then(function(response) {
                        if (response.status == 200) {
                            $scope.villages = response.data;
                            angular.forEach($scope.villages, function(value, key) {
                                $scope.villages[key].distVillage = value.village_name + " : " + value.village_district;
                            })
                        }
                    }, function(error) {
                        MessageService.error(error.data);
                    })
            }
        };

        $scope.village_list = {
            onItemSelect: function(item) {
                console.log(item);
                var village_ids = _.map($scope.selected_villages, function(village) {
                    return village.village_id;
                });
                $scope.village_ids = village_ids;
            },
            onItemDeselect: function(item) {
                console.log(item);
                _.remove($scope.selected_villages, item);
                var village_ids = _.map($scope.selected_villages, function(village) {
                    return village.village_id;
                });
                console.log(village_ids);
                $scope.village_ids = village_ids;
            }
        };

        $scope.update_cluster = function(villages) {
            console.log(villages);
            QueryService.query('PUT', '/clusters/' + clusterId, {}, {
                    "cluster_villages": $scope.village_ids
                })
                .then(function(response) {
                    if (response.status == 200) {
                        $scope.cluster_detail = response.data;
                        console.log(response.data);
                        MessageService.success("The Cluster has been Updated");
                        $state.go('fpoClusterDetail', {
                            clusterId: clusterId
                        })
                    }
                }, function(error) {
                    MessageService.error(error.data);
                });
        };

    }
])
