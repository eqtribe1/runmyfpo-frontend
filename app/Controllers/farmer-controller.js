angular.module('fpoApp.controllers')
.controller('FpoMembersDetailsUpdateCtrl',['$scope','$stateParams','$filter','QueryService','MessageService','$timeout',
	function($scope,$stateParams,$filter,QueryService,MessageService,$timeout){

		$scope.farmerDetails = {};

		$("#DOB").datetimepicker({
			showClear: true,
         	keepOpen: false,
          	focusOnShow: false,
          	format: 'DD-MM-YYYY',
          	ignoreReadonly: true,
          	maxDate: moment(),
          	minDate: new Date("01/01/1940")
        });

        $("#DOB").on("dp.change", function(e) {
        	var tmpDob = moment($('#DOB').val(),"DD-MM-YYYY").year();
			$scope.farmerData.farmerDetails.year_birth = tmpDob;

			$timeout(function(){
				angular.element($("#year_birth")).scope().$apply();
			});
        });

        
        QueryService.query('GET','/education_types',{},{}).then(function(response){
	    
	        if(response.status == 200){
	            $scope.education_level = response.data;
	        }
	        else{
	            MessageService.error("Error in fetching data.");
	        }
        },function(error){
        	MessageService.error(error.data);
        });
        
        
        QueryService.query('GET','/religions',{},{}).then(function(response){
	    
	        if(response.status == 200){
	            $scope.religions = response.data;
	        }
	        else{
	            MessageService.error("Error in fetching data...");
	        }
	    },function(error){
	        MessageService.error(error.data);
        });

        
        QueryService.query('GET','/castes',{},{}).then(function(response){
	    
	        if(response.status == 200){
	            $scope.castes = response.data;
	        }
	        else{
	            MessageService.error("Error in fetching data...");
	        }
	        },function(error){
	        MessageService.error(error.data);
        });

		var farmerId = JSON.parse($stateParams.farmerId);
		$scope.farmerData = JSON.parse(window.sessionStorage.getItem('farmerData'));

		if(_.isObject($scope.farmerData.farmerDetails.marital_status)){
			$scope.farmerData.farmerDetails.marital_status = $scope.farmerData.farmerDetails.marital_status.code;
		}
		
		if(_.isObject($scope.farmerData.farmerDetails.religion)){
			$scope.farmerData.farmerDetails.religion = $scope.farmerData.farmerDetails.religion.code;
		}

		$scope.filterObjectByKey = function(filterObject,filterKeys){
			var result = {}, key;

			for (key in filterObject) {
		        if (filterKeys.indexOf(key) > -1 && filterObject[key] ) {
		            result[key] = filterObject[key];
		        }
		    }
		    return result;
	  	};

	  	$('#DOB').data("DateTimePicker").date(moment($scope.farmerData.farmerDetails.DOB));		
		
		var validParams = 	['father_name', 'household_year_income','mother_name','religion', 'caste', 'year_birth',,'fname','mname','lname',
							'phone_primary','email_personal','gender','marital_status','education_level','dob'];

		$scope.update_details = function(){

			$scope.farmerData.farmerDetails.dob = new Date(moment($('#DOB').val(),"DD-MM-YYYY"));
			$scope.farmerData.farmerDetails.year_birth = $("#year_birth").val();
			
			var updatedData = _.merge($scope.farmerData.farmerDetails,$scope.fpoRelationships);
			updatedData.marital_status = updatedData.marital_status;
			updatedData.education_level = updatedData.education_level_code;

			var data = $scope.filterObjectByKey(updatedData,validParams);

			QueryService.query('PUT', '/farmers/'+farmerId,{},data).then(function(response){
			
				if(response.status == 200){
					MessageService.success("Farmer updated successfull...");
					window.history.back();
				}
				else{
					MessageService.error("Could'nt save data to backend server..");
				}
			},function(error){
			
				if(error.status == -1){
					MessageService.error("Could'nt connect to backend server..");
				}
				else{
					console.log(error.data);
					MessageService.error(error.data);
				}
			})
		}

		$scope.goBack = function() {
		  	window.history.back();
		}

        $scope.getYear = function(date){
             $scope.farmerData.farmerDetails.year_birth = $filter('date')(date, 'yyyy');
        }

        //maxDate for DOB in updateFarmerPersonalDetail screen
        $scope.maxDate= new Date();
}])
