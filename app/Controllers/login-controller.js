;(function() {
    'use strict';
    angular.module('fpoApp.controllers')

    .controller('loginCtrl', ['MessageService', '$location', '$state', '$scope', '$rootScope', 'QueryService', 'otpService',
            function(MessageService, $location, $state, $scope, $rootScope, QueryService, otpService) {
                $scope.user = {};

                // Simple helper function which triggers user logout action.
                $scope.logout = function logout() {
                    window.localStorage.clear();
                    MessageService.success('You have been successfully logged out.');
                    $state.go('logout');
                };

                $scope.login = function() {
                    var state;
                    QueryService.query('POST', '/login', {}, $scope.user)
                        .then(function(response) {
                            var data = response.data;
                            if (response.status === 200) {
                                MessageService.success('You have successfully logged in');
                                window.localStorage.setItem('userName', data.personName);
                                window.localStorage.setItem('token', data.token);
                                window.localStorage.setItem('personId', data.personId);

                                //window.localStorage.fpos = JSON.stringify(data.FPOs);
                                window.localStorage.setItem('admin_status', response.data.admin_status[0].sys_role);

                                $state.go('fpoSelect');
                            } else if (response.status === 203 && data.state === 'otpVerificaiton') {
                                data.email = $scope.user.loginid;
                                $rootScope.otpModal = otpService.modal(data);
                                $rootScope.otpModal.closed.then(function(result) {
                                    MessageService.success("OTP verified. Please login");
                                    // $state.go($rootScope.nextState);
                                });
                            }
                        }, function(error) {
                            console.error(error.data);
                        });
                };

                $scope.userRegister = function userRegister() {
                    $state.go('newRegister');
                };
            }
        ])
        .controller('validateEmail', function() {
            var EMAIL_REGEXP = /^[_a-z0-9]+(\.[_a-z0-9]+)*@example\.com$/;
            return {
                require: 'ngModel',
                restrict: 'abc@example.com',
                link: function(scope, elm, attrs, ctrl) {
                    if (ctrl && ctrl.$validators.loginid) {
                        ctrl.$validators.loginid = function(modelValue) {
                            return ctrl.$isEmpty(modelValue) || EMAIL_REGEXP.test(modelValue);
                        };
                    }
                }
            };
        })
        .controller('otpModalCtrl', ['$scope', '$stateParams', '$state', '$rootScope', 'otpService', 'QueryService', 'MessageService',
            function($scope, $stateParams, $state, $rootScope, otpService, QueryService, MessageService) {
                var data = $scope.data = {};
                data.email = $rootScope.email;
                data.mobile = $rootScope.mobile;
                if (!data.mobile) {
                    $scope.changeMobileSection = true;
                }
                $scope.changeMobile = function(data) {
                    $scope.changeMobileSection = !$scope.changeMobileSection;
                    if (data && data.mobile) {
                        data.mobile = $rootScope.mobile;
                    }
                };
                $scope.verifyOtp = function() {
                    if (!data || !data.otp) {
                        MessageService.info('Plese enter the otp!');
                    } else {
                        QueryService.query('POST', '/user/verifyotp', {}, data)
                            .then(function(response) {
                                if (response.status === 200) {
                                    otpService.dismissModal({ success: true, state: 'fpoSelect' });
                                }
                            }, function(error) {
                                console.log(error.data);
                            });
                    }
                };
                $scope.resendOtp = function() {
                    QueryService.query('POST', '/user/regenerate_otp', {}, data)
                        .then(function(response) {
                            if (response.status === 200) {
                                MessageService.success("An otp is send to " + data.mobile);
                            }
                        }, function(error) {
                            MessageService.error(error.data);
                        });
                };
                $scope.cancel = function() {
                    otpService.dismissModal();
                };
            }
        ]);


}());
