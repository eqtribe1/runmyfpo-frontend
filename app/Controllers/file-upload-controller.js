angular.module('fpoApp.controllers')
    .controller('FileUploadCtrl', ['$scope', '$timeout', 'fileUpload', 'QueryService', 'MessageService',
        function($scope, $timeout, fileUpload, QueryService, MessageService) {

            $scope.uploadFile = function(file) {
                console.log(file);

                var img;
                var APIUrl = "/images/upload";

                if (file) {
                    fileUpload.uploadFile(file, APIUrl, function(response) {
                        var url = response.data.profilePicUrl;
                        console.log(url);
                        if (!url) {
                            MessageService.info('Unable to Uploaded Image. Please try again!');
                        } else {
                            MessageService.success('Image Uploaded successfully');
                        }
                    })
                }
            }

            $scope.thumbnail = {
                dataUrl: ''
            };

            $scope.fileReaderSupported = window.FileReader != null;

            $scope.photoChanged = function(files) {
                if (files != null) {
                    var file = files[0];
                    if ($scope.fileReaderSupported && file.type.indexOf('image') > -1) {
                        $timeout(function() {
                            var fileReader = new FileReader();
                            fileReader.readAsDataURL(file);
                            fileReader.onload = function(e) {
                                $timeout(function() {
                                    $scope.thumbnail.dataUrl = e.target.result;
                                });
                            }
                        });
                    }
                    $scope.uploadFile(file);
                }
            };
            $scope.removeFeaturedImage = function(file) {
                $scope.thumbnail.dataUrl = ""
            }
        }
    ])