
  angular
    .module('fpoApp.controllers')//,['ngCookies'])
    .controller('SearchFarmerCtrl',['MessageService','$state','$scope','QueryService',
      function(MessageService,$state,$scope,QueryService){
        'use strict';

      	$scope.farmerData = {};
		$scope.showSearchBar = true;

        QueryService.query('GET','/kyc_doc_types',{},{}).then(function(response){
			//console.log(response);
		   	if(response.status ==  200){
		    	$scope.kyc_doc_types = response.data;
		   	}else{
		    	MessageService.error("Failed to connect server...");
		   	}
		},function(error){
		   MessageService.error(error.data);
	  	});

        $scope.searchFarmer = function(farmerData){
        	var queryString = "";
        	var count=0;
        		if(farmerData){
        			for (var data in farmerData ){
        				if(count > 0){
        					queryString += "&";
        				}
        				queryString += data+"="+farmerData[data];
        				count++;
        			}
        		}
        	    QueryService.query('GET','/farmer/search'+'?'+queryString,{},{}).then(function(response){
        	    	if(response.status == 200){
						$scope.showSearchBar = false;

	        	    	$scope.farmersData = response.data;
        	    	}else{
        	    		MessageService.error("Error in searching farmer data...");
        	    	}
        	    },function(error){
			    	MessageService.error(error.data);
			    })
        };
        $scope.searchAgain = function(){
			$scope.showSearchBar = true;
    	}

    	$scope.selectFarmer = function(farmer){
	        var farmerId = farmer.id;
	        $state.go('farmerDetails',{'farmerId':farmerId});
    	}

    }])


    .controller('EventSearchCtrl',['MessageService','$state','$scope','QueryService',
      function(MessageService,$state,$scope,QueryService){
        'use strict';
        $scope.farmerData = {};
        $scope.showSearchBar = true;
        $scope.event = {};
        QueryService.query('GET','/event_types',{},{}).then(function(response){
            console.log(response);
            if(response.status ==  200){
                $scope.eventTypes = response.data;
            }else{
                MessageService.error("Failed to connect server...");
            }
        },function(error){
           MessageService.error(error.data);
        });

        $scope.initEventSubType = function(){

            QueryService.query('GET','/event_subtypes/'+$scope.event.type,{},{}).then(function(response){
                console.log(response.data);
                if(response.status ==  200){
                    $scope.eventSubtypes = response.data;
                    console.log(response.data);
                }else{
                    MessageService.error("Failed to connect server...");
                }
            },function(error){
               MessageService.error(error.data);
            });
        };
        $scope.searchEvent = function(eventData){
            var fpoId = localStorage.fpoId;
            var queryString = "";
            if(eventData.startDate){
                eventData.start_date = moment(eventData.startDate).format('YYYY-MM-DD');
            };
            delete eventData.startDate;
            var count=0;
                if(eventData){
                    for (var data in eventData ){
                        if(eventData[data]){
                            if(count > 0 ){
                                queryString += "&";
                            };
                            queryString += data+"="+eventData[data];
                        };
                        count++;
                    };
                };

                QueryService.query('GET','/event/search'+'?'+'fpo='+fpoId+'&'+queryString,{},{}).then(function(response){
                    if(response.status == 200){
                        $scope.showSearchBar = false;
                        $scope.eventsData = response.data;
                    }else{
                        MessageService.error("Error in searching Event data...");
                    }
                },function(error){
                    MessageService.error(error.data);
                });
        };
        $scope.searchAgain = function(){
            $scope.showSearchBar = true;
        };

        $scope.selectEvent = function(event){
            var eventId = event.ID;
            $state.go('eventDetails',{'eventId':eventId});
        };

    }]);
