angular.module('fpoApp.controllers')
    .controller('EventListCtrl', ['$scope', '$stateParams', '$state', 'QueryService', 'MessageService',
        function($scope, $stateParams, $state, QueryService, MessageService) {
            if (window.localStorage.getItem('fpoName')) {
                $scope.fpoName = window.localStorage.getItem('fpoName');
            }
            var fpoId = $stateParams.fpoId;
            console.log(fpoId);

            //call API to get upcoming events on page load or refresh......
            QueryService.query('GET', '/fpos/' + fpoId + '/event_summary', {}, {}).then(function(response) {
                if (response.status == 200) {
                    console.log(response.data);
                    $scope.upcoming_events = response.data;
                }
            }, function(error) {
                MessageService.error("Error in fetching upcoming events.");
            })

            //call api to get meetings.....
            $scope.getEvents = function(event) {
                //console.log(event);
                $scope.events = [];
                QueryService.query('GET', '/fpos/' + fpoId + '/events/' + event, {}, {}).then(function(response) {
                    $scope.events = response.data;
                    console.log(response.data);
                }, function(error) {
                    MessageService.error(response.data);
                }); //end of QueryService....

            }

            //this function will call when user click on plus button to create new event....
            $scope.eventCreate = function(event) {
                console.log(event);
                $state.go('create-event', {
                    event_type: event
                });
            }

            //this function will used when user click any event.....
            $scope.eventDetails = function(event) {
                var eventId = event.id;
                $state.go('eventDetails', {
                    eventId: eventId
                });
            }
        }
    ])

.controller('EventDetailsCtrl', ['$scope', '$stateParams', '$state', '$sce', 'QueryService', 'MessageService', 'shareDataService',
    function($scope, $stateParams, $state, $sce, QueryService, MessageService, shareDataService) {

        var eventId = $stateParams.eventId;
        $scope.eventId = eventId;
        $scope.fpoId = fpoId = window.localStorage.getItem('fpoId');
        console.log(eventId)
        $scope.address_title = "Event Address";

        //this QueryService will call everytime when this page will load and fetch all details of particular event...
        QueryService.query('GET', '/events/' + eventId, {}, {})
            .then(function(response) {
                console.log(response.data);
                $scope.eventDetails = response.data;
                $scope.address = response.data.location;
                //store event_name for add address screen....
                window.localStorage.setItem('eventName', response.data.name);
            }, function(error) {
                MessageService.error(JSON.stringify(error.data));
            });

        //on button click event add address.....
        $scope.goAddress = function() {
            $state.go('add-event-address', { eventId: eventId });
        }

        //this button will open page to update address of event......
        $scope.updateAddress = function() {
            $state.go('update-event-address', { eventId: eventId });
        }


        $scope.openUpdateScreen = function() {
            $state.go('update-event', { eventId: eventId });
        }

        $scope.goToInvite = function(invitation) {

            $state.go('event-invite', { invite: JSON.stringify({ eventId: eventId, inviteType: invitation }) });
        }

        $scope.getAssociatedMedia = function() {
            QueryService.query('GET', '/event/' + eventId + '/associat_media/', {}, {})
                .then(function(response) {
                    if (response.status == 200) {
                        $scope.associatMedia = response.data;
                    }
                }, function(error) {
                    MessageService.error(error.data);
                })
        };

        // Trust the video url;
        $scope.trustSrc = function(src) {
            return $sce.trustAsResourceUrl(src);
        };
        $scope.addAssociatedMedia = function() {
            $state.go('add-associated-media', { 'eventId': eventId });
        };

        //on clicking this button, open new page....
        $scope.goToTrackAttendance = function() {
            $state.go('event_attendance', { eventId: eventId });
        }

        $scope.getGroups = function(group_code) {
            //console.log(group_code);
            $scope.fpo_groups = {};
            QueryService.query('GET', '/fpos/' + fpoId + '/groups/', {}, {}).then(function(response) {
                if (response.status == 200) {
                    console.log(response.data);
                    $scope.fpo_groups = response.data;
                }
            }, function(error) {
                MessageService.error(error.data);
            })
        };

    }
])

.controller('EventInviteCtrl', ['$scope', '$stateParams', '$state', 'QueryService', 'MessageService',
    function($scope, $stateParams, $state, QueryService, MessageService) {

        var fpoId = window.localStorage.getItem('fpoId');
        $scope.invite = JSON.parse($stateParams.invite);
        var eventId = $scope.invite.eventId;
        var inviteType = $scope.invite.inviteType;
        $scope.eventInvitation = {};
        //console.log(eventId);

        $scope.selected = [];
        var preSelected = [];

        //set custom settings and dropdown button text according to inviteType variable....

        if (inviteType == "groups") {
            $scope.screen_title = "Groups";
            $scope.customButtomText = { buttonDefaultText: 'Select Groups' };
            $scope.customListSettings = { displayProp: 'listTypeWithName', idProp: 'id', enableSearch: true };
        } else {
            $scope.screen_title = "Farmers";
            $scope.customButtomText = { buttonDefaultText: 'Select Farmers' };
            $scope.customListSettings = { displayProp: 'memberName', idProp: 'memberId', enableSearch: true };
        }

        //call API to get event datails for already invited groups or farmers......
        QueryService.query('GET', '/events/' + eventId, {}, {}).then(function(response) {
            if (response.status == 200) {
                console.log(response.data);
                if (response.data.participant_groups.length > 0 && inviteType == "groups") {
                    var selected = response.data.participant_groups.map(function(obj) {
                        var rObj = {};
                        rObj["id"] = obj.id;
                        return rObj;
                    });
                    $scope.selected = selected;
                    preSelected = response.data.participant_groups.map(function(obj) {
                        var rObj = {};
                        rObj = obj.id;
                        return rObj;
                    });
                } else if (response.data.participant_persons.length > 0 && inviteType == "farmers") {
                    var selected = response.data.participant_persons.map(function(obj) {
                        var rObj = {};
                        rObj["id"] = obj.id;
                        return rObj;
                    });
                    $scope.selected = selected;
                    preSelected = response.data.participant_persons.map(function(obj) {
                        var rObj = {};
                        rObj = obj.id;
                        return rObj;
                    });
                }
            }
        }, function(error) {
            MessageService.error("Error in fetching event details");
        })

        //below API will call according to inviteType variable....
        if (inviteType == "groups") {
            //this API will get All groups associated with FPO.....
            //this api will fetch all fpo groups.....
            QueryService.query('GET', '/fpos/' + fpoId + '/groups', {}, {}).then(function(response) {
                if (response.status == 200) {
                    console.log(response.data);
                    $scope.list = response.data;
                    angular.forEach($scope.list, function(value, key) {
                        $scope.list[key].listTypeWithName = value.typeName + ": " + value.name;
                    })
                }
            }, function(error) {
                MessageService.error(JSON.stringify(error.data));
            }); //end of this queryService ....

        } else {
            //this api will fetch all farmers from fpo.....
            QueryService.query('GET', '/fpos/' + fpoId + '/farmers', {}, {}).then(function(response) {
                if (response.status == 200) {
                    $scope.list = response.data;
                }
            }, function(error) {
                MessageService.error(error.data);
            }); //end of this api....
        } //end of API getting farmers of groups list......

        //function that will call API to invite more selected groups or farmers.....
        $scope.inviteForEvent = function() {
                $scope.selected = $scope.selected.map(function(obj) {
                    var rObj = {};
                    rObj = obj.id;
                    return rObj;
                });

                //find new values from array to be add.....
                $scope.new_selected = _($scope.selected).difference(preSelected);
                $scope.delete_selected = _(preSelected).difference($scope.selected);

                if (inviteType == "groups") {
                    if ($scope.new_selected.__wrapped__.length > 0) {
                        $scope.eventInvitation.participant_groups_add = $scope.new_selected.__wrapped__;
                    }

                    if ($scope.delete_selected.__wrapped__.length > 0) {
                        $scope.eventInvitation.participant_groups_delete = $scope.delete_selected.__wrapped__;
                    }

                } else {
                    if ($scope.new_selected.__wrapped__.length > 0) {
                        $scope.eventInvitation.participant_farmers_add = $scope.new_selected.__wrapped__;

                    }

                    if ($scope.delete_selected.__wrapped__.length > 0) {
                        $scope.eventInvitation.participant_farmers_delete = $scope.delete_selected_farmer.__wrapped__;
                    }

                }

                console.log($scope.eventInvitation);

                //call update API........
                QueryService.query('PUT', '/event/' + eventId, {}, $scope.eventInvitation).then(function(response) {
                    if (response.status == 200) {
                        //MessageService.success("Event updated successfully.");
                        window.history.back();
                    }
                }, function(error) {
                    MessageService.error(JSON.stringify(error.data));
                }); //end of API call.....


            } //end of this function.....

    }
])

.controller('CreateEventCtrl', ['$scope', '$stateParams', '$state', 'QueryService', 'MessageService',
        function($scope, $stateParams, $state, QueryService, MessageService) {

            var event_type = $stateParams.event_type;
            var fpoId = window.localStorage.getItem('fpoId');
            $scope.event_type_name = {};
            $scope.event = {};
            $scope.customSettings = { displayProp: 'memberName', idProp: 'memberId' };
            $scope.customGroupSettings = { displayProp: 'groupTypeWithName', idProp: 'id' };
            $scope.selected_farmer = [];
            $scope.selected_groups = [];
            console.log(event_type);
            if (event_type == 'MEETING') {
                console.log("meeting");
                $scope.screen_title_name = "Create Event Meeting";
                $scope.event_type_name = "Meeting";
            } else {
                console.log("training");
                $scope.screen_title_name = "Create Event Training";
                $scope.event_type_name = "Training";
            }

            $scope.minDate = new Date();

            //this api will call on page load, and fetch all mgmt_users for contact_person details....
            QueryService.query('GET', '/fpos/' + fpoId + '/roles/' + "FPO-MANAGER", {}, {}).then(function(response) {
                console.log(response.data);
                if (response.status == 200) {
                    $scope.contact_persons = response.data;
                } else {
                    MessageService.error("Error in fetching data");
                }

            }, function(error) {
                MessageService.error(error.data);
            }); //end of this api response....

            //this api will fetch all farmers from fpo.....
            QueryService.query('GET', '/fpos/' + fpoId + '/farmers', {}, {}).then(function(response) {
                if (response.status == 200) {
                    $scope.farmers = response.data;
                }
            }, function(error) {
                MessageService.error(error.data);
            }); //end of this api....

            //this api will fetch all fpo groups.....
            QueryService.query('GET', '/fpos/' + fpoId + '/groups', {}, {}).then(function(response) {
                if (response.status == 200) {
                    console.log(response.data);
                    $scope.groups = response.data;
                    angular.forEach($scope.groups, function(value, key) {
                        $scope.groups[key].groupTypeWithName = value.typeName + ": " + value.name;
                    })
                }
            }, function(error) {
                MessageService.error(JSON.stringify(error.data));
            }); //end of this queryService ....

            //on cancel button click.....
            $scope.goBack = function() {
                window.history.back();
            }

            //on submit button click.....
            $scope.submitEvent = function() {
                var date = moment($scope.start_date).format("YYYY-MM-DD");
                var start_time = moment($scope.start_time).format("H:mm:ss");
                var end_time = moment($scope.end_time).format("H:mm:ss");
                var start_date_time = moment(date + " " + start_time, "YYYY-MM-DD H:mm:ss");
                var end_date_time = moment(date + " " + end_time, "YYYY-MM-DD H:mm:ss");

                //get datetime from Moment object....
                $scope.event.start_date_time = start_date_time._d;
                $scope.event.end_date_time = end_date_time._d;

                console.log($scope.event.start_date_time);
                //this method will fetch all selected_farmers id only....
                angular.forEach($scope.selected_farmer, function(value, key) {
                    $scope.selected_farmer[key] = value.id;
                });
                angular.forEach($scope.selected_groups, function(value, key) {
                    $scope.selected_groups[key] = value.id;
                });
                $scope.event.type = event_type;
                if ($scope.selected_farmer.length > 0) {
                    $scope.event.participant_farmers = $scope.selected_farmer;
                }
                if ($scope.selected_groups.length > 0) {
                    $scope.event.participant_groups = $scope.selected_groups;
                }

                if ($scope.event.$invalid || $scope.event.$error || $scope.event.$dirty) {
                    console.log($scope.event);
                    return;
                } else {
                    console.log($scope.event);
                    QueryService.query('POST', '/fpos/' + fpoId + '/event', {}, $scope.event).then(function(response) {
                        if (response.status == 200) {
                            MessageService.success("Event created successfully.");
                            window.history.back();
                        }
                    }, function(error) {
                        MessageService.error(JSON.stringify(error.data))
                    })
                }
            }

        }
    ])
    .controller('UpdateEventCtrl', ['$scope', '$stateParams', '$state', 'QueryService', 'MessageService',
        function($scope, $stateParams, $state, QueryService, MessageService) {

            var fpoId = window.localStorage.getItem('fpoId');
            var eventId = $stateParams.eventId;
            var preselected_farmers = [];
            var preselected_groups = [];
            var event_type;
            //$scope.screen_title_name = "Update Event";
            $scope.customSettings = { displayProp: 'memberName', idProp: 'memberId' };
            $scope.customGroupSettings = { displayProp: 'groupTypeWithName', idProp: 'id' };
            $scope.selected_farmer = [];
            $scope.selected_groups = [];


            //this QueryService will call everytime when this page will load and fetch all details of particular event...
            QueryService.query('GET', '/events/' + eventId, {}, {}).then(function(response) {
                if (response.status == 200) {
                    console.log(response.data);
                    $scope.event = response.data;
                    //$scope.event.subtype = response.data.subtype.code;
                    if (response.data.contact) {
                        $scope.event.contact_person = response.data.contact.id;
                    }
                    //$scope.event.contact_person = response.data.contact.id;
                    $scope.event_type_name = response.data.type.name;
                    //set type in variable....
                    event_type = response.data.type.code;

                    if (response.data.participant_groups.length > 0) {
                        var selected_groups = response.data.participant_groups.map(function(obj) {
                            var rObj = {};
                            rObj["id"] = obj.id;
                            return rObj;
                        });
                        $scope.selected_groups = selected_groups;
                        preselected_groups = response.data.participant_groups.map(function(obj) {
                            var rObj = {};
                            rObj = obj.id;
                            return rObj;
                        });

                    }
                    if (response.data.participant_persons.length > 0) {
                        var selected_farmer = response.data.participant_persons.map(function(obj) {
                            var rObj = {};
                            rObj["id"] = obj.id;
                            return rObj;
                        });
                        $scope.selected_farmer = selected_farmer;
                        preselected_farmers = response.data.participant_persons.map(function(obj) {
                            var rObj = {};
                            rObj = obj.id;
                            return rObj;
                        });
                    }
                    //set screen title...
                    $scope.screen_title_name = "Update " + response.data.name;
                    //format date to bind with model ....
                    $scope.start_date = new Date(response.data.start_date_time);
                    $scope.start_time = new Date(response.data.start_date_time);
                    $scope.end_time = new Date(response.data.end_date_time);
                }
            }, function(error) {
                MessageService.error(JSON.stringify(error.data));
            });


            //this api will call on page load, and fetch all mgmt_users for contact_person details....
            QueryService.query('GET', '/fpos/' + fpoId + '/roles/' + "FPO-MANAGER", {}, {}).then(function(response) {
                console.log(response.data);
                if (response.status == 200) {
                    $scope.contact_persons = response.data;
                } else {
                    MessageService.error("Error in fetching data");
                }

            }, function(error) {
                MessageService.error(error.data);
            }); //end of this api response....

            //this api will fetch all fpo groups.....
            QueryService.query('GET', '/fpos/' + fpoId + '/groups', {}, {}).then(function(response) {
                if (response.status == 200) {
                    console.log(response.data);
                    $scope.groups = response.data;
                    angular.forEach($scope.groups, function(value, key) {
                        $scope.groups[key].groupTypeWithName = value.typeName + ": " + value.name;
                    })
                }
            }, function(error) {
                MessageService.error(JSON.stringify(error.data));
            }); //end of this queryService ....

            //this api will fetch all farmers from fpo.....
            QueryService.query('GET', '/fpos/' + fpoId + '/farmers', {}, {}).then(function(response) {
                if (response.status == 200) {
                    $scope.farmers = response.data;
                }
            }, function(error) {
                MessageService.error(error.data);
            }); //end of this api....

            //on check button click......
            $scope.submitEvent = function() {
                    //set event type into object.....
                    $scope.event.type = event_type;

                    var date = moment($scope.start_date).format("YYYY-MM-DD");
                    var start_time = moment($scope.start_time).format("H:mm:ss");
                    var end_time = moment($scope.end_time).format("H:mm:ss");
                    var start_date_time = moment(date + " " + start_time, "YYYY-MM-DD H:mm:ss");
                    var end_date_time = moment(date + " " + end_time, "YYYY-MM-DD H:mm:ss");

                    //get datetime from Moment object....
                    $scope.event.start_date_time = start_date_time._d;
                    $scope.event.end_date_time = end_date_time._d;

                    $scope.selected_farmer = $scope.selected_farmer.map(function(obj) {
                        var rObj = {};
                        rObj = obj.id;
                        return rObj;
                    });
                    $scope.selected_groups = $scope.selected_groups.map(function(obj) {
                        var rObj = {};
                        rObj = obj.id;
                        return rObj;
                    });
                    //find new values from array to be add.....
                    $scope.new_selected_farmer = _($scope.selected_farmer).difference(preselected_farmers);
                    $scope.new_selected_groups = _($scope.selected_groups).difference(preselected_groups);
                    $scope.delete_selected_farmer = _(preselected_farmers).difference($scope.selected_farmer);
                    $scope.delete_selected_groups = _(preselected_groups).difference($scope.selected_groups);

                    if ($scope.new_selected_farmer.__wrapped__.length > 0) {
                        $scope.event.participant_farmers_add = $scope.new_selected_farmer.__wrapped__;
                    };
                    if ($scope.new_selected_groups.__wrapped__.length > 0) {
                        $scope.event.participant_groups_add = $scope.new_selected_groups.__wrapped__;
                    };
                    if ($scope.delete_selected_farmer.__wrapped__.length > 0) {
                        $scope.event.participant_farmers_delete = $scope.delete_selected_farmer.__wrapped__;
                    };
                    if ($scope.delete_selected_groups.__wrapped__.length > 0) {
                        $scope.event.participant_groups_delete = $scope.delete_selected_groups.__wrapped__;
                    };

                    //check event object that will be send to backend....
                    if ($scope.event.$invalid || $scope.event.$error || $scope.event.$dirty) {
                        return;
                    } else {
                        console.log($scope.event);
                        //delete unwanted parameters....
                        delete $scope.event.contact;
                        delete $scope.event.end_time;
                        delete $scope.event.fpo;
                        delete $scope.event.id;
                        delete $scope.event.mom;
                        delete $scope.event.start_time;
                        delete $scope.event.status;
                        delete $scope.event.participant_persons;
                        delete $scope.event.participant_groups;
                        delete $scope.event.location;

                        //call API to update event.....
                        QueryService.query('PUT', '/event/' + eventId, {}, $scope.event).then(function(response) {
                            if (response.status == 200) {
                                MessageService.success("Event updated successfully.");
                                window.history.back();
                            }
                        }, function(error) {
                            MessageService.error(JSON.stringify(error.data));
                        }); //end of API call.....
                    } //end of if-else condition...
                } //end of submitEvent function...

        }
    ])

.controller('EventAssociatedMediaCtrl', ['$scope', '$stateParams', '$state', '$timeout',
    'fileUpload', 'QueryService', 'MessageService',
    function($scope, $stateParams, $state, $timeout,
        fileUpload, QueryService, MessageService) {
        $scope.headerText = "Upload the event media";
        $scope.uploadFile = function(file) {
            var APIUrl = "/event/" + $stateParams.eventId + "/associat_media";
            if (file) {
                fileUpload.uploadFile(file, "eventMedia", APIUrl,
                    function(response) {
                        var url = response.data.Url;
                        if (!url) {
                            MessageService.info('Unable to Uploaded File. Please try again!');
                        } else {
                            $scope.showSaveButton = true;
                            MessageService.success('File Uploaded successfully');
                        }
                    })
            } else {
                MessageService.info("Please select the file!");
            }
        }

        $scope.photoChanged = function(files) {
            // $scope.showSaveButton = true;
            fileUpload.changeFile($scope, files);
        };

        $scope.saveFile = function() {
            if ($scope.selectedFile) {
                $scope.uploadFile($scope.selectedFile);
            } else {
                MessageService.info("Please select a file!");
            };
        };

        $scope.removeFile = function(file) {
            $scope.thumbnail.dataUrl = "http://fakeimg.pl/250x100/CCC/"
        };

        $scope.deleteFile = function(url) {
            QueryService.query('PATCH', '/event/' + $stateParams.eventId + '/associat_media', {}, { 'url': fileUrl })
                .then(function(response) {
                    if (typeof cb == 'function') {
                        cb(response)
                    };
                }, function(error) {
                    MessageService.error(error.data);
                });
        };

    }
])

.controller('EventAttendanceCtrl', ['$scope', '$stateParams', '$state', 'QueryService', 'MessageService',
        function($scope, $stateParams, $state, QueryService, MessageService) {
            var eventId = $stateParams.eventId;
            $scope.customSettings = { displayProp: 'fullame', idProp: 'attendance_id' };
            $scope.customDropdownTexts = { buttonDefaultText: 'Select Members' };
            $scope.selected_farmers = [];
            $scope.attendance = {};
            var attended_farmers, absent_farmers, invited_farmers;

            //get fpo_members list who has invited fro event.....
            QueryService.query('GET', '/events/' + eventId, {}, {})
                .then(function(response) {
                    if (response.status == 200) {
                        $scope.farmers = response.data.participant_persons;
                        console.log(response.data.participant_persons);
                        invited_farmers = $scope.farmers.map(function(obj) {
                            var rObj = {};
                            rObj = obj.attendance_id;
                            return rObj;
                        });
                        console.log(invited_farmers);
                    }
                }, function(error) {
                    MessageService.error(error.data);
                }); //end of get API....

            //on Apply button click...
            $scope.applySelected = function() {
                if ($scope.event == "ATTENDED") {
                    console.log($scope.event);
                    attended_farmers = $scope.selected_farmers.map(function(obj) {
                        var rObj = {};
                        rObj = obj.id;
                        return rObj;
                    });

                    //get absent array....
                    absent_farmers = _(invited_farmers).difference(attended_farmers);
                    //absent_farmers = _(absent_farmers).difference(invited_farmers);
                    console.log(absent_farmers);

                    $scope.attendance.ATTENDED = attended_farmers;
                    $scope.attendance.ABSENT = absent_farmers.__wrapped__;
                } else {
                    console.log($scope.event);
                    absent_farmers = $scope.selected_farmers.map(function(obj) {
                        var rObj = {};
                        rObj = obj.id;
                        return rObj;
                    });
                    console.log(absent_farmers);
                    //get attended farmer array....
                    attended_farmers = _(invited_farmers).difference(absent_farmers);
                    $scope.attendance.ATTENDED = attended_farmers.__wrapped__;
                    $scope.attendance.ABSENT = absent_farmers;
                }
                console.log($scope.attendance);

                //call API to update attendance of farmer.....
                QueryService.query('PUT', '/event/' + eventId + '/attendance', {}, $scope.attendance)
                    .then(function(response) {
                        if (response.status == 200) {
                            console.log(response);
                            window.history.back();
                        }
                    }, function(error) {
                        MessageService.error(error.data);
                    }); //end of API call....
            }; //end of method......

        }
    ]) //end of controller....
