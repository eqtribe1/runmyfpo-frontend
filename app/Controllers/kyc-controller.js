angular.module('fpoApp.controllers')//,['ngCookies'])
.controller('ListKycCtrl',['$scope','$stateParams', 'QueryService','MessageService',
  function ($scope, $stateParams, QueryService,MessageService){

  	$scope.kycDoc = [];

  	//call KycService method list all KYC Documents......
  	//var farmerId = JSON.parse(window.localStorage['farmerDetails'] || '{}').farmerDetails.id;//$cookieStore.get('farmerDetails').farmerDetails.id;
			//console.log(farmerId);

   // QueryService.query('GET','/farmers/'+farmerId+'/kyc',{},{}).then(function(response){
  	// 	$scope.kycDoc = response;
  	// })
   //
   // $scope.add_new_kyc_doc = function(){
   //  $state.go('/add-kyc');
   // }
  }])
.controller('AddKycCtrl',['$scope','$stateParams','QueryService','MessageService',
	function($scope,$stateParams,QueryService,MessageService){

  var farmerId = $stateParams.farmerId;
		$scope.kyc_doc_types = [];

		//call KycService to get KYC document_types..........
  QueryService.query('GET','/kyc_doc_types',{},{}).then(function(response){
			//console.log(response);
   if(response.status ==  200){
     $scope.kyc_doc_types = response.data;
   }else{
    MessageService.error("Failed to connect server...");
   }
		},function(error){
   MessageService.error(error.data);
  });

		$scope.submitKyc = function(){
			//var farmerId = window.localStorage.getItem('farmerId');
			console.log(farmerId);
			if($scope.kyc.doc_no && $scope.kyc.doc_type != null){
				console.log("Inside If "+$scope.kyc);
				//KycService method to add new kyc document......
    QueryService.query('POST','/farmers/'+farmerId+'/kyc',{},$scope.kyc).then(function(response){
					//console.log(response);
					if(response.status == 200){
						//$state.go('/moreDetails');
      window.history.back();
					}else{
      MessageService.error("Error in saving kyc data..");
						//$state.go('/add-kyc/'+farmerId);
					}
				},function(error){
     MessageService.error(error.data);
    })
			}else{
				//console.log("Inside else "+$scope.kyc);
    MessageService.error("Please fill all required fields!!");
			}
		};//end of this method.....

  $scope.goBack = function() {
   window.history.back();
   //$state.go('/moreDetails');
  };

	}])
.controller('UpdateKycCtrl',['$scope','$stateParams','QueryService','MessageService',
	function($scope,$stateParams,QueryService,MessageService){
		$scope.oldKyc = {};
  console.log(JSON.parse($stateParams.kyc))
  $scope.oldKyc = JSON.parse($stateParams.kyc);
		// $scope.oldKyc.doc_no = $stateParams.doc_no;
		// $scope.oldKyc.doc_type = $stateParams.doc_type;
   var kycId = $scope.oldKyc.id;
   delete $scope.oldKyc.owner;

		$scope.kyc_doc_types = [];

  $scope.deleteKycDoc = function(){
    QueryService.query('DELETE','/kyc/'+kycId,{},{}).then(function(response){
       if(response.status ==  200){
          // $scope.kyc_doc_types = response.data;
          MessageService.success("KYC document deleted successfully")
          $scope.goBack();
       }else{
          MessageService.error("Failed to connect server...");
       }
    },function(error){
      MessageService.error(error.data);
    });
  }

		//call KycService to get KYC document_types..........
  QueryService.query('GET','/kyc_doc_types',{},{}).then(function(response){
    if(response.status ==  200){
      $scope.kyc_doc_types = response.data;
    }else{
      MessageService.error("Failed to connect server...");
    }
	},function(error){
    MessageService.error(error.data);
  });

		$scope.updateKyc = function(){
			console.log($scope.oldKyc);
   QueryService.query('PUT','/kyc/'+kycId,{},$scope.oldKyc).then(function(response){
				if(response.status == 200){
     window.history.back();
				}else{
     MessageService.error("error in saving data...");
				}
			},function(error){
    console.log(error.data);
    MessageService.error("error in saving data...");
   })
		};
  $scope.goBack = function() {
   window.history.back();
  };

	}])
