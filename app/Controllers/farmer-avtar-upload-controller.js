angular.module('fpoApp.controllers')
    .controller('FarmerAvtarUpdateCtrl', ['$scope', '$timeout', '$stateParams', 'fileUpload', 'QueryService', 'MessageService',
        function($scope, $timeout, $stateParams, fileUpload, QueryService, MessageService) {

            $scope.headerText = "Update profile pic of " + localStorage.getItem('farmerName');
            var personId = $stateParams.personId;
            $scope.fileUrl = window.localStorage.getItem('farmerProfilePic');
            $scope.thumbnail = {};
            $scope.uploadFile = function(file) {
                console.log(file);

                var APIUrl = "/person/upload_avtar/" + personId;

                if (file) {
                    fileUpload.uploadFile(file, 'image', APIUrl, function(response) {
                        var url = response.data.profilePicUrl;
                        console.log(url);
                        if (!url) {
                            MessageService.info('Unable to Uploaded Image. Please try again!');
                        } else {
                            $scope.removePreviousProfilePic($scope.fileUrl);
                            localStorage.setItem('farmerProfilePic', url);
                            MessageService.success('Image Uploaded successfully');
                        }
                    })
                } else {
                    MessageService.info("Please select the file!");
                }
            }

            $scope.photoChanged = function(files) {
                fileUpload.changeFile($scope, files);
            };

            $scope.saveFile = function() {
                if ($scope.selectedFile) {
                    $scope.uploadFile($scope.selectedFile);
                } else {
                    MessageService.info("Please select a file!");
                };
            };

            $scope.removeFile = function(file) {
                $scope.thumbnail.dataUrl = "http://fakeimg.pl/250x100/CCC/"
            };
            $scope.removePreviousProfilePic = function(url) {
                var apiUrl = "/file/delete/";
                if (url && (typeof(url) == 'string') && (url.length > 20)) {
                    fileUpload.removeFile(apiUrl, url, 'PUT', function(res) {
                        if (response.status == 200) {
                            MessageService.success("File Deleted successfully");
                        };
                    });
                }
            };
        }
    ])
