angular.module('fpoApp.controllers')
.controller('areaModalCtrl', ['$scope', '$stateParams', '$state','areaMultiSelect','QueryService',
                              'MessageService','shareDataService',
             function($scope, $stateParams, $state, areaMultiSelect,
               QueryService, MessageService,shareDataService) {

      if($state.current.name === 'fpoGroupDetails'){
         var data = {} ;
         var villages = shareDataService.getList("area_association");
         console.log(villages);
         $scope.selected_states = [{ "id" : 'KA'}];

         $scope.selected_villages = _.map(villages, function(village){
                                       return { "id" : village.id }
         })

         $scope.selected_districts = _.map(villages, function(village){
                                       return { "id" : village.district }
         })
      } else {
        $scope.selected_states = $scope.selected_states || [];
        $scope.selected_districts = $scope.selected_districts || [];
        $scope.selected_villages = $scope.selected_villages|| [];
      }

      $scope.customStateSettings = areaMultiSelect.customStateSettings;
      $scope.customDistrictSettings = areaMultiSelect.customDistrictSettings;
      $scope.customVillageSettings = areaMultiSelect.customVillageSettings;

        QueryService.query('GET', '/countries/INDIA/states', {}, {})
          .then(function(response) {
              if (response.status == 200) {
                  $scope.states = response.data;
              }
          }, function(error) {
              MessageService.error(error.data);
          });
          var state_ids = _.map($scope.selected_states,
                            function(state) {
                               return state.id;
                            });
                            console.log(state_ids);
          QueryService.query('PATCH', '/state_districts', {}, {
                            "ids": state_ids
                            })
                           .then(function(response) {
                              if (response.status == 200) {

                                 $scope.districts = response.data ;
                                 console.log($scope.districts);
                              }
                           }, function(error) {
                             MessageService.error(error.data);
                             })
                             var dist_ids = _.map($scope.selected_districts, function(dist) {
                                 return dist.id
                             });
               QueryService.query('PATCH', '/district_villages', {}, {
                       "ids": dist_ids
                   })
                   .then(function(response) {
                       if (response.status == 200) {
                           $scope.villages = response.data;
                           console.log($scope.villages);
                           angular.forEach($scope.villages, function(value, key) {
                               $scope.villages[key].distVillage = value.village_name + " : " + value.village_district;
                           })
                       }
                   }, function(error) {
                       MessageService.error(error.data);
                   })
$scope.state_list = {
 onItemSelect: function(item) {
      var state_ids = _.map($scope.selected_states,
                        function(state) {
                           return state.id;
                        });
                        console.log(state_ids);
      // QueryService.query('GET', '/villages/in/states?ids=('+"'KA','UP','BI'"+");", {}, {})
      //             .then(function(response) {
      //               var districts = [];
      //               var villages = [];
      //             if (response.status == 200) {
      //              _.map(response.data.villages, function(element,key){
      //                console.log(element);
      //                   districts.push({ "district_code": element[key].district ,
      //                                    "district_name": element[key].name });
      //
      //                         //   _.map(response.data.villages, function(village){
      //                         //        villages.push({ "village_id": village.id ,
      //                         //           "village": village.village })
      //                         // })
      //                       })
      //                console.log(districts);
      //             //   console.log(villages);
      //                $scope.districts = districts;
      //                $scope.villages = villages;
      //                      }
      //                }, function(error) {
      //                   MessageService.error(error.data);
      //               });
     QueryService.query('PATCH', '/state_districts', {}, {
                       "ids": state_ids
                       })
                      .then(function(response) {
                         if (response.status == 200) {

                            $scope.districts = response.data ;
                            console.log($scope.districts);
                         }
                      }, function(error) {
                        MessageService.error(error.data);
                        })
     }
 }

           $scope.district_list = {
               onItemSelect: function(item) {
                   var dist_ids = _.map($scope.selected_districts, function(dist) {
                       return dist.id
                   });
                   QueryService.query('PATCH', '/district_villages', {}, {
                           "ids": dist_ids
                       })
                       .then(function(response) {
                           if (response.status == 200) {
                               $scope.villages = response.data;
                               console.log($scope.villages);
                               angular.forEach($scope.villages, function(value, key) {
                                   $scope.villages[key].distVillage = value.village_name + " : " + value.village_district;
                               })
                           }
                       }, function(error) {
                           MessageService.error(error.data);
                       })
               }
           };

         $scope.village_list = {
         onChange: function(item) {
                   var village_ids = _.map($scope.selected_villages, function(village) {
                   return village.id;
                   });
              $scope.selected_villages = village_ids;
              }
         };
        $scope.completeModal = function() {
          if($state.current.name === 'fpoGroupDetails'){
          console.log($stateParams.fpogroupId);
          QueryService.query('PATCH', '/group_area/'+$stateParams.fpogroupId, {}, {
                  "villages": _.map($scope.selected_villages, function(item){
                                     return item.id })
              })
              .then(function(response) {
                  if (response.status == 200) {
                      MessageService.success("Area Association updated successfully");
                      $state.reload();
                  }
              }, function(error) {
                  MessageService.error(error.data);
              })
            }
           console.log($scope.selected_villages);
           areaModal.dismiss('OK');
        };

        $scope.cancel = function() {
            areaModal.dismiss('cancel');
        };
    }
]);
