angular.module('fpoApp.controllers')

.controller('FpoSelectCtrl', ['$scope', '$state', 'QueryService', 'MessageService',
        function($scope, $state, QueryService, MessageService) {

            $scope.fpo = {};
            $scope.fpos = [];
            //this fpoService will call to fetch the FPO Name from database inwhich loggedIn user......
            $scope.userName = window.localStorage.getItem('userName');
            console.log($scope.userName);
            //console.log($scope.userId);
            //$scope.fpos = JSON.parse(window.localStorage.getItem('fpos'));

            var personId = window.localStorage.getItem('personId');
            QueryService.query('GET', '/fpo_lists/' + personId, {}, {}).then(function(response) {
                if (response.status == 200) {
                    $scope.fpos = response.data;
                    console.log(response.data);
                }
            }, function(error) {
                MessageService.error(error.data);
            });


            $scope.selectFpo = function(fpo) {
                console.log(fpo);
                window.localStorage.setItem('fpoId', fpo.fpoId);
                window.localStorage.setItem('fpoName', fpo.fpoName);

                $state.go("fpoHome");
            }
        }
    ])
    .controller('FpoHomeCtrl', ['$scope', '$state', 'QueryService', 'MessageService',
        function($scope, $state, QueryService, MessageService) {
            $scope.fpoName = window.localStorage.getItem('fpoName');
            var fpoId = window.localStorage.getItem('fpoId');

            $scope.getFpoDetails = function() {
                $state.go('fpoDetails', { fpoId: fpoId });
            };

            $scope.getFpoMembers = function() {
                //this service method will used to call backend API, that will fetch fpo members list according fpoID...
                $state.go('fpoMemberList', { fpoId: fpoId });
            }
            $scope.gotoEvents = function() {
                $state.go('eventsList', { fpoId: fpoId });
            }

            $scope.outputPurchaseRecord = function() {
                $state.go('output-purchase-invoice');
            }

            $scope.getShopDashboard = function() {
                $state.go('shop-dashboard');
            }
        }
    ])
    .controller('fpoCreateCtrl', ['$scope', '$state', 'QueryService', 'MessageService',
        function($scope, $state, QueryService, MessageService) {

            $("#date_joined").datetimepicker({
                showClear: true,
                keepOpen: false,
                focusOnShow: false,
                format: 'DD-MM-YYYY',
                ignoreReadonly: true,
                maxDate: moment()
            });

            //this method will call to create fpo......
            $scope.addFpo = function() {
                    $scope.fpo.person = window.localStorage.getItem('personId');

                    $scope.fpo.date_incorporation = new Date(moment($('#date_joined').val(), "DD-MM-YYYY"));

                    if ($scope.fpo.name && $scope.fpo.description != null) {
                        QueryService.query('POST', '/fpos', {}, $scope.fpo)
                            .then(function(response) {
                                console.log('fpo');
                                if (response.status == 200) {
                                    window.localStorage.setItem('fpos', JSON.stringify(response.data.FPOs));
                                    $state.go('fpoSelect');
                                } else {
                                    MessageService.error(JSON.stringify(response.data));
                                    $state.go('fpoCreate');
                                }
                            }, function(error) {
                                if (error.status == -1) {
                                    MessageService.error("Error in saving data...");
                                } else {
                                    MessageService.error(error.data);
                                }
                            })
                    }
                }
                //when user press cancel button...
                // $scope.goBack = function(){
                //   console.log("On button click...");
                //   window.history.back();
                // }
                //maxDate for dateIncorporation in addFpo
            $scope.maxDate = new Date();
        }
    ])

.controller('FpoDetailsCtrl', ['$scope', '$stateParams', '$state', 'QueryService', 'MessageService', 'shareDataService',
        function($scope, $stateParams, $state, QueryService, MessageService, shareDataService) {
            var fpoId;
            $scope.fpoId = fpoId = $stateParams.fpoId || localStorage.fpoId;
            console.log(fpoId);
            $scope.fpoName = window.localStorage.getItem('fpoName');


            //this QueryService will call everytime when this page will load and fetch all details of particular fpo...
            QueryService.query('GET', '/fpos/' + fpoId, {}, {}).then(function(response) {
                if (response.status == 200) {
                    console.log(response.data);
                    $scope.fpoDetails = response.data;
                    $scope.fpoAddress = response.data.addresses[0];
                    //$scope.member_farmers = response.data.member_farmers;
                    $scope.farmerShare = response.data.shareInfo;
                }
            }, function(error) {
                MessageService.error(error.data);
            });

            //this function will call when user wants to see more details of fpo...
            $scope.fpoMoreDetails = function(id) {
                $state.go('fpoMoreDetails({fpoId:id})');
            }

            //this function will used when user click on add button to add new members...
            $scope.addMember = function() {
                $state.go('addFpoMember');
            }


            //this function will used when user click on show user button.....
            $scope.showFpoUsers = function() {
                    $state.go('userList');
                } //end of showFpoUsers...

            $scope.farmerDetails = function(farmer) {
                var farmerId = farmer.farmerId;
                $state.go('farmerDetails', { farmerId: farmerId });
            }

            // Method to add a FPO Group for a FPO
            $scope.addFpoGroup = function(group_type) {
                $state.go('fpoGroupCreate', { group_type: JSON.stringify(group_type) });
            }

            $scope.fpoGroupDetails = function(fpogroup) {
                $state.go('fpoGroupDetails');
            }

            $scope.add_member_farmer = function(fpo_group_type) {
                $state.go('AddMemberFarmer', { fpoGroup: JSON.stringify(fpo_group_type) });
            }


            //automatically call this API directly when page will load..
            QueryService.query('GET', '/fpos/' + fpoId + '/group_types', {}, {}).then(function(response) {
                if (response.status == 200) {
                    $scope.fpo_group_types = response.data;
                    angular.forEach($scope.fpo_group_types, function(value, key) {
                        $scope.fpo_group_types[key].id = value.groupId;
                    });
                }
            }, function(error) {
                MessageService.error(error.data);
            });

            //this function will call api to get all members of FPO...
            $scope.getMembers = function() {

                    QueryService.query('GET', '/fpos/' + fpoId + '/farmers', {}, {})
                        .then(function(response) {
                            if (response.status == 200) {
                                //console.log(response.data);
                                $scope.member_farmers = response.data;
                            }
                        }, function(error) {
                            MessageService.error(error.data);
                        });

                } //end of this method....

            //call API on panel click....
            $scope.getGroups = function(group_code) {
                //console.log(group_code);
                $scope.fpo_groups = {};
                QueryService.query('GET', '/fpos/' + fpoId + '/groups/' + group_code, {}, {}).then(function(response) {
                    if (response.status == 200) {
                        console.log(response.data);
                        $scope.fpo_groups = response.data;
                    }
                }, function(error) {
                    MessageService.error(error.data);
                })
            }


            // member delete from fpo groups
            $scope.memberIds = {};
            $scope.farmerIds = [];
            $scope.removeSelectedItems = function() {
                angular.forEach($scope.memberIds, function(value, key) {
                    if (key)
                        $scope.farmerIds.push(key);
                });
                if ($scope.farmerIds.length < 1) {
                    MessageService.warning("Please select members!");
                } else {
                    var fpoGroupId = $scope.fpo_groups.id;
                    QueryService.query('PATCH', '/groups/' + fpoGroupId + '/members', {}, { "members": $scope.farmerIds })
                        .then(function(response) {
                            if (response.status == 200) {
                                $state.reload();
                                MessageService.success("Member deleted successfully");
                            } else {
                                MessageService.error("Couldn't save data to backend server..");
                            }
                        }, function(error) {
                            MessageService.error(error.data);
                        })
                }
            }
            $scope.group_id = [];
            $scope.deleteGroup = function() {
                    angular.forEach($scope.memberIds, function(value, key) {
                        if (key)
                            $scope.group_id.push(key);
                    });
                    if ($scope.group_id.length < 1) {
                        MessageService.warning("Please select members!");
                    } else {
                        var group_id = $scope.group_id;
                        QueryService.query('DELETE', '/fpo_groups/', {}, { group_id: group_id }).then(function(response) {
                            if (response.status == 200) {
                                $state.reload();
                                MessageService.success("FPO group deleted successfully");
                            }
                        }, function(error) {
                            MessageService.error(error.data);
                        })
                    }
                } //end of delete fpo_group method.....
                //cancel button for fpo details screen
            $scope.cancelFPODetails = function() {
                window.history.back();
            }


        }
    ])
    .controller('FpoMembersListCtrl', ['$scope', '$state', '$stateParams', 'QueryService', 'MessageService',
        function($scope, $state, $stateParams, QueryService, MessageService) {

            var fpoId = $stateParams.fpoId;
            if (window.localStorage.getItem('fpoName')) {
                $scope.fpoName = window.localStorage.getItem('fpoName');
            }

            QueryService.query('GET', '/fpos/' + fpoId + '/farmers', {}, {})
                .then(function(response) {
                    if (response.status == 200) {
                        //console.log(response.data);
                        $scope.member_farmers = response.data;
                    }
                }, function(error) {
                    MessageService.error(error.data);
                });

            $scope.farmerDetails = function(farmer) {
                    var farmerId = farmer.memberId;
                    $state.go('farmerDetails', { farmerId: farmerId });
                }
                //this function will used when user click on add button to add new members...
            $scope.addMember = function() {
                $state.go('addFpoMember');
            }

        }
    ])

.controller('FpoMembersAddCtrl', ['$scope', '$stateParams', '$filter', 'QueryService', 'MessageService', '$timeout',
    function($scope, $stateParams, $filter, QueryService, MessageService, $timeout) {

        $("#dob").datetimepicker({
            showClear: true,
            keepOpen: false,
            focusOnShow: false,
            format: 'DD-MM-YYYY',
            ignoreReadonly: true,
            maxDate: moment(),
            minDate: new Date("01/01/1940")
        });

        $("#dob").on("dp.change", function(e) {

            var tmpDob = moment($('#dob').val(), "DD-MM-YYYY").year();
            $scope.farmer.year_birth = tmpDob;

            $timeout(function() {
                angular.element($("#year_birth")).scope().$apply();
            });
        });

        $("#date_joined").datetimepicker({
            showClear: true,
            keepOpen: false,
            focusOnShow: false,
            format: 'DD-MM-YYYY',
            ignoreReadonly: true,
            maxDate: moment(),
            minDate: new Date("01/01/1990")
        });

        $("#date_joined").on("dp.change", function(e) {

            $scope.farmer.date_joined = moment($("#date_joined").data("DateTimePicker").date()).format('DD-MM-YYYY');

            $timeout(function() {
                angular.element($("#date_joined")).scope().$apply();
            });
        });

        $scope.gender_types = [];
        $scope.marital_status = [];
        $scope.religions = [];
        $scope.education_level = [];
        $scope.caste = [];
        $scope.farmer = {};

        //set default value of marital status....
        $scope.farmer.marital_status = "MARRIED";
        $scope.farmer.gender = "MALE";

        QueryService.query('GET', '/religions', {}, {})
            .then(function(response) {
                if (response.status == 200) {
                    $scope.religions = response.data;
                    console.log($scope.religions);
                } else {
                    MessageService.error("Error in fetching data...");
                }
            }, function(error) {
                MessageService.error(error.data);
            });

        //fetch education from database lookup table......
        QueryService.query('GET', '/education_types', {}, {})
            .then(function(response) {
                if (response.status == 200) {
                    $scope.education_level = response.data;
                } else {
                    MessageService.error("Error in fetching data.");
                }
            }, function(error) {
                MessageService.error(error.data);
            });

        QueryService.query('GET', '/castes', {}, {}).then(function(response) {
            if (response.status == 200) {
                $scope.castes = response.data;
            } else {
                MessageService.error("Error in fetching data...");
            }
        }, function(error) {
            MessageService.error(error.data);
        });

        //on select date.....

        $scope.getYear = function(date) {
            if ($scope.farmer.dob) {
                // console.log(date);
                $scope.farmer.year_birth = $filter('date')($scope.farmer.dob, 'yyyy');
            }
        }


        //$scope.farmer = {};
        $scope.addFpoMember = function() {
            console.log($scope.farmer);
            var fpoId = window.localStorage.getItem('fpoId');

            $scope.farmer.dob = new Date(moment($('#dob').val(), "DD-MM-YYYY"));
            $scope.farmer.year_birth = $("#year_birth").val();
            $scope.farmer.date_joined = new Date(moment($('#date_joined').val(), "DD-MM-YYYY"));

            if ($scope.fpoMember.$valid) {
                QueryService.query('POST', '/fpos/' + fpoId + '/farmers', {}, $scope.farmer)
                    .then(function(response) {
                            // console.log(response.data);
                            if (response.status == 200) {
                                window.history.back();
                            }
                        },
                        function(error) {
                            MessageService.error(JSON.stringify(error.data));
                        });
            };
        };

        $scope.maxDate = new Date();
        $scope.farmer.religion = "Hindu";
        $scope.farmer.caste = "General-Hindu";
    }
])

.controller('FpoMembersDetailsCtrl', ['$scope', '$state', '$stateParams', 'QueryService', 'MessageService',
    function($scope, $state, $stateParams, QueryService, MessageService) {

        // $scope.fpoName = window.localStorage.getItem('fpoName');
        var farmerId = $stateParams.farmerId;
        $scope.farmerId = farmerId;
        //console.log(farmerId);
        QueryService.query('GET', '/farmers/' + farmerId + '/basicDetails', {}, {})
            .then(function(response) {
                if (response.status == 200) {
                    $scope.farmerDetails = response.data.farmerDetails;
                    $scope.eventAttended = response.data.eventAttended;
                    $scope.memberSince = response.data.fpoRelationships.fpo_relationship[0];
                    $scope.svcNamesList = response.data.fpoRelationships.svcNamesList.join(', ');
                    $scope.farmerName = $scope.farmerDetails.fname + ($scope.farmerDetails.mname ? (" " + $scope.farmerDetails.mname) + " " : " ") + $scope.farmerDetails.lname;
                    window.localStorage.setItem('farmerName', $scope.farmerName);
                    window.localStorage.setItem('farmerProfilePic', ($scope.farmerDetails.avtar ? $scope.farmerDetails.avtar : ""));
                }
            }, function(error) {
                MessageService.error(JSON.stringify(error.data));
            });

        //call api to get family members.....

        //console.log(farmerId);
        //call QueryService to get seasonal_cultivation according to farmerId.....
        QueryService.query('GET', '/farmers/' + farmerId + '/cultivation', {}, {})
            .then(function(response) {
                if (response.status == 200) {
                    //console.log(response.data);
                    $scope.cultivations = response.data;
                }
            }, function(error) {
                MessageService.error(error.data);
            });

        //call api to get landholdings details....
        QueryService.query('GET', '/farmers/' + farmerId + '/land_ownership ', {}, {})
            .then(function(response) {
                if (response.status == 200) {
                    $scope.landholdings = response.data.plots;
                    $scope.landOwned = response.data.landOwned;
                    $scope.cultivable_area = response.data.landCultivable;
                } else {
                    MessageService.error("Error in fetching landholdings..");
                }
            }, function(error) {
                MessageService.error(JSON.stringify(error.data));
            }); //end of fetching landholdings....

        //on click family_member list item......
        $scope.selected_family_member = function(member) {
                var memberId = member.id;
                $state.go('detail-family-member', { family_member_id: memberId, farmerName: $scope.farmerName }, { inherit: false });
            } //end of method....
    }
])

.controller('FpoMembersEndowmentsCtrl', ['$scope', '$state', '$stateParams', 'QueryService', 'MessageService',
    function($scope, $state, $stateParams, QueryService, MessageService) {
        $scope.fpoName = window.localStorage.getItem('fpoName');
        $scope.farmerName = $stateParams.farmerName;
        $scope.familyMemberIds = {};
        $scope.memberIds = [];
        var farmerId = $stateParams.farmerId;

        //call api to get family members.....
        // console.log(farmerId);
        $scope.farmerId = farmerId;
        $scope.initFamilyMemberData = function() {
            QueryService.query('GET', '/farmers/' + farmerId + '/family', {}, {}).then(function(response) {
                $scope.family_members = response.data;
            }, function(error) {
                MessageService.error(response.data);
            }); //end of QueryService....

        }
        $scope.initFamilyMemberData();
        //call api to get landholdings details....
        QueryService.query('GET', '/farmers/' + farmerId + '/land_ownership ', {}, {}).then(function(response) {
            if (response.status == 200) {
                $scope.landholdings = response.data.plots;
                $scope.landOwned = response.data.landOwned;
                $scope.cultivable_area = response.data.landCultivable;
            } else {
                MessageService.error("Error in fetching landholdings..");
            }
        }, function(error) {
            MessageService.error(JSON.stringify(error.data));
        }); //end of fetching landholdings....

        //on click familt_member list item......
        $scope.selected_family_member = function(member) {
                var memberId = member.id;
                $state.go('detail-family-member', { family_member_id: memberId, farmerName: $scope.farmerName }, { inherit: false });

                //        $state.go('/detail-family-member/' + memberId+"/"+$scope.farmerName);
            } //end of method....


        $scope.removeSelectedItems = function() {
            angular.forEach($scope.familyMemberIds, function(key, value) {
                if (key)
                    $scope.memberIds.push(value);
            });
            if ($scope.memberIds.length < 1) {
                MessageService.warning("Please select the family members!");
            } else {
                QueryService.query('PATCH', '/farmers/family_member', {}, { "ids": $scope.memberIds })
                    .then(function(response) {
                        if (response.status == 200) {
                            $scope.initFamilyMemberData();
                            MessageService.success("Family Member deleted successfully");
                        } else {
                            MessageService.error("Couldn't save data to backend server..");
                        }
                    }, function(error) {
                        MessageService.error(error.data);
                    })
            }
        }

        // when you click to add landholdings//
        $scope.addFamilyMember = function() {
            $state.go('add-family-member', { farmerId: farmerId });
        }

        $scope.addLandHoldings = function() {
            $state.go('add-landholding', { farmerId: farmerId });
        }
        $scope.landHoldingIds = {};
        $scope.landHoldingArrayOfIds = [];
        $scope.removeItems = function() {
            angular.forEach($scope.landHoldingIds, function(key, value) {
                if (key)
                    $scope.landHoldingArrayOfIds.push(value);
            });
            if ($scope.landHoldingArrayOfIds.length < 1) {
                MessageService.warning("Please select member LandHoldings");
            } else {
                QueryService.query('PATCH', '/land_owned', {}, { "ids": $scope.landHoldingArrayOfIds })
                    .then(function(response) {
                        if (response.status == 200) {
                            $state.reload();
                            MessageService.success("LandHolding deleted successfully");
                        } else {
                            MessageService.error("Couldn't save data to backend server..");
                        }
                    }, function(error) {
                        if (error.status == -1) {
                            MessageService.error("Couldn't connect to backend server..");
                        } else {
                            // console.log(error.data);
                            MessageService.error(error.data);
                        }
                    })
            }
        }

        //when user press cancel button.....
        $scope.goBack = function() {
            window.history.back();
        };

        // to make accordion always open
        $scope.status = {
            isFirstOpen: true,
        };
        $scope.statusIs = {
            isFirstOpen: true,
        };

    }

])


.controller('FpoMembersInputsCtrl', ['$scope', '$state', '$stateParams', 'QueryService', 'MessageService',
    function($scope, $state, $stateParams, QueryService, MessageService) {
        $scope.fpoName = window.localStorage.getItem('fpoName');
        $scope.farmerName = window.localStorage.getItem('farmerName');
        var farmerId = $stateParams.farmerId;

        $scope.updateSeasonalCultivation = function(landData, landType) {
            window.sessionStorage.setItem('farmerSeasonalCultivationData', JSON.stringify(landData));
            $state.go('update_seasonal_cultivation', { 'farmerId': farmerId, 'landType': landType });
        }
        console.log(farmerId);
        $scope.farmerId = farmerId;
        //call QueryService to get seasonal_cultivation according to farmerId.....
        QueryService.query('GET', '/farmers/' + farmerId + '/cultivation', {}, {})
            .then(function(response) {
                if (response.status == 200) {
                    console.log(response.data);
                    $scope.cultivations = response.data;
                }
            }, function(error) {
                MessageService.error(error.data);
            });

        $scope.landLeasedIds = {};
        $scope.landLeasedArrayOfIds = [];
        $scope.removeItems = function() {
            alert("removeItems");
            angular.forEach($scope.landLeasedIds, function(key, value) {
                if (key)
                    $scope.landLeasedArrayOfIds.push(value);
            });

            if ($scope.landLeasedArrayOfIds.length >= 1) {
                QueryService.query('PATCH', '/land_leased', {}, {
                        "ids": $scope.landLeasedArrayOfIds
                    })
                    .then(function(response) {
                        if (response.status == 200) {
                            $state.reload();
                            MessageService.success("LandLeased deleted successfully");
                        } else {
                            MessageService.error("Couldn't save data to backend server..");
                        }
                    }, function(error) {
                        if (error.status == -1) {
                            MessageService.error("Couldn't connect to backend server..");
                        } else {
                            // console.log(error.data);
                            MessageService.error(error.data);
                        }
                    })
            }
        }
        $scope.landOwnedIds = {};
        $scope.landOwnedArrayOfIds = [];
        $scope.removeSelectedItems = function() {
            alert("removeSelectedItems");
            angular.forEach($scope.landOwnedIds, function(key, value) {
                if (key)
                    $scope.landOwnedArrayOfIds.push(value);
            });

            if ($scope.landOwnedArrayOfIds.length >= 1) {
                QueryService.query('PATCH', '/land_usage', {}, {
                        "ids": $scope.landOwnedArrayOfIds
                    })
                    .then(function(response) {
                        if (response.status == 200) {
                            $state.reload();
                            MessageService.success("LandOwned deleted successfully");
                        } else {
                            MessageService.error("Couldn't save data to backend server..");
                        }
                    }, function(error) {
                        if (error.status == -1) {
                            MessageService.error("Couldn't connect to backend server..");
                        } else {
                            // console.log(error.data);
                            MessageService.error(error.data);
                        }
                    })
            }
        }
        $scope.add_seasonal_cultivation = function() {
            console.log(farmerId);
            $state.go('add_seasonal_cultivation', { farmerId: farmerId });
        }

        //when user press cancel button.....
        $scope.goBack = function() {
            window.history.back();
        };

        $scope.status = {
            isFirstOpen: true,
        };
    }
])

.controller('FpoMembersMoreDetailsCtrl', ['$scope', '$state', '$stateParams', 'QueryService', 'MessageService', 'shareDataService',
    function($scope, $state, $stateParams, QueryService, MessageService, shareDataService) {

        //QueryService to get farmer_shares and personal details.......
        var farmerId = $stateParams.farmerId;
        $scope.farmerId = farmerId;
        //console.log(farmerId);
        //this title is for farmer address
        $scope.address_title = "Farmer Address";
        $scope.farmerName = window.localStorage.getItem('farmerName');
        $scope.farmerProfilePic = window.localStorage.getItem('farmerProfilePic');
        //console.log($scope.farmerName)
        //QueryService to get farmer_shares and personal details.......
        QueryService.query('GET', '/farmers/' + farmerId, {}, {}).then(function(response) {
            if (response.status == 200) {
                console.log(response.data);
                window.sessionStorage.setItem('farmerData', JSON.stringify(response.data));
                if (response.data) {
                    $scope.farmerDetails = response.data.farmerDetails;
                    $scope.personId = ($scope.farmerDetails?$scope.farmerDetails.personId:null);
                };
                $scope.address = response.data.farmer_addresses[0];

                //fetch district name from district object.....
                //check condition.....
                if (response.data.farmer_addresses.length > 0) {
                    $scope.address.district = response.data.farmer_addresses[0].district.name;
                };

                $scope.fpoRelationships = response.data.fpoRelationships;
                if ($scope.fpoRelationships) {
                    $scope.farmerShare = response.data.fpoRelationships.fpo_relationship;
                    $scope.svcNamesList = response.data.fpoRelationships.svcNamesList.join(', ');
                };
                shareDataService.addList('farmerShare', $scope.farmerShare);
                console.log($scope.farmerShare);
                console.log($scope.svcNamesList);
            } else {
                console.error("Error in fetching farmer details");
            }
        }, function(error) {
            console.error(JSON.stringify(error.data));
        }); //end of this QueryService.....

        //get all list of kyc documents.....
        console.log(farmerId);
        QueryService.query('GET', '/farmers/' + farmerId + '/kyc', {}, {}).then(function(response) {
            if (response.status == 200) {
                $scope.kycDoc = response.data;
            } else {
                MessageService.error("Failed to connect server...");
            }
        }, function(error) {
            MessageService.error(error.data);
        });

        //add new KYC....
        $scope.add_new_kyc_doc = function() {
            //  $state.go('/add-kyc/' + farmerId);
            console.log(farmerId);
            $state.go('add-kyc', { farmerId: farmerId });
        }

        //this method will open new page for update existing KYC DOC....
        $scope.update_kyc = function(kyc_doc) {
            console.log(kyc_doc);
            $state.go('update-kyc', { kyc: JSON.stringify(kyc_doc) });
        }

        //this method will open new page for fpo_service and fpo_share update....
        $scope.update_fpo_service_usage = function() {
            console.log("console...");
            var farmerShare = JSON.stringify($scope.farmerShare);
            $state.go('fpo_services_usage', { farmerShare: farmerShare });
        }

        // update and add farmer Shares Details
        $scope.fpoShare = function() {
            if ($scope.fpoRelationships.fpo_relationship.length > 0) {
                var farmerShareId = $scope.fpoRelationships.fpo_relationship[0].id;
                $state.go('fpoUpdateShare', { farmerShareId: farmerShareId });
                //    $state.go('/fpoUpdateShare/' + farmerShareId); //TODO Should be based on farmerId
            } else {
                MessageService.error("No Share Information recorded yet");
            }
        }

        //on cancel button press....
        $scope.goBack = function() {
            window.history.back();
        }

    }
])

// End of  ==> FpoMembersMoreDetailsCtrl <==   Method...

.controller('FPOServicesCtrl', ['$scope', '$stateParams', '$filter', 'QueryService', 'MessageService', 'shareDataService',
    function($scope, $stateParams, $filter, QueryService, MessageService, shareDataService) {
        console.log("Inside FPOServicesCtrl...");


        var fpoId = window.localStorage.getItem('fpoId');

        $scope.farmerShare = JSON.parse($stateParams.farmerShare)[0];
        var farmerId = $scope.farmerShare.farmer;

        $scope.farmerShareOld = JSON.parse($stateParams.farmerShare)[0];
        $scope.maxDate = new Date();
        $scope.farmerShare.date_joined = new Date($scope.farmerShare.date_joined);
        $scope.farmerShareOld.date_joined = new Date($scope.farmerShare.date_joined);

        /* Start the FPOServicesCtrl */

        $scope.fpoServiceUsed = [];
        $scope.fpoServices = [];
        $scope.deletefpoServices = [];
        $scope.item_checked = {};
        $scope.item_unChecked = {};
        var i = 0;
        var isAddRedirect = false;
        var isDeleteRedirect = false;
        var isShareUpdate = false;

        $scope.farmerName = window.localStorage.getItem('farmerName');



        //fetching all used services through farmerId.....
        QueryService.query('GET', '/fpo_service_usage/' + farmerId).then(function(response) {
                if (response.status == 200) {
                    console.log(response.data.services);
                    $scope.fpoServicesUsed = response.data.services;
                    $scope.getFPOServices();
                }
            }, function(error) {
                if (error.status == -1) {
                    MessageService.error("Could'nt connect to database..");
                } else {
                    MessageService.error(error.data);
                }
            }) //end of fetching used services through farmerId.....

        //this method will call on upper QueryService's response....
        $scope.getFPOServices = function() {

                QueryService.query('GET', '/fposervices')
                    .then(function(response) {
                        if (response.status == 200) {
                            $scope.fpoServices = response.data.data;
                        } else {
                            MessageService.error("Error in fetching data...");
                        }
                    }, function(error) {
                        if (error.status == -1) {
                            MessageService.error("Could not connect to backend server...");
                        } else {
                            MessageService.error(JSON.stringify(error.data));
                        }
                    })
            } //end of this function.....

        $scope.isChecked = function(checkList) {
            checkList.isChecked = $scope.fpoServicesUsed.some(function(serviceUsed) {
                return serviceUsed.service_code == checkList.code;
            });
        }

        //this function will create arrayList of fpoServices that will delete....
        $scope.isUnChecked = function(checkList) {
            if (checkList.isChecked) {
                $scope.deletefpoServices[i] = checkList.code;
                //console.log($scope.deletefpoServices);
                i++;
            } else {
                //console.log($scope.deletefpoServices);
            }
        }

        $scope.submitFPOServicesAndShareDetails = function() {
                $scope.item_unChecked.fpo_services = $scope.deletefpoServices;
                // console.log($scope.item_unChecked.fpo_services);
                // console.log($scope.item_checked);
                //in this if loop...we will check item_unChecked parameter and call api to delete fpo_services...
                if ($scope.item_unChecked.fpo_services.length != 0) {
                    //console.log($scope.item_unChecked);
                    QueryService.query('POST', '/update_service_usage/' + fpoId + '/' + farmerId, {}, $scope.item_unChecked).then(function(response) {
                        if (response.status == 200) {
                            //console.log(response);
                            MessageService.success("FPO Service Usage record has been deleted successfully");
                            isDeleteRedirect = true;
                            $scope.redirect();
                        } else {
                            MessageService.error("Error in deleting fpo services!!");
                            $scope.redirect();
                        }
                    })
                } else {
                    isDeleteRedirect = true;
                    $scope.redirect();
                }

                //in this if loop...we will check item_checked parameter and call api to add new fpo_services...
                if ($scope.item_checked.fpo_services != null) {
                    //console.log($scope.item_checked);
                    QueryService.query('POST', '/fpo_service_usage/' + fpoId + '/' + farmerId, {}, $scope.item_checked)
                        .then(function(response) {
                            if (response.status == 200) {
                                //console.log(response.data);
                                MessageService.success("FPO Service Usage record has been added successfully");
                                isAddRedirect = true;
                                $scope.redirect();
                            }
                        }, function(error) {
                            if (error.status == -1) {
                                MessageService.error("Couldn't connect to backend server...");
                                $scope.redirect();
                            } else {
                                MessageService.error(error.data);
                                $scope.redirect();
                            }
                        })
                } else {
                    isAddRedirect = true;
                    $scope.redirect();
                }

                //check for farmerShare update.....
                if (($scope.farmerShare.share_number == $scope.farmerShareOld.share_number) && ($scope.farmerShare.share_par_value == $scope.farmerShareOld.share_par_value) && ($scope.farmerShare.share_capital == $scope.farmerShareOld.share_capital) && ($scope.farmerShare.date_joined == $scope.farmerShareOld.date_joined)) {
                    //console.log("true");
                    isShareUpdate = true;
                    $scope.redirect();
                } else {

                    if ($scope.fpoUpdateShare.$invalid) {
                        $scope.redirect();
                    } else {
                        var farmerShareId = $scope.farmerShare.id;

                        //console.log(farmerId);
                        delete $scope.farmerShare.fpo;
                        delete $scope.farmerShare.farmer;
                        QueryService.query('PUT', '/shares_info/' + farmerShareId, {}, $scope.farmerShare)
                            .then(function(response) {
                                if (response.status == 200) {
                                    MessageService.success("FPO Shares details has been updated!");
                                    isShareUpdate = true;
                                    $scope.redirect();
                                }
                            }, function(error) {
                                MessageService.error(error.data);
                                $scope.redirect();
                            });

                    }
                }
            } //end of submitFPOServices method......

        //this function will redirect from this page to another page.....
        $scope.redirect = function() {
                if (isAddRedirect && isDeleteRedirect && isShareUpdate) {
                    //console.log("isAddRedirect: "+ isAddRedirect +" isDeleteRedirect: "+isDeleteRedirect+" isShareUpdate: " +isShareUpdate);
                    window.history.back();
                } else {
                    console.log("isAddRedirect: " + isAddRedirect + " isDeleteRedirect: " + isDeleteRedirect + " isShareUpdate: " + isShareUpdate);
                }
            }
            //on cancel button press....
        $scope.goBack = function() {
                window.history.back();
            } //end of goBack() function.....


        /* Start the FPOServicesCtrl */

    }
])
