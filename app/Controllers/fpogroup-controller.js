angular.module('fpoApp.controllers')

.controller('fpoGroupCreateController', ['$scope', '$stateParams', 'QueryService',
    'MessageService', 'areaMultiSelect', 'shareDataService',
    function($scope, $stateParams, QueryService,
        MessageService, areaMultiSelect, shareDataService) {

        $scope.fpo_groups_types = [];
        $scope.fpogroup = {};

        var fpoId = window.localStorage.getItem('fpoId');

        if (JSON.parse($stateParams.group_type) == "None") {
            console.log($stateParams.group_type);
        } else {
            $scope.fpo_group_type = JSON.parse($stateParams.group_type);
            $scope.fpogroup.type = $scope.fpo_group_type.code;
        }

        QueryService.query('GET', '/fpo_group_types', {}, {})
            .then(function(response) {
                if (response.status == 200) {
                    $scope.fpo_group_types = response.data;
                }
            }, function(error) {
                MessageService.error(error.data);
            });

        QueryService.query('GET', '/fpos/' + fpoId + '/farmers', {}, {})
            .then(function(response) {
                if (response.status == 200) {
                    $scope.farmer_members = response.data;
                }
            }, function(error) {
                MessageService.error(error.data);
            });

        // Getting the available clusters for CLA Group Creation
        QueryService.query('GET', '/clusters/' + fpoId, {}, {}).then(function(response) {
            if (response.status == 200) {
                $scope.member_clusters = response.data;
            }
        }, function(error) {
            MessageService.error(error.data);
        });

        $scope.addFpoGroup = function() {
            var selected_villages = shareDataService.getList('selected_villages');

            if (!_.isUndefined(selected_villages) &&
                $scope.fpogroup.type === 'JRG' || $scope.fpogroup.type === 'FIG') {
                $scope.fpogroup.area_association = _.map(selected_villages,
                    function(x) {
                        return x.id;
                    })
            }
            console.log($scope.fpogroup.area_association);
            if ($scope.fpogroup.name && $scope.fpogroup.type && $scope.fpogroup.admin != null) {
                QueryService.query('POST', '/fpo_groups/' + fpoId, {}, $scope.fpogroup)
                    .then(function(response) {
                        if (response.status == 200) {
                            MessageService.success("FPO Group has been created successfully");
                            window.history.back();
                        }
                    }, function(error) {
                        MessageService.error(error.data);
                    });
            }
        }

        $scope.cancelFpoGroup = function() {
            window.history.back();
        }
        $scope.show_area_modal = function() {
            areaModal = areaMultiSelect.modal();
        }
    }
])

.controller('fpoGroupDetailController', ['$scope', '$stateParams',
        '$state', 'QueryService', 'MessageService',
        'areaMultiSelect', 'shareDataService',
        function($scope, $stateParams, $state, QueryService, MessageService,
            areaMultiSelect, shareDataService) {
            var fpoId = JSON.parse(window.localStorage.getItem('fpoId'));

            $scope.fpogroup_details = {};
            $scope.fpo_group = {};
            $scope.selected_group_members = {};

            if (window.localStorage.getItem('fpoName')) {
                $scope.fpoName = window.localStorage.getItem('fpoName');
            }
            var fpoGroupId = $stateParams.fpogroupId;
            QueryService.query('GET', '/fpo_groups/' + fpoGroupId + '/details', {}, {})
                .then(function(response) {
                    if (response.status == 200) {
                        $scope.fpogroup_details = response.data;
                        console.log(response.data);
                        if ($scope.fpogroup_details.area_association.length > 0) {
                            shareDataService.addList("area_association", $scope.fpogroup_details.area_association);
                        }
                        $scope.fpo_group.name = $scope.fpogroup_details.name;
                    }
                }, function(error) {
                    MessageService.error(error.data);
                });

            //on edit button click...
            $scope.updateGroupDetails = function(group_details) {
                $state.go('updateFpoGroupDetails', { fpoGroupDetails: JSON.stringify(group_details) });
            }

            //on plus button click......
            $scope.AddMemberFarmer = function() {
                $scope.fpo_group.id = $scope.fpogroup_details.id;
                $scope.fpo_group.name = $scope.fpogroup_details.name;
                $state.go('AddMemberFarmer', { fpoGroup: JSON.stringify($scope.fpo_group) });
            }

            QueryService.query('GET', '/fpo_groups/' + fpoGroupId + '/members', {}, {})
                .then(function(response) {
                    //console.log(fpoGroupId);
                    if (response.status == 200) {
                        console.log(response.data);
                        $scope.fpogroup_members = response.data;
                    }
                }, function(error) {
                    MessageService.error(error.data);
                });
            QueryService.query('GET', '/fpos/' + fpoId + '/groups', {}, {})
                .then(function(response) {
                    //console.log(response.data);
                    if (response.status == 200) {
                        $scope.fpo_farmergroups = response.data;
                    }
                }, function(error) {
                    MessageService.error(error.data);
                });

            //on minus button click.....
            $scope.removeSelectedItems = function() {
                    $scope.members = [];
                    console.log($scope.selected_group_members);
                    angular.forEach($scope.selected_group_members, function(value, key) {
                        if (value) {
                            $scope.members.push(key);
                        }
                    });
                    QueryService.query('PATCH', '/groups/' + fpoGroupId + '/members', {}, { "members": $scope.members }).then(function(response) {
                        if (response.status == 200) {
                            //$scope.fpo_farmergroups = response.data;
                            $state.reload();
                        }
                    }, function(error) {
                        MessageService.error(error.data);
                    })
                } //end of this method......

            //on click of remove button.......
            $scope.deleteGroup = function(group_id) {
                    console.log(group_id);
                    QueryService.query('DELETE', '/fpo_groups/', {}, { group_id: group_id }).then(function(response) {
                        if (response.status == 200) {
                            window.history.back();
                            MessageService.success("FPO group deleted successfully")
                        }
                    }, function(error) {
                        MessageService.error(error.data);
                    })
                } //end of delete fpo_group method.....
            $scope.status = {
                isFirstOpen: true,
            };

            $scope.show_area_modal = function() {
                areaModal = areaMultiSelect.modal();
                areaModal.closed.then(function(result) {});
            }
        }
    ])
    .controller('addFpoGroupMemberController', ['$scope', '$state', '$stateParams', 'QueryService', 'MessageService',
        function($scope, $state, $stateParams, QueryService, MessageService) {

            var fpoId = window.localStorage.getItem('fpoId');

            $scope.fpo_group = JSON.parse($stateParams.fpoGroup);
            var fpo_group_id = $scope.fpo_group.id;
            console.log($scope.fpo_group);
            $scope.farmer_members = [];
            $scope.selected_farmer = [];
            $scope.customSettings = { displayProp: 'memberName', idProp: 'memberId' };

            QueryService.query('GET', '/fpos/' + fpoId + '/farmers', {}, {})
                .then(function(response) {
                    if (response.status == 200) {
                        $scope.farmer_members = response.data;
                    }
                }, function(error) {
                    MessageService.error(error.data);
                });


            $scope.addFpoGroupMember = function() {
                angular.forEach($scope.selected_farmer, function(value, key) {
                    $scope.selected_farmer[key] = value.id;
                })
                var member_farmer = $scope.selected_farmer;
                //console.log(member_farmer);
                QueryService.query('POST', '/groups/' + fpo_group_id + '/members', {}, { members: member_farmer })
                    .then(function(response) {
                        if (response.status == 200) {
                            //$state.go('fpoDetails')
                            window.history.back();
                        }
                    }, function(error) {
                        MessageService.error(error.data);
                    });
            }

            $scope.cancelAddFpoGroupMember = function() {
                window.history.back();
            }

        }
    ])
    .controller('UpdateFpoGroupDetailsCtrl', ['$scope', '$stateParams', 'QueryService', 'MessageService',
        function($scope, $stateParams, QueryService, MessageService) {

            $scope.fpogroup_members = [];

            var fpoId = window.localStorage.getItem('fpoId');

            console.log(JSON.parse($stateParams.fpoGroupDetails));
            $scope.fpogroup_details = JSON.parse($stateParams.fpoGroupDetails);
            $scope.fpogroup_details.type = $scope.fpogroup_details.type.code;
            $scope.fpogroup_details.admin = $scope.fpogroup_details.admin.id;
            console.log($scope.fpogroup_details.admin);

            var fpoGroupId = $scope.fpogroup_details.id;

            QueryService.query('GET', '/fpo_groups/' + fpoGroupId + '/members', {}, {})
                .then(function(response) {
                    //console.log(fpoGroupId);
                    if (response.status == 200) {
                        console.log(response.data);
                        $scope.fpogroup_members = response.data;
                        angular.forEach(response.data, function(value, key) {
                            $scope.fpogroup_members[key].fullName = value.fname + " " + value.lname;
                        })
                    }
                }, function(error) {
                    MessageService.error(error.data);
                });

            //fetch all default groups from server....
            // QueryService.query('GET','/fpo_group_types',{},{})
            // .then(function(response){
            //  if(response.status == 200){
            //   console.log(response.data);
            //   $scope.fpogroups_types = response.data;
            //  }
            //  },function(error){
            //   MessageService.error(error.data);
            // });

            //on cancel button click.....
            // $scope.goBack = function() {
            //     window.history.back();
            // }

            //On click button click....
            $scope.updateDetails = function() {
                console.log($scope.fpogroup_details);
                delete $scope.fpogroup_details.owner_fpo;
                delete $scope.fpogroup_details.created_on;
                delete $scope.fpogroup_details.area_association;
                delete $scope.fpogroup_details.id;
                console.log($scope.fpogroup_details);
                QueryService.query('PUT', '/groups/' + fpoGroupId, {}, $scope.fpogroup_details).then(function(response) {
                    if (response.status == 200) {
                        MessageService.success("FPO Group details updated successfully.");
                        window.history.back();
                    }
                }, function(error) {
                    MessageService.error(error.data);
                })
            }
        }
    ])
