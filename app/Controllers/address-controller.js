angular.module('fpoApp.controllers')
    .controller('UpdateFpoAddressCtrl', ['$scope', '$stateParams', 'QueryService', 'MessageService',
        function($scope, $stateParams, QueryService, MessageService) {

            $scope.address = {};
            var fpoId = $stateParams.fpoId;
            $scope.name = window.localStorage.getItem('fpoName');
            //console.log(fpoId);
            QueryService.query('GET', '/fpos/' + fpoId + '/addresses', {}, {})
                .then(function(response) {
                    if (response.status == 200) {
                        console.log(response.data);
                        $scope.address.country = response.data[0].country.code;
                        $scope.address.state = response.data[0].state.code;
                        $scope.address.district = response.data[0].district.code;
                        $scope.address = response.data[0];
                    }
                }, function(error) {
                    MessageService.error(error.data);
                });

            QueryService.query('GET', '/countries', {}, {}).then(function(response) {
                console.log(response)
                if (response.status == 200) {
                    $scope.countries = response.data.data;
                }
            }, function(error) {
                MessageService.error(error.data);
            });

            //this API will fetch all states.....
            QueryService.query('GET', '/states', {}, {})
                .then(function(response) {
                    console.log(response);
                    if (response.status == 200) {
                        $scope.states = response.data;
                    }
                }, function(error) {
                    MessageService.error(error.data);
                });


            $scope.getDistricts = function(states) {
                console.log(states);
                //call service method to load districts according to selected states..
                QueryService.query('GET', '/states/' + states + '/districts', {}, {}).then(function(response) {
                    console.log(response.data);
                    if (response.status == 200) {
                        $scope.districts = response.data;
                    }
                }, function(error) {
                    if (error.status == -1) {
                        MessageService.error("Could'nt connect to server...");
                    } else {
                        MessageService.error(JSON.stringify(error.data));
                    }
                })
            }

            //this method will call when user press update button....
            $scope.Address = function() {
                $scope.address.domain = "FPO";
                if ($scope.address.$invalid) {
                    return;
                }
                var addressId = $scope.address.id;
                QueryService.query('PUT', '/addresses/' + addressId, {}, $scope.address)
                    .then(function(response) {
                        if (response.status == 200) {
                            MessageService.success("FPO Address has been updated!");
                            window.history.back();
                        }
                    }, function(error) {
                        MessageService.error(error.data);
                    });
            }

            //on cancel button click.....
            $scope.goBack = function() {
                window.history.back();
            }
        }
    ])
    //end of updatefpoaddressctrl.....

.controller('AddFpoAddressCtrl', ['$scope', '$stateParams', 'QueryService', 'MessageService',
        function($scope, $stateParams, QueryService, MessageService) {

            var fpoId = $stateParams.fpoId;
            $scope.name = window.localStorage.getItem('fpoName');

            QueryService.query('GET', '/countries', {}, {}).then(function(response) {
                console.log(response)
                if (response.status == 200) {
                    $scope.countries = response.data.data;
                }
            }, function(error) {
                MessageService.error(error.data);
            });

            //this API will fetch all states.....
            QueryService.query('GET', '/states', {}, {})
                .then(function(response) {
                    console.log(response);
                    if (response.status == 200) {
                        $scope.states = response.data;
                    }
                }, function(error) {
                    MessageService.error(error.data);
                });


            //this method will call when any state will select......then this methos will call API to get all districts from selected state...
            $scope.getDistricts = function(states) {
                    console.log(states);
                    //call service method to load districts according to selected states..
                    QueryService.query('GET', '/states/' + states + '/districts', {}, {}).then(function(response) {
                        console.log(response.data);
                        if (response.status == 200) {
                            $scope.districts = response.data;
                        }
                    }, function(error) {
                        if (error.status == -1) {
                            MessageService.error("Could'nt connect to server...");
                        } else {
                            MessageService.error(JSON.stringify(error.data));
                        }
                    })
                } //end of this method.....


            //this method will add new address for fpo.......
            //this method will call when user press update button....
            $scope.Address = function() {
                    $scope.address.domain = "FPO";
                    if ($scope.address.$invalid) {
                        return;
                    } else {
                        //console.log($scope.address);
                        QueryService.query('POST', '/fpos/' + fpoId + '/addresses', {}, $scope.address).then(function(response) {
                            if (response.status == 200) {
                                //console.log(response.data);
                                window.history.back();
                            }
                        }, function(error) {
                            MessageService.error(error.data);
                        });
                    }
                } //end of this method....

            // This method when user press cancel button on  address screen..
            $scope.goBack = function() {
                window.history.back();
            }

        }
    ])
    //end of addfpoaddressctrl....

.controller('UpdateFarmerAddressCtrl', ['$scope', '$stateParams', 'QueryService', 'MessageService',
        function($scope, $stateParams, QueryService, MessageService) {

            $scope.name = window.localStorage.getItem('farmerName');
            var farmerId = $stateParams.farmerId;

            //get farmer address from api and set it to all fields........
            QueryService.query('GET', '/farmers/' + farmerId + '/addresses', {}, {}).then(function(response) {
                    if (response.status == 200) {
                        console.log(response.data);
                        $scope.address = response.data[0];
                        $scope.address.country = response.data[0].country.code;
                        $scope.address.state = response.data[0].state.code;
                        if (response.data[0].district != null) {
                            $scope.address.district = response.data[0].district.code;
                        }
                        $scope.getDistrictOnAPIResponse($scope.address.state);
                    }
                }, function(error) {
                    console.log(error.data);
                }) //end of this api response....

            //get all countries list from this API....
            QueryService.query('GET', '/countries', {}, {}).then(function(response) {
                //console.log(response)
                if (response.status == 200) {
                    $scope.countries = response.data.data;
                }
            }, function(error) {
                MessageService.error(error.data);
            });


            //API method to fetch states...
            QueryService.query('GET', '/states', {}, {}).then(function(response) {
                if (response.status == 200) {
                    $scope.states = response.data;
                    //console.log($scope.states);
                }
            }, function(error) {
                MessageService.error(error.data);
            });

            //this method will call on the response of GET farmerAddress API and with that response we will get all districts according to state...
            $scope.getDistrictOnAPIResponse = function(state_code) {
                    QueryService.query('GET', '/states/' + state_code + '/districts', {}, {}).then(function(response) {
                            //console.log(response.data);
                            if (response.status == 200) {
                                $scope.districts = response.data;
                            }
                        }, function(error) {
                            if (error.status == -1) {
                                MessageService.error("Could'nt connect to server...");
                            } else {
                                MessageService.error(JSON.stringify(error.data));
                            }
                        }) //end of this API response.....
                } //end of this method.....


            //this method will call on change state.....
            $scope.getDistricts = function(state) {
                console.log(state);
                //call service method to load districts according to selected states..
                QueryService.query('GET', '/states/' + state + '/districts', {}, {}).then(function(response) {
                    //console.log(response.data);
                    if (response.status == 200) {
                        $scope.districts = response.data;
                    }
                }, function(error) {
                    if (error.status == -1) {
                        MessageService.error("Could'nt connect to server...");
                    } else {
                        MessageService.error(JSON.stringify(error.data));
                    }
                })
            }

            //this method will call to submit updated address of farmer.....
            $scope.Address = function() {
                    //console.log($scope.address);
                    var farmerAddressId = $scope.address.id;
                    delete $scope.address.id;
                    delete $scope.address.district_code;
                    delete $scope.address.district_name;
                    delete $scope.address.state_code;
                    console.log(farmerAddressId);
                    $scope.address.domain = "FPO";
                    if ($scope.address.$invalid) {
                        return;
                    } else {
                        console.log($scope.address);
                        QueryService.query('PUT', '/addresses/' + farmerAddressId, {}, $scope.address).then(function(response) {
                            if (response.status == 200) {
                                MessageService.success("Updated successfully.");
                                window.history.back();
                            } else {
                                MessageService.error("Could not successfully update address.");
                            }
                        }, function(error) {
                            MessageService.error(error.data);
                        })
                    }
                } //end of update farmer address function....

            //on cancel button press....
            $scope.goBack = function() {
                window.history.back();
            }

        }
    ])
    //end of this controller...

.controller('AddFarmerAddressCtrl', ['$scope', '$stateParams', 'QueryService', 'MessageService',
        function($scope, $stateParams, QueryService, MessageService) {

            $scope.name = window.localStorage.getItem('farmerName');
            var farmerId = $stateParams.farmerId;
            console.log($scope.name);
            //get all countries list from this API....
            QueryService.query('GET', '/countries', {}, {}).then(function(response) {
                //console.log(response)
                if (response.status == 200) {
                    $scope.countries = response.data.data;
                }
            }, function(error) {
                MessageService.error(error.data);
            })

            //API method to fetch states...
            QueryService.query('GET', '/states', {}, {}).then(function(response) {
                if (response.status == 200) {
                    $scope.states = response.data;
                    //console.log($scope.states);
                }
            }, function(error) {
                MessageService.error(error.data);
            });

            //this method will call on change state.....
            $scope.getDistricts = function(states) {
                console.log(states);
                //call service method to load districts according to selected states..
                QueryService.query('GET', '/states/' + states + '/districts', {}, {}).then(function(response) {
                    //console.log(response.data);
                    if (response.status == 200) {
                        $scope.districts = response.data;
                    }
                }, function(error) {
                    if (error.status == -1) {
                        MessageService.error("Could'nt connect to server...");
                    } else {
                        MessageService.error(JSON.stringify(error.data));
                    }
                })
            }

            //on check button click....
            $scope.Address = function() {
                    $scope.address.domain = "FPO";
                    if ($scope.address.$invalid) {
                        return;
                    } else {
                        console.log($scope.address);
                        QueryService.query('POST', '/farmers/' + farmerId + '/addresses', {}, $scope.address).then(function(response) {
                            if (response.status == 200) {
                                MessageService.success("Address saved successfully.");
                                window.history.back();
                            } else {
                                MessageService.success("Could not save successfully.");
                            }
                        }, function(error) {
                            MessageService.error(error.data);
                        })
                    }
                } //end of this method.....

            //on cancel button click....
            $scope.goBack = function() {
                window.history.back();
            }

        }
    ])
    //end of this controller....

.controller('AddEventAddressCtrl', ['$scope', '$stateParams', 'QueryService', 'MessageService',
    function($scope, $stateParams, QueryService, MessageService) {

        var eventId = $stateParams.eventId;
        $scope.name = window.localStorage.getItem('eventName');

        //call API to get country list....
        QueryService.query('GET', '/countries', {}, {}).then(function(response) {
            //console.log(response)
            if (response.status == 200) {
                $scope.countries = response.data.data;
            }
        }, function(error) {
            MessageService.error(error.data);
        })

        //call API to get all states from country .....
        QueryService.query('GET', '/states', {}, {}).then(function(response) {
            if (response.status == 200) {
                $scope.states = response.data;
                //console.log($scope.states);
            }
        }, function(error) {
            MessageService.error(error.data);
        })

        //get all districts from selected states....
        $scope.getDistricts = function(states) {
                console.log(states);
                //call service method to load districts according to selected states..
                QueryService.query('GET', '/states/' + states + '/districts', {}, {}).then(function(response) {
                    //console.log(response.data);
                    if (response.status == 200) {
                        $scope.districts = response.data;
                    }
                }, function(error) {
                    if (error.status == -1) {
                        MessageService.error("Could'nt connect to server...");
                    } else {
                        MessageService.error(JSON.stringify(error.data));
                    }
                })
            } //end of this method......


        //on Address button click......
        $scope.Address = function() {
                $scope.address.domain = "FPO";
                if ($scope.address.$invalid) {
                    return;
                } else {
                    console.log($scope.address);
                    QueryService.query('POST', '/event/' + eventId + '/location', {}, $scope.address).then(function(response) {
                        if (response.status == 200) {
                            MessageService.success("Address save successfully.");
                            window.history.back();
                        }
                    }, function(error) {
                        MessageService.error(JSON.stringify(error.data));
                    })
                }
            } //end of this method.....

        //on cancel button click......
        $scope.goBack = function() {
            window.history.back();
        }


    }
])

.controller('UpdateEventAddressCtrl', ['$scope', '$stateParams', 'QueryService', 'MessageService',
        function($scope, $stateParams, QueryService, MessageService) {

            var eventId = $stateParams.eventId;
            $scope.name = window.localStorage.getItem('eventName');

            //get event address and bint it to html file.....
            QueryService.query('GET', '/events/' + eventId, {}, {}).then(function(response) {
                if (response.status == 200) {
                    console.log(response.data.location);
                    $scope.address = response.data.location;
                    //if condition to check state...if state is available, then fetch all district from that state....
                    if (response.data.location.state) {
                        $scope.getDistrictOnAPIResponse(response.data.location.state);
                    }
                }
            }, function(error) {
                MessageService.error(JSON.stringify(error.data));
            });

            //get all country list on page refresh or page load.....
            QueryService.query('GET', '/countries', {}, {}).then(function(response) {
                    //console.log(response)
                    if (response.status == 200) {
                        $scope.countries = response.data.data;
                    }
                }, function(error) {
                    MessageService.error(error.data);
                }) //end of this API response...

            //get all states on page refresh or page load.....
            QueryService.query('GET', '/states', {}, {}).then(function(response) {
                    //console.log(response)
                    if (response.status == 200) {
                        $scope.states = response.data;
                    }
                }, function(error) {
                    MessageService.error(error.data);
                }) //end of this API response...

            //this function will call when event address API response will come and if state is there....
            $scope.getDistrictOnAPIResponse = function(state_code) {

                    QueryService.query('GET', '/states/' + state_code + '/districts', {}, {}).then(function(response) {
                            //console.log(response.data);
                            if (response.status == 200) {
                                $scope.districts = response.data;
                            }
                        }, function(error) {
                            if (error.status == -1) {
                                MessageService.error("Could'nt connect to server...");
                            } else {
                                MessageService.error(JSON.stringify(error.data));
                            }
                        }) //end of this API response.....

                } //end of this method........



            //this method will call when user change his state....
            $scope.getDistricts = function(state) {

                QueryService.query('GET', '/states/' + state + '/districts', {}, {}).then(function(response) {
                        //console.log(response.data);
                        if (response.status == 200) {
                            $scope.districts = response.data;
                        }
                    }, function(error) {
                        if (error.status == -1) {
                            MessageService.error("Could'nt connect to server...");
                        } else {
                            MessageService.error(JSON.stringify(error.data));
                        }
                    }) //end of this API response.....

            }

            //on update button click ......
            $scope.Address = function() {
                    $scope.address.domain = "FPO";
                    var eventAddressId = $scope.address.id;
                    if ($scope.address.$invalid) {
                        return;
                    } else {
                        console.log($scope.address);
                        QueryService.query('PUT', '/addresses/' + eventAddressId, {}, $scope.address).then(function(response) {
                            if (response.status == 200) {
                                MessageService.success("Event Address updated successfully.");
                                window.history.back();
                            }
                        }, function(error) {
                            MessageService.error(JSON.stringify(error.data));
                        })
                    }
                } //end of this method.....
        }
    ])
    .controller('AddOtherFPOAddressCtrl', ['$scope', '$stateParams', 'QueryService', 'MessageService',
        function($scope, $stateParams, QueryService, MessageService) {

            //var fpoAddressType = JSON.parse($stateParams.fpo_address_type);
            console.log($stateParams);
            var fpoId = $stateParams.fpoId;
            var address_type = $stateParams.address_type;
            $scope.name = window.localStorage.getItem('fpoName');

            //call API to get country list....
            QueryService.query('GET', '/countries', {}, {}).then(function(response) {
                //console.log(response)
                if (response.status == 200) {
                    $scope.countries = response.data.data;
                }
            }, function(error) {
                MessageService.error(error.data);
            })

            //call API to get all states from country .....
            QueryService.query('GET', '/states', {}, {}).then(function(response) {
                if (response.status == 200) {
                    $scope.states = response.data;
                    //console.log($scope.states);
                }
            }, function(error) {
                MessageService.error(error.data);
            })

            //get all districts from selected states....
            $scope.getDistricts = function(states) {
                    console.log(states);
                    //call service method to load districts according to selected states..
                    QueryService.query('GET', '/states/' + states + '/districts', {}, {}).then(function(response) {
                        //console.log(response.data);
                        if (response.status == 200) {
                            $scope.districts = response.data;
                        }
                    }, function(error) {
                        if (error.status == -1) {
                            MessageService.error("Could'nt connect to server...");
                        } else {
                            MessageService.error(JSON.stringify(error.data));
                        }
                    })
                } //end of this method......

            //on Address button click......
            $scope.Address = function() {
                    $scope.address.domain = "FPO";
                    $scope.address.type = address_type;
                    if ($scope.address.$invalid) {
                        return;
                    } else {
                        console.log($scope.address);
                        QueryService.query('POST', '/fpos/' + fpoId + '/addresses', {}, $scope.address).then(function(response) {
                            if (response.status == 200) {
                                MessageService.success("Address save successfully.");
                                window.history.back();
                            }
                        }, function(error) {
                            MessageService.error(JSON.stringify(error.data));
                        })
                    }
                } //end of this method.....



        }
    ])
    .controller('AddVendorAddressCtrl', ['$scope', '$state', '$stateParams', 'QueryService', 'MessageService',
        function($scope, $state, $stateParams, QueryService, MessageService) {
            var vendorId = $stateParams.vendorId;
            $scope.name = "Add " + "[ " + window.localStorage.getItem('vendorName') + " ]";

            console.log($scope.name);
            //get all countries list from this API....
            QueryService.query('GET', '/countries', {}, {}).then(function(response) {
                //console.log(response)
                if (response.status == 200) {
                    $scope.countries = response.data.data;
                }
            }, function(error) {
                MessageService.error(error.data);
            })

            //API method to fetch states...
            QueryService.query('GET', '/states', {}, {}).then(function(response) {
                if (response.status == 200) {
                    $scope.states = response.data;
                    //console.log($scope.states);
                }
            }, function(error) {
                MessageService.error(error.data);
            });

            //this method will call on change state.....
            $scope.getDistricts = function(states) {
                console.log(states);
                //call service method to load districts according to selected states..
                QueryService.query('GET', '/states/' + states + '/districts', {}, {}).then(function(response) {
                    //console.log(response.data);
                    if (response.status == 200) {
                        $scope.districts = response.data;
                    }
                }, function(error) {
                    if (error.status == -1) {
                        MessageService.error("Could'nt connect to server...");
                    } else {
                        MessageService.error(JSON.stringify(error.data));
                    }
                })
            }

            //on check button click....
            $scope.Address = function() {
                    if ($scope.address.$invalid) {
                        return;
                    } else {
                        console.log($scope.address);
                        QueryService.query('POST', '/vendor/' + vendorId + '/addresses', {}, $scope.address).then(function(response) {
                            if (response.status == 200) {
                                MessageService.success("Address saved successfully.");
                                $state.go('recordPurchase');
                            } else {
                                MessageService.success("Could not save successfully.");
                                window.history.back();
                            }
                        }, function(error) {
                            window.history.back();
                            MessageService.error(error.data);
                        })
                    }
                } //end of this method.....

            //on cancel button click....

        }
    ]);
    //end of this controller....
