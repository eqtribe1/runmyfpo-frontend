angular.module('fpoApp.controllers')
    .controller('InputCtrl', ['$scope', '$state', '$stateParams', 'QueryService', 'MessageService',
        function($scope, $state, $stateParams, QueryService, MessageService) {
  
            $scope.newMaterialCategory = function() {
                $state.go('new-material-category');
            }
            $scope.newMaterialAttribute = function() {
                $state.go('new-material-attribute');
            }
            $scope.addProductIn = function() {
                $state.go('add-product');
            }
            
            $scope.addPurchaseRecord = function(){
              $state.go('add-purchase-record');
            }

            $scope.createMaterialSKU = function(){
            	$state.go('add-material-sku');
            }

            $scope.createGoodsReceivedRecord = function(){
            	$state.go('add-goods-received-record');
            }
             $scope.currentStockSummary = function(){
            	$state.go('current-stock-summary');
            }
            $scope.skuStockDetails = function(){
            	$state.go('sku-stock-details');
            }
            $scope.addSalesRecord = function(){
            	$state.go('add-sales-record');
            }
            
             $scope.materialAttiributeModel = []; 
              $scope.material_attributeData = [{
               id: 1,
                Name: 'NPK Rating',
            }, {
                id: 2,
                Name: 'Nitrogen',
            }, {
                id: 3,
                Name: 'Phosphorous',
            },
            {
                id: 4,
                Name: 'Potassium',
            },
            {
                id: 5,
                Name: 'Some Attribute',
            },
            {
                id: 6,
                Name: 'Some Other Attribute',
            }]; 
            
            $scope.customSettings = {displayProp: 'Name', idProp: 'id'};
            $("#purchase_date").datetimepicker({
            showClear: true,
            keepOpen: false,
            focusOnShow: false,
            format: 'DD-MM-YYYY',
            ignoreReadonly: true,
            minDate: new Date("01/01/1990")
        });

        // $("#purchase_date").on("dp.change", function(e) {

        //     $scope.farmer.purchase_date = moment($("#purchase_date").data("DateTimePicker").date()).format('DD-MM-YYYY');

        //     $timeout(function() {
        //       angular.element($("#purchase_date")).scope().$apply();
        //     });
            
        // });
        $scope.customSettings = {displayProp: 'Name', idProp: 'id'};
            $("#delievery_date").datetimepicker({
            showClear: true,
            keepOpen: false,
            focusOnShow: false,
            format: 'DD-MM-YYYY',
            ignoreReadonly: true, 
            minDate: new Date("01/01/1990")
        });

        // $("#delievery_date").on("dp.change", function(e) {

        //     //$scope.farmer.delievery_date = moment($("#delievery_date").data("DateTimePicker").date()).format('DD-MM-YYYY');

        //     $timeout(function() {
        //       angular.element($("#delievery_date")).scope().$apply();
        //     }); 
        //  });
         
         $scope.customSettings = {displayProp: 'Name', idProp: 'id'};
            $("#received_date").datetimepicker({
            showClear: true,
            keepOpen: false,
            focusOnShow: false,
            format: 'DD-MM-YYYY',
            ignoreReadonly: true,
            minDate: new Date("01/01/1990")
        });

        // $("#received_date").on("dp.change", function(e) {

        //     $scope.farmer.received_date = moment($("#received_date").data("DateTimePicker").date()).format('DD-MM-YYYY');

        //     $timeout(function() {
        //       angular.element($("#received_date")).scope().$apply();
        //     }); 
        //  });
         $scope.customSettings = {displayProp: 'Name', idProp: 'id'};
            $("#sale_date").datetimepicker({
            showClear: true,
            keepOpen: false,
            focusOnShow: false,
            format: 'DD-MM-YYYY',
            ignoreReadonly: true,
            minDate: new Date("01/01/1990")
        });

        // $("#sale_date").on("dp.change", function(e) {

        //     $scope.farmer.sale_date = moment($("#sale_date").data("DateTimePicker").date()).format('DD-MM-YYYY');

        //     $timeout(function() {
        //       angular.element($("#sale_date")).scope().$apply();
        //     }); 
        //  });
         //var row_total = $scope.total; 
         
         //To Hide and Show Divs
       $scope.color = 'anyone';
        $scope.isShown = function(anyone) {
        return anyone === $scope.anyone;
    };                 
}])


          

       