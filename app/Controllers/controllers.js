// Generic models angular module initialize.
;(function() {
  'use strict';
  angular.module('fpoApp.controllers', ['checklist-model']);
}());
