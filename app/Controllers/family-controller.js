angular.module('fpoApp.controllers')
	.controller('ListFamilyMemberCtrl',['$scope','QueryService','MessageService',
		
		function($scope,QueryService,MessageService){

			$scope.family_members = [];

			var farmerId = JSON.parse(window.localStorage.getItem('farmerDetails')).farmerDetails.id;
			
			QueryService.query('GET', '/farmers/'+farmerId+'/family' ,{},{}).then(function(response){
					$scope.family_members = response.data;
					$scope.family_member.dob = $filter('date')(new Date(family_member.dob), 'yyyy-MM-dd');
			})
		
		}])
	
	.controller('AddFamilyMemberCtrl',['$scope','$filter','$stateParams','QueryService','MessageService','$timeout',
		
		function($scope,$filter,$stateParams,QueryService,MessageService,$timeout){

			$("#dob").datetimepicker({
				showClear: true,
	         	keepOpen: false,
	          	focusOnShow: false,
	          	format: 'DD-MM-YYYY',
	          	ignoreReadonly: true,
	          	maxDate: moment(),
	          	minDate: new Date("01/01/1940")
	        });

	        $("#dob").on("dp.change", function(e) {

	        	var tmpDob = moment($('#dob').val(),"DD-MM-YYYY").year();
				$scope.family_member.birth_year = tmpDob;

				$timeout(function() {
					angular.element($("#birth_year")).scope().$apply();
				});
	        });

			var farmerId = $stateParams.farmerId;
	
			//fetch relationship from database lookup table......
			QueryService.query('GET', '/family_relation_types' ,{},{}).then(function(response){
				$scope.relationships = response.data;
			})
		

			QueryService.query('GET', '/education_types' ,{},{}).then(function(response){
				$scope.educations = response.data;
			})

			$scope.addFamilyMember = function(){
				
				if($scope.FamilyMember.$invalid){ 

					console.log("FamilyMember Form is invalid");
					return; 
				}

				$scope.family_member.dob = new Date(moment($('#dob').val(),"DD-MM-YYYY"));
				$scope.family_member.birth_year = $("#birth_year").val();

				// $scope.family_member.earning = ($scope.family_member.earning === 'true');
				// $scope.family_member.stay_together = ($scope.family_member.stay_together === 'true');
			
				//call service to add family member of farmer....
				QueryService.query('POST', '/farmers/'+farmerId+'/family',{},$scope.family_member).then(function(response){
					
					if(response.status == 200){
						window.history.back();
					}
				},function(error){
					MessageService.error(error.data);
				})
			}

        	//on selecting date filtering year from calender to birth_year..
        	$scope.getYear = function(date){
            	$scope.family_member.birth_year = $filter('date')(date, 'yyyy');
        	}

			//when user press cancel button.....
	        $scope.goBack = function() {
            	window.history.back();
        	};
        
        	$scope.maxDate= new Date();
		}])

	.controller('UpdateFamilyMemberCtrl',['$scope','$stateParams','QueryService','$filter','MessageService','$timeout',
		
		function($scope,$stateParams,QueryService,$filter,MessageService,$timeout){

			$("#dob").datetimepicker({
				showClear: true,
	         	keepOpen: false,
	          	focusOnShow: false,
	          	format: 'DD-MM-YYYY',
	          	ignoreReadonly: true,
	          	maxDate: moment(),
	          	minDate: new Date("01/01/1940")
	        });

	        $("#dob").on("dp.change", function(e) {

	        	var tmpDob = moment($('#dob').val(),"DD-MM-YYYY").year();
				$scope.family_member.birth_year = tmpDob;

				$timeout(function() {
					angular.element($("#birth_year")).scope().$apply();
				});
	        });

			var familyMemberId = $stateParams.familyMemberId;
			$scope.family_member = {};
			$scope.initFamilyMemberData =  function(){
				QueryService.query('GET', '/family/'+familyMemberId ,{},{}).then(function(response){
					if(response.status == 200){
						$scope.family_member = family_member = response.data.family_member;
						$scope.family_member.relationship = family_member.relationship.code;
						$scope.family_member.education = family_member.education.code;
						$scope.family_member.dob = new Date(family_member.dob);
						$('#dob').data("DateTimePicker").date(moment(family_member.dob));
					}
					else{
				 		MessageService.error("Error in fetching family member details..");
					}
				},function(error){
					MessageService.error(error.data);
				});
			};
		
			$scope.initFamilyMemberData();

			//fetch relationship from database lookup table......
			QueryService.query('GET', '/family_relation_types' ,{},{}).then(function(response){
				console.log(response.data);
				$scope.relationships = response.data;
			})

			QueryService.query('GET', '/education_types' ,{},{}).then(function(response){
				console.log(response.data);
				$scope.educations = response.data;
			})

			$scope.updateFamilyMember = function(){
				var member_id = $scope.family_member.id;
				$scope.family_member.dob = new Date(moment($('#dob').val(),"DD-MM-YYYY"));
				$scope.family_member.birth_year = $("#birth_year").val();

				//call service to add family member of farmer....
				QueryService.query('PUT', '/family/'+member_id,{},family_member).then(function(response){
					
					if(response.status == 200){
						window.history.back();
					}else{
						MessageService.error("Couldn't save data to backend server..");
					}
				},function(error){
					if(error.status == -1){
						MessageService.error("Couldn't connect to backend server..");
					}
					else{
						console.log(error.data);
						//MessageService.error(error.data);
					}
				})
			}//end of update family member....

			//on cancel button press...
			$scope.goBack = function() {
			  	window.history.back();
			}//end of back button press..

  			//on selecting date filtering year from calender to birth_year..
	        $scope.getYear = function(date){
            	console.log(date);
            	$scope.family_member.birth_year = $filter('date')(date, 'yyyy');
        	}

         	$scope.maxDate= new Date();

		}])

	.controller('DetailFamilyMemberCtrl',['$scope','$state','$stateParams','QueryService','MessageService',
	
		function($scope,$state,$stateParams,QueryService,MessageService){
			
			var memberId = $stateParams.family_member_id;
			$scope.farmerName = $stateParams.farmerName;
			$scope.earningIds = {};
	        $scope.earningArrayOfIds = [];
        
        	$scope.removeSelectedItems = function(){
            	angular.forEach($scope.earningIds,function(key,value){
                	if(key)
                    	$scope.earningArrayOfIds.push(value);
            	});
            
            	if($scope.earningArrayOfIds.length < 1){
					MessageService.warning("Please select the member earnings");
            	}
            	else{
	            	QueryService.query('PATCH', '/member_earnings', {}, {"ids":$scope.earningArrayOfIds})
	          			.then(function(response) {
	                    	if (response.status == 200) {
								$scope.initFamilyMemberData();
								MessageService.success("FamilyMember earning deleted successfully");
	                    	}
	                    	else{
	                        	MessageService.error("Couldn't save data to backend server..");
	                    	}
	                	}, function(error) {
	                    	if (error.status == -1) {
	                        	MessageService.error("Couldn't connect to backend server..");
	                    	}
							else {
		                        // console.log(error.data);
		                        MessageService.error(error.data);
	                    	}
	               		})
	            }
			}

			$scope.update_family_member = function(){
				$state.go('update-family-member',{familyMemberId:memberId});
			};
		
			$scope.addFamilyMemberEarning = function(){
				$state.go('add-family-member-earning',{id:memberId,familyMemberName:$scope.family_member.name});
			};
		
			$scope.updateEarning = function(earning){
				$state.go('update_earning',{familyMemberName:$scope.family_member.name,'earning':JSON.stringify(earning)});
			};
			
			//fetching all details of selected family_member....
			$scope.initFamilyMemberData =  function(){
				QueryService.query('GET', '/family/'+memberId ,{},{}).then(function(response){
					if(response.status == 200){
						$scope.family_member = response.data.family_member;
						//$scope.familyMemberName = $scope.family_member.name;
						$scope.member_earnings = response.data.member_earnings;
			 			// window.localStorage.setItem('familyMemberName', $scope.familyMemberName);
					}
					else{
				 		MessageService.error("Error in fetching family member details..");
					}
				},function(error){
					MessageService.error(error.data);
				});
			};
		
			$scope.initFamilyMemberData();

			//when user press cancel button.....
        	$scope.goBack = function() {
            	window.history.back();
        	};

		}])

	.controller('AddFamilyMemberEarningCtrl',['$scope','$stateParams','QueryService','MessageService',
		
		function($scope,$stateParams,QueryService,MessageService){
    		
    		$scope.familyMemberName = $stateParams.familyMemberName;
			var memberId = $stateParams.id;
		
			//list all occupation details from backend....
			QueryService.query('GET', '/occupations' ,{},{}).then(function(response){
				console.log(response.data);
				$scope.occupations = response.data;
			})

			$scope.filterValue = function($event){
	        	if(isNaN(String.fromCharCode($event.keyCode))){
	            	$event.preventDefault();
	        	}
			};

			$scope.addEarnings = function(){
				if($scope.MemberEarning.$valid){
					QueryService.query('POST','/family/'+memberId+'/earnings',{},$scope.member_earning).then(function(response){
						if(response.status == 200){
							window.history.back();
						}
						else{
							MessageService.error("Could not connect to backend server...");
						}
					})
				}
				else{
					MessageService.error("Please fill valid data...");
				}
			}

  			$scope.goBack = function() {
				window.history.back();
  			};

  			// for showing year dropdown range..
   			$scope.dates = [];
        	var initDates = function() {
            	var i;
            	for (i = 2014;i <= 2018; i++) {
            		$scope.dates.push(i);
                }
            }
                
            initDates();
    	}])

	.controller('UpdateFamilyMemberEarningCtrl',['$scope','$stateParams','QueryService','MessageService',
		
		function($scope,$stateParams,QueryService,MessageService){
            
            $scope.familyMemberName = $stateParams['familyMemberName'];
			var member_earning = JSON.parse($stateParams.earning);
			
			$scope.member_earning = member_earning;
			var earningId = $scope.member_earning.id;

			$scope.filterValue = function($event){
		        if(isNaN(String.fromCharCode($event.keyCode))){
		            $event.preventDefault();
		        }
			};
			
			QueryService.query('GET', '/occupations' ,{},{}).then(function(response){
				console.log(response.data);
				$scope.occupations = response.data;
			});

			$scope.updateEarnings = function(member_earning){
				delete member_earning.member;
				delete member_earning.id;

				if($scope.MemberEarning.$valid){
					QueryService.query('PUT','/earnings/'+earningId,{},member_earning)
						.then(function(response){
							if(response.status == 404){
								console.log(response.data);
								MessageService.error(response.data);
							}
							if(response.status == 200){
								MessageService.success("Member Earnings have been updated successfully.")
								window.history.back();
							}
							else{
								console.log(response.data);
								MessageService.error("Could not connect to backend server...");
							}
						},function(error){
							if(error.status == 400){
								MessageService.error(error.data)
							}
						})
				}
				else{
					MessageService.error("Please fill valid data...");
				}
			}
             
            $scope.goBack = function() {
				window.history.back();
  			};

  			// for showing year dropdown range..
  			$scope.calendarYears = [];
        	var initDates = function() {
            	var i;
            	for (i = 2014;i <= 2018; i++) {
            		$scope.calendarYears.push(i);
                }
            }

            initDates();
}]);
