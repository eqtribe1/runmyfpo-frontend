angular.module('fpoApp.controllers')

.controller('FpoMoreDetailsCtrl', ['$scope', '$state', '$stateParams', 'QueryService', 'MessageService',
    function($scope, $state, $stateParams, QueryService, MessageService) {

        var fpoId = $stateParams.fpoId;
        $scope.fpo_details = {};
        $scope.address_title = "FPO Address";
        $scope.shop_addresses = [];
        $scope.coll_center_addresses = [];
        $scope.warehouse_addresses = [];
        //this QueryService will call everytime when this page will load and fetch all details of particular fpo...
        QueryService.query('GET', '/fpos/' + fpoId, {}, {}).then(function(response) {
            if (response.status == 200) {
                //console.log(response.data.addresses);
                $scope.fpoName = response.data.name;
                $scope.fpoDetails = response.data;
                $scope.address = response.data.addresses[0];
                
                //console.log($scope.shop_addresses);
                angular.forEach(response.data.addresses,function(value,key){
                    //console.log(value);
                    if(value.type == "FPO-SHOP"){
                        $scope.shop_addresses.push(value);
                    }
                    if(value.type == "FPO-COLL-CENTER"){
                        $scope.coll_center_addresses.push(value);
                    }
                    if(value.type == "FPO-WAREHOUSE"){
                        $scope.warehouse_addresses.push(value);
                    }
                });
                //console.log($scope.shop_addresses);
                // $scope.member_farmers = response.data.member_farmers;
                // $scope.farmerShare = response.data.shareInfo;
                $scope.fpo_details.id = response.data.id;
                $scope.fpo_details.name = response.data.name;
                $scope.fpo_details.description = response.data.description;
                $scope.fpo_details.registration_location = response.data.registration_location;
                $scope.fpo_details.date_incorporation = response.data.date_incorporation;
                $scope.fpo_details.cin = response.data.cin;
                $scope.fpo_details.ceoId = response.data.ceoID;
            }
        }, function(error) {
            MessageService.error(error.data);
        });

        QueryService.query('GET', '/clusters/' + fpoId, {}, {}).then(function(response) {
            if (response.status == 200) {
                $scope.member_clusters = response.data;
            }
        }, function(error) {
            MessageService.error(error.data);
        });


        //this function will used to expand accordion list...
        $scope.expand = function(type){
            //console.log(type);
            $scope.expand_div = {};
            $scope.expand_div = type;
        }

        //this method will fetch fpo address using fpoID...
        $scope.getFpoAddress = function() {
            //console.log("FPO ID is: "+$scope.fpoDetails.id);
            //var fpoId = $scope.fpoDetails.id;
            $state.go('fpoUpdateAddress', { fpoId: fpoId });
        }

        $scope.addFpoAddress = function() {
            $state.go('fpoAddAddress', { fpoId: fpoId });
        }

        //on cancel button click...
        // $scope.goBack = function() {
        //     window.history.back();
        // }

        $scope.updateFpoDetails = function() {
            //console.log($scope.fpo_details);
            $state.go('fpoDetailsUpdate', { fpo_details: JSON.stringify($scope.fpo_details) });
        }

        //QueryService to fetch all address_types....
        QueryService.query('GET', '/address_types', {}, {}).then(function(response) {
            if (response.status == 200) {
                //console.log(response.data);
                $scope.address_types = response.data;

            }
        },function(error){
          console.log(error.data);
        });//end of API call...

        //this function will call on plus button click event to add addresses....
        $scope.addAddress = function(type){
            console.log(type);
            //var fpo_address = JSON.stringify()
            $state.go('addFpoAddress', {fpoId:fpoId,address_type:type});
        } 


        //on cluster add button click....
        $scope.addCluster = function(){
            $state.go('fpoClusterAdd');
        } 

    }
]);//end of controller
