/**
 * Angular module for examples component. This component is divided to following logical components:
 *
 *
 * Each component has it own configuration for ui-router.
 */
(function() {
  'use strict';

  // Define frontend.admin module
  angular.module('fpoApp.Appviews', [
    'fpoApp.Appviews.login',
    'fpoApp.Appviews.fpo',
    'fpoApp.Appviews.layout',
    'fpoApp.Appviews.fpoMoreDetails',
    'fpoApp.Appviews.fpoDetailsUpdate',
    'fpoApp.Appviews.fpogroup',
    'fpoApp.Appviews.fpoGroupDetailsUpdate',
    'fpoApp.Appviews.farmerAddress',
    'fpoApp.Appviews.user',
    'fpoApp.Appviews.kyc',
    'fpoApp.Appviews.family',
    'fpoApp.Appviews.farmer',
    'fpoApp.Appviews.land',
    'fpoApp.Appviews.search'
  ]);

  // Module configuration
  angular.module('fpoApp.Appviews')
    .config([
      '$stateProvider',
      function($stateProvider) {
        $stateProvider
          .state('fpoApp.Appviews', {
            parent: 'fpoApp',
            data: {
              access: 1
            },
            views: {
              'content@': {
                controller: [
                  '$state',
                  function($state) {
                    $state.go('fpoApp.Appviews.fpo');
                  }
                ]
              }
            }
          })
        ;
      }
    ])
  ;
}());
