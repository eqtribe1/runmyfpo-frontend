// Generic models angular module initialize.
(function() {
  'use strict';

  angular.module('fpoApp.components.interceptors', []);
}());
