/**
 * Service to initialize multi-select across Controllers for State,District
 * and Village Selection.
 */
 (function() {
 'use strict';
 angular.module('fpoApp')
 .service('areaMultiSelect', ['QueryService', 'MessageService','$uibModal',
                               areaMultiSelect]);

 function areaMultiSelect(QueryService, MessageService, $uibModal) {

   var modal = function(data) {
       var modalInstance = $uibModal.open({
           animation: true,
           templateUrl: '/fpoApp/views/fpocluster/area_modal.html',
           controller: 'areaModalCtrl',
           backdrop: false,
           keyboard: false,
       });
      //  modalInstance.result.then(function(result) {
      //      $rootScope.nextState = result && result.state;
      //  });
       return modalInstance;
   };

 var customStateSettings = {
     displayProp: 'state_name',
     idProp: 'state_code',
     scrollable: true,
     scrollableHeight: '400px',
     enableSearch: true,
     smartButtonMaxItems: 5,
     smartButtonTextConverter: function(itemText, originalItem) {
                                  return itemText;
                               },
 };

    var customDistrictSettings = {
     displayProp: 'district_name',
     idProp: 'district_code',
     scrollable: true,
     scrollableHeight: '400px',
     enableSearch: true,
     smartButtonMaxItems: 5,
     smartButtonTextConverter: function(itemText, originalItem) {
                                  return itemText;
                               },
 };

   var customVillageSettings = {
     displayProp: 'distVillage',
     idProp: 'village_id',
     scrollable: true,
     scrollableHeight: '400px',
     enableSearch: true,
     smartButtonMaxItems: 5,
     smartButtonTextConverter: function(itemText, originalItem) {
                                  return itemText.split(':',1);
                               },
 };

 return {
   customDistrictSettings: customDistrictSettings,
   customStateSettings: customStateSettings,
   customVillageSettings: customVillageSettings,
   modal: modal,
  // dismissModal: dismiss
}
}
})();
