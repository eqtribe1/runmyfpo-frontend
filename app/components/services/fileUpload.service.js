(function () {
    'use strict';

    angular
        .module('fpoApp.components.services')
        .factory('fileUpload', ['$http','CONSTANTS', '$q','$timeout','QueryService',fileUpload]);

    function fileUpload($http, CONSTANTS, $q, $timeout, QueryService) {

        function uploadFile(file,fileName, url, callback){
               var fd = new FormData();
               // (id) && fd.append('id', id);
               fd.append('mediaType', file.type.split("/")[0]);
               fd.append(fileName, file);
               $http.post(CONSTANTS.API_URL+url, fd, {
                  transformRequest: angular.identity,
                  headers: {'Content-Type': undefined}
               })
                .then(function (response) {
                    callback(response);
                }, function (response) {
                    callback(response);
                });
            };

        function removeFile(apiUrl,fileUrl,method,cb){
            QueryService.query(method,apiUrl,{},{'url':fileUrl}).then(function(response){
                  if(typeof cb == 'function'){
                    cb(response)
                  };
              },function(error){
                console.error(error.data);
              });
        };

        function changeFile(scope,files){
                scope.fileReaderSupported = window.FileReader != null;
                scope.thumbnail = {
                    dataUrl: ''
                };
                if (files != null) {
                    var file = files[0];
                    scope.selectedFile = file;
                    if (scope.fileReaderSupported) {
                        $timeout(function() {
                            var fileReader = new FileReader();
                            fileReader.readAsDataURL(file);
                            fileReader.onload = function(e) {
                                $timeout(function() {
                                    scope.thumbnail.dataUrl = e.target.result;
                                });
                            };
                        });
                    };
                };
        }

        var service = {};
        service.uploadFile = uploadFile;
        service.removeFile = removeFile;
        service.changeFile = changeFile;
        return service;

    }

})();