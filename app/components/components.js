/**
 * Angular module for 'components' component.
 */
;(function() {
  'use strict';

  // Define fpoApp.components module
  angular.module('fpoApp.components', [
    // 'frontend.core.dependencies', // Note that this must be loaded first
    // 'frontend.core.auth',
    // 'frontend.core.components',
    'fpoApp.components.directives',
    'fpoApp.components.interceptors',
    // 'frontend.core.layout',
    'fpoApp.components.services'
  ]);
}());
