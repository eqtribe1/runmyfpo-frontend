angular.module('fpoApp.routes')
.config(['$stateProvider', '$urlRouterProvider', 'CONSTANTS',
function($stateProvider, $urlRouterProvider, CONSTANTS) {
$stateProvider
.state('farmerDetails', {
    url: '/farmerDetails/:farmerId',
    parent: 'master',
    params:{farmerId: null},
    views: {
        'content@': {
            //templateUrl: '/fpoApp/views/fpo/fpo-member-details.html',
            templateUrl: '/fpoApp/views/fpo/farmer-dashboard.html',
            controller: 'FpoMembersDetailsCtrl'
        }
    },
    data: {
        access: CONSTANTS.AccessLevels.user,
        screen_title: 'Dashboard'
      }
})
.state('farmerDetailsUpdate', {
    url: '/farmerDetailsUpdate/:farmerId',
    parent: 'master',
    views: {
        'content@': {
            templateUrl: '/fpoApp/views/fpo/farmer-details-update.html',
            controller: 'FpoMembersDetailsUpdateCtrl'
        }
    },
    data: {
        access: CONSTANTS.AccessLevels.user,
        screen_title: 'Update Details'
      }
})
.state('update-farmer-address', {
    url: '/update-farmer-address/:farmerId',
    parent: 'master',
    views: {
        'content@': {
            // templateUrl: '/fpoApp/views/fpo/farmer-update-address.html',
            templateUrl: '/fpoApp/views/address/address.html',
            controller: 'UpdateFarmerAddressCtrl'
        }
    },
    data: {
        access: CONSTANTS.AccessLevels.user,
        screen_title:'Update Address for'
      }
})
.state('add-farmer-address', {
    url: '/add-farmer-address/:farmerId',
    parent: 'master',
    views: {
        'content@': {
            //templateUrl: '/fpoApp/views/fpo/farmer-add-address.html',
            templateUrl: '/fpoApp/views/address/address.html',
            controller: 'AddFarmerAddressCtrl'
        }
    },
    data: {
        access: CONSTANTS.AccessLevels.user,
        screen_title:'Add Address for'
      }
})
.state('farmerEndowments', {
    url: '/farmerEndowments/:farmerId/:farmerName',
    parent: 'master',
    views: {
        'content@': {
            templateUrl: '/fpoApp/views/fpo/farmer-endowments.html',
            controller: 'FpoMembersEndowmentsCtrl'
        }
    },
    data: {
        access: CONSTANTS.AccessLevels.user,
        screen_title:'Family & Land Holdings'
      }
})
.state('farmerInputs', {
    url: '/farmerInputs/:farmerId',
    parent: 'master',
    views: {
        'content@': {
            templateUrl: '/fpoApp/views/fpo/farmer-inputs.html',
            controller: 'FpoMembersInputsCtrl'
        }
    },
    data: {
        access: CONSTANTS.AccessLevels.user,
        screen_title:'Inputs'
      }
})
.state('farmer_moreDetails', {
    url: '/moreDetails/:farmerId',
    parent: 'master',
    views: {
        'content@': {
            templateUrl: '/fpoApp/views/fpo/fpo-member-details-more.html',
            controller: 'FpoMembersMoreDetailsCtrl'
        }
    },
    data: {
        access: CONSTANTS.AccessLevels.user,
        screen_title:'Details'
      }
})
//KYC Documents......
.state('list-kyc', {
    url: '/list-kyc',
    parent: 'master',
    views: {
        'content@': {
            templateUrl: '/fpoApp/views/kyc/list-kyc-doc.html',
            controller: 'ListKycCtrl'
        }
    },
    data: {
        access: CONSTANTS.AccessLevels.user,
        screen_title: 'KYC List'
      }
})
.state('add-kyc', {
    url: '/add-kyc/:farmerId',
    parent: 'master',
    views: {
        'content@': {
            templateUrl: '/fpoApp/views/kyc/add-kyc-doc.html',
            controller: 'AddKycCtrl'
        }
    },
    data: {
        access: CONSTANTS.AccessLevels.user,
        screen_title:'Add New KYC Document'
      }
})
.state('update-kyc', {
    url: '/update-kyc/:kyc',
    parent: 'master',
    views: {
        'content@': {
            templateUrl: '/fpoApp/views/kyc/update-kyc-doc.html',
            controller: 'UpdateKycCtrl'
        }
    },
    data: {
        access: CONSTANTS.AccessLevels.user,
        screen_title:'Update KYC Document'
      }
})
//Family Details.......
.state('list-family-member', {
    url: '/list-family-member',
    parent: 'master',
    views: {
        'content@': {
            templateUrl: '/fpoApp/views/family/list-family-member.html',
            controller: 'ListFamilyMemberCtrl'
        }
    },
    data: {
        access: CONSTANTS.AccessLevels.user,
        screen_title: 'Family Member List'
      }
})
.state('add-family-member', {
    url: '/add-family-member/:farmerId/:familyMemberName',
    parent: 'master',
    views: {
        'content@': {
            templateUrl: '/fpoApp/views/family/add-family-member.html',
            controller: 'AddFamilyMemberCtrl'
        }
    },
    data: {
        access: CONSTANTS.AccessLevels.user,
        screen_title: 'Add New Family Member' 
      }
})
.state('update-family-member', {
    url: '/update-family-member/:familyMemberId',
    parent: 'master',
    views: {
        'content@': {
            templateUrl: '/fpoApp/views/family/update-family-member.html',
            controller: 'UpdateFamilyMemberCtrl'
        }
    },
    data: {
        access: CONSTANTS.AccessLevels.user,
        screen_title: 'Update Details'
      }
})
.state('detail-family-member', {
    url: '/detail-family-member/:family_member_id/:farmerName',
    parent: 'master',
    params: {'familyMember':null},
    views: {
        'content@': {
            templateUrl: '/fpoApp/views/family/detail-family-member.html',
            controller: 'DetailFamilyMemberCtrl'
        }
    },
    data: {
        access: CONSTANTS.AccessLevels.user,
        screen_title: 'Details'
      }
})
.state('add-family-member-earning', {
    url: '/add-family-member-earning/:id/:familyMemberName',
    parent: 'master',
    views: {
        'content@': {
            templateUrl: '/fpoApp/views/family/add-family-member-earning.html',
            controller: 'AddFamilyMemberEarningCtrl'
        }
    },
    data: {
        access: CONSTANTS.AccessLevels.user,
        screen_title:'Add Family Member'
      }
})
.state('update_earning', {
    url: '/update_earning/:familyMemberName/:earning',
    parent: 'master',
    views: {
        'content@': {
            templateUrl: '/fpoApp/views/family/update-family-member-earning.html',
            controller: 'UpdateFamilyMemberEarningCtrl'
        }
    },
    data: {
        access: CONSTANTS.AccessLevels.user,
        screen_title:'Update Details'
      }
})
/*landholding details.....*/
.state('list-landholding', {
    url: 'list-landholding/:farmerId',
    parent: 'master',
    views: {
        'content@': {
            templateUrl: '/fpoApp/views/land/list-land-holdings.html',
            controller: 'ListLandHoldingCtrl'
        }
    },
    data: {
        access: CONSTANTS.AccessLevels.user,
        screen_title:'Landholding Lists'
      }
})
.state('add-landholding', {
    url: '/add-landholding/:farmerId',
    parent: 'master',
    views: {
        'content@': {
            templateUrl: '/fpoApp/views/land/add-land-holdings.html',
            controller: 'AddLandHoldingCtrl'
        }
    },
    data: {
        access: CONSTANTS.AccessLevels.user,
        screen_title: 'Add New Land Holding'
      }
})
.state('land-detail', {
    url: '/land-detail/:landholdingId',
    parent: 'master',
    views: {
        'content@': {
            templateUrl: '/fpoApp/views/land/detail-particular-land-holding.html',
            controller: 'DetailLandholdingCtrl'
        }
    },
    data: {
        access: CONSTANTS.AccessLevels.user,
        screen_title: 'Land Holding Details'
      }
})
.state('update_landholdings', {
    url: '/update_landholdings',
    parent: 'master',
    params: {landholding:null},
    views: {
        'content@': {
            templateUrl: '/fpoApp/views/land/update_land_holdings.html',
            controller: 'UpdateLandholdingCtrl'
        }
    },
    data: {
        access: CONSTANTS.AccessLevels.user,
        screen_title:'Update'
      }
})
.state('land-usage-detail', {
    url: '/land-usage-detail/:landName/:land_usage',
    parent: 'master',
    views: {
        'content@': {
            templateUrl: '/fpoApp/views/land/land_usage_detail.html',
            controller: 'LandUsageDetailCtrl'
        }
    },
    data: {
        access: CONSTANTS.AccessLevels.user,
        screen_title:'Land Usage Details'
      }
})
.state('add_landUsage', {
    url: '/add_landUsage/:landId',
    parent: 'master',
    params:{landId:null},
    views: {
        'content@': {
            templateUrl: '/fpoApp/views/land/add-land-holdings-usage.html',
            controller: 'LandholdingUsageCtrl'
        }
    },
    data: {
        access: CONSTANTS.AccessLevels.user,
        screen_title:'Add Land Holding Usage Details'
      }
})
//for seasonal cultivation.....
.state('farmer', {
    url: '/farmers/:farmerId',
    parent: 'master',
    views: {
        'content@': {
            templateUrl: '/fpoApp/views/land/seasonal-cultivation.html',
            controller: 'SeasonalCultivationCtrl'
        }
    },
    data: {
        access: CONSTANTS.AccessLevels.user,
        screen_title: 'Seasonal Cultivation'
      }
})

.state('add_seasonal_cultivation', {
url: '/add_seasonal_cultivation/:farmerId',
parent: 'master',
views: {
    'content@': {
        templateUrl: '/fpoApp/views/land/add-seasonal-cultivation.html',
        controller: 'AddSeasonalCultivationCtrl'
    }
},
data: {
    access: CONSTANTS.AccessLevels.user,
    screen_title: 'Add Seasonal Cultivation'
  }
})

.state('update_seasonal_cultivation', {
url: '/update_seasonal_cultivation',
parent: 'master',
params: {farmerId:null, landType:null},
views: {
    'content@': {
        templateUrl: '/fpoApp/views/land/update-seasonal-cultivation.html',
        controller: 'UpdateSeasonalCultivationCtrl'
    }
},
data: {
    access: CONSTANTS.AccessLevels.user,
    screen_title: 'Update Seasonal Cultivation Details'
  }
})

//fpo-services usages.....
.state('fpo_services_usage', {
url: '/fpo_services_usage/:farmerShare',
parent: 'master',
params:{farmerShare:null},
views: {
    'content@': {
        templateUrl: '/fpoApp/views/fpo/fpo-services.html',
        controller: 'FPOServicesCtrl'
    }
},
data: {
    access: CONSTANTS.AccessLevels.user,
    screen_title: 'FPO Services Used' 
  }
})
.state('searchFarmer', {
    url: '/searchFarmer',
    parent: 'master',
    views: {
        'content@': {
            templateUrl: '/fpoApp/views/search/farmer-search.html',
            controller: 'SearchFarmerCtrl'
        }
    },
    data: {
        access: CONSTANTS.AccessLevels.user,
        screen_title: 'Search Farmer'
      }
})
.state('upload-farmer-avtar',{
    url:'/person/:personId/avtar_upload',
    parent:'master',
    views:{
        'content@':{
            templateUrl:'/fpoApp/views/farmer/file_upload.html',
            controller:'FarmerAvtarUpdateCtrl'
        }
    },
    data: {
        access: CONSTANTS.AccessLevels.user,
        screen_title: 'Upload'
      }
})
}]);
