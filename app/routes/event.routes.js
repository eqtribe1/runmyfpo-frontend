angular.module('fpoApp.routes')
    .config(['$stateProvider', '$urlRouterProvider', 'CONSTANTS',
        function($stateProvider, $urlRouterProvider, CONSTANTS) {
            $stateProvider
            // ------For Events and Trainings----------

                .state('eventsList', {
                url: '/eventsList/:fpoId',
                parent: 'master',
                views: {
                    'content@': {
                        templateUrl: '/fpoApp/views/events/events-list.html',
                        controller: 'EventListCtrl'
                    }
                },
                data: {
                    access: CONSTANTS.AccessLevels.user,
                    screen_title: 'Event Lists'
                }
            })

            .state('eventDetails', {
                    url: '/eventDetails/:eventId',
                    parent: 'master',
                    params: { eventId: null },
                    views: {
                        'content@': {
                            templateUrl: '/fpoApp/views/events/event-detail.html',
                            controller: 'EventDetailsCtrl'
                        }
                    },
                    data: {
                        access: CONSTANTS.AccessLevels.user
                    }
                })
                .state('create-event', {
                    url: '/create-event/:event_type',
                    parent: 'master',
                    params: { event_type: null },
                    views: {
                        'content@': {
                            templateUrl: '/fpoApp/views/events/create_event.html',
                            controller: 'CreateEventCtrl'
                        }
                    },
                    data: {
                        access: CONSTANTS.AccessLevels.user
                    }
                })

            .state('update-event', {
                url: '/update-event/:eventId',
                parent: 'master',
                params: { eventId: null },
                views: {
                    'content@': {
                        templateUrl: '/fpoApp/views/events/create_event.html',
                        controller: 'UpdateEventCtrl'
                    }
                },
                data: {
                    access: CONSTANTS.AccessLevels.user
                }
            })

            .state('event-invite',{
                url:'/event-invite/:invite',
                parent:'master',
                params:{ invite: null },
                views:{
                    'content@':{
                        templateUrl:'/fpoApp/views/events/event-invite.html',
                        controller:'EventInviteCtrl'
                    }
                },
                data:{
                    access:CONSTANTS.AccessLevels.user,
                    screen_title:'Invite for Event'
                }
            })

            .state('add-event-address', {
                url: '/add-event-address/:eventId',
                parent: 'master',
                params: { eventId: null },
                views: {
                    'content@': {
                        templateUrl: '/fpoApp/views/address/address.html',
                        controller: 'AddEventAddressCtrl'
                    }
                },
                data: {
                    access: CONSTANTS.AccessLevels.user,
                    screen_title: 'Add Address for'
                }
            })

            .state('update-event-address', {
                    url: '/update-event-address/:eventId',
                    parent: 'master',
                    params: { eventId: null },
                    views: {
                        'content@': {
                            templateUrl: '/fpoApp/views/address/address.html',
                            controller: 'UpdateEventAddressCtrl'
                        }
                    },
                    data: {
                        access: CONSTANTS.AccessLevels.user,
                        screen_title: 'Update Address for'
                    }
                })
                .state('event_attendance', {
                    url: '/event-attendance/:eventId',
                    parent: 'master',
                    params: { eventId: null },
                    views: {
                        'content@': {
                            templateUrl: '/fpoApp/views/events/event_attendance.html',
                            controller: 'EventAttendanceCtrl'
                        }
                    },
                    data: {
                        access: CONSTANTS.AccessLevels.user
                    }
                })
                .state('eventFileUpload', {
                    url: '/events/file-upload',
                    parent: 'master',
                    views: {
                        'content@': {
                            templateUrl: '/fpoApp/views/events/file-upload.html',
                            controller: 'FileUploadCtrl'
                        }
                    },
                    data: {
                        access: CONSTANTS.AccessLevels.user
                    }
                })
                .state('searchEvent', {
                    url: '/event/search',
                    parent: 'master',
                    views: {
                        'content@': {
                            templateUrl: '/fpoApp/views/events/search_event.html',
                            controller: 'EventSearchCtrl'
                        }
                    },
                    data: {
                        access: CONSTANTS.AccessLevels.user
                    }
                })
                .state('add-associated-media', {
                    url: '/event/:eventId/add_associated_media',
                    parent: 'master',
                    views: {
                        'content@': {
                            templateUrl: '/fpoApp/views/farmer/file_upload.html',
                            controller: 'EventAssociatedMediaCtrl'
                        }
                    },
                    data: {
                        access: CONSTANTS.AccessLevels.user
                    }
                });

        }
    ]);
