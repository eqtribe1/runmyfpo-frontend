angular.module('fpoApp.routes')
.config(['$stateProvider', '$urlRouterProvider', 'CONSTANTS',
	function($stateProvider, $urlRouterProvider, CONSTANTS) {
		
		$stateProvider

		// .state('shop-dashboard',{
		// 	url:'/shop-dashboard',
		// 	parent: 'master',
		// 	views: {
		//         'content@': {
		//             templateUrl: '/fpoApp/views/fposhop/fpo_shop_dashboard.html',
		//             controller: 'FpoShopDashboardCtrl'
		//         }
		//     },
		//     data:{
		//         access: CONSTANTS.AccessLevels.user
		//     },
		// })
        
        .state('new-material-category',{
			url:'/new-material-category',
			parent: 'master',
			views: {
		        'content@': {
		            templateUrl: '/fpoApp/views/inputs/new-material-category.html',
		            controller: 'InputCtrl'
		        }
		    },
		    data:{
		        access: CONSTANTS.AccessLevels.user
		    },
		})
        
        .state('new-material-attribute',{
			url:'/new-material-attribute',
			parent: 'master',
			views: {
		        'content@': {
		            templateUrl: '/fpoApp/views/inputs/new-material-attribute.html',
		            controller: 'InputCtrl'
		        }
		    },
		    data:{
		        access: CONSTANTS.AccessLevels.user
		    },
		})
        
        .state('add-product',{
			url:'/add-product',
			parent: 'master',
			views: {
		        'content@': {
		            templateUrl: '/fpoApp/views/inputs/add-product.html',
		            controller: 'InputCtrl'
		        }
		    },
		    data:{
		        access: CONSTANTS.AccessLevels.user
		    },
		})
		
		.state('add-purchase-record',{
			url:'/add-purchase-record',
			parent: 'master',
			views: {
		        'content@': {
		            templateUrl: '/fpoApp/views/inputs/add-purchase-record.html',
		            controller: 'InputCtrl'
		        }
		    },
		    data:{
		        access: CONSTANTS.AccessLevels.user
		    },
		})
		
		.state('add-material-sku',{
			url:'/add-material-sku',
			parent:'master',
			views:{
				'content@':{
					templateUrl:'/fpoApp/views/inputs/add-material-sku-definition.html',
					controller:'InputCtrl'
				}
			},
			data:{
				access:CONSTANTS.AccessLevels.user
			},

		})
		.state('add-goods-received-record',{
			url:'/add-goods-received-record',
			parent:'master',
			views:{
				'content@':{
					templateUrl:'/fpoApp/views/inputs/add-goods-received-record.html',
					controller:'InputCtrl'
				}
			},
			data:{
				access:CONSTANTS.AccessLevels.user
			},

		})
	
		.state('current-stock-summary',{
			url:'/current-stock-summary',
			parent:'master',
			views:{
				'content@':{
					templateUrl:'/fpoApp/views/inputs/current-stock-summary.html',
					controller:'InputCtrl'
				}
			},
			data:{
				access:CONSTANTS.AccessLevels.user
			},
		})
		.state('sku-stock-details',{
			url:'/sku-stock-details',
			parent:'master',
			views:{
				'content@':{
					templateUrl:'/fpoApp/views/inputs/sku-stock-details.html',
					controller:'InputCtrl'
				}
			},
			data:{
				access:CONSTANTS.AccessLevels.user
			},
		})
		
		.state('add-sales-record',{
			url:'/add-sales-record',
			parent:'master',
			views:{
				'content@':{
					templateUrl:'/fpoApp/views/inputs/add-sales-record.html',
					controller:'InputCtrl'
				}
			},
			data:{
				access:CONSTANTS.AccessLevels.user
			},
		})
	}]);