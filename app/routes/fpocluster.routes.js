angular.module('fpoApp.routes')
  .config(['$stateProvider', '$urlRouterProvider', 'CONSTANTS',
  function($stateProvider, $urlRouterProvider, CONSTANTS) {
          $stateProvider
          /* ----- FPO Cluster Routes  ----- */
          .state('fpoClusterAdd', {
             url: '/fpo_cluster/add_cluster',
             parent: 'master',
             views: {
                  'content@': {
                      templateUrl: '/fpoApp/views/fpocluster/add-fpo-cluster.html',
                      controller: 'FpoClusterAddCtrl'
                    }
            },
            data: {
                  access: CONSTANTS.AccessLevels.user,
                  screen_title: 'Add a Cluster'
                  },
            })
            .state('fpoClusterAddVillages', {
               url: '/update_fpo_cluster/:clusterId',
               parent: 'master',
               views: {
                    'content@': {
                        templateUrl: '/fpoApp/views/fpocluster/update-fpo-cluster.html',
                        controller: 'FpoClusterDetailsUpdateCtrl'
                      }
              },
            data: {
                  access: CONSTANTS.AccessLevels.user,
                  screen_title: 'Update Villages in FPO Cluster'
                  },
            })
          .state('fpoClusterDetail', {
            url: '/fpo_cluster/:clusterId',
            parent: 'master',
            views: {
                    'content@': {
                     templateUrl: '/fpoApp/views/fpocluster/fpo-cluster.html',
                     controller: 'FpoClusterDetailsCtrl'
                    }
              },
            data: {
                    access: CONSTANTS.AccessLevels.user,
                    screen_title: 'FPO Cluster Details'
                  }
            })
        }
    ]);
