angular.module('fpoApp.routes')
.config(['$stateProvider', '$urlRouterProvider', 'CONSTANTS',
function($stateProvider, $urlRouterProvider, CONSTANTS) {
$stateProvider
/* ----- FPOGroup  ----- */
.state('fpoGroupCreate', {
        url: '/fpoGroupCreate/:group_type',
        parent: 'master',
        views: {
            'content@': {
                templateUrl: '/fpoApp/views/fpogroup/add-fpo-group.html',
                controller: 'fpoGroupCreateController'
            }
        },
        data: {
            access: CONSTANTS.AccessLevels.user,
            screen_title: 'Create FPO Group'
          },
    })
    .state('/fpo_group', {
        url: '/fpo_groups/:fid',
        parent: 'master',
        views: {
            'content@': {
                templateUrl: '/fpoApp/views/fpogroup/fpo-group-list.html',
                controller: 'fpoGroupDetailController'
            }
        },
        data: {
            access: CONSTANTS.AccessLevels.user,
            screen_title: 'FPO Members'
          }
    })
    .state('AddMemberFarmer', {
        url: '/AddMemberFarmer/:fpoGroup',
        parent: 'master',
        views: {
            'content@': {
                templateUrl: '/fpoApp/views/fpogroup/fpogroup-add-member.html',
                controller: 'addFpoGroupMemberController'
            }
        },
        data: {
            access: CONSTANTS.AccessLevels.user,
            screen_title: 'Add FPO Member/Farmer'
          }
    })
    .state('fpoGroupDetails', {
        url: '/fpoGroupDetails/:fpogroupId',
        parent: 'master',
        views: {
            'content@': {
                templateUrl: '/fpoApp/views/fpogroup/fpogroup-details.html',
                controller: 'fpoGroupDetailController'
            }
        },
        data: {
            access: CONSTANTS.AccessLevels.user,
            screen_title: 'Details'
          }
    })
    .state('updateFpoGroupDetails', {
        url: '/updateFpoGroupDetails/:fpoGroupDetails',
        parent: 'master',
        params:{fpoGroupDetails:null},
        views: {
            'content@': {
                templateUrl: '/fpoApp/views/fpogroup/update_fpo_group_details.html',
                controller: 'UpdateFpoGroupDetailsCtrl'
            }
        },
        data: {
            access: CONSTANTS.AccessLevels.user,
            screen_title: 'FPO Details'

          }
    })

// /* ----- User Registration ----- */
// .state('userList', {
//         url: '/userList',
//         parent: 'master',
//         params: {'user': null},
//         views: {
//             'content@': {
//                 templateUrl: '/fpoApp/views/user/user-list-fpo-wise.html',
//                 controller: 'UserListCtrl'
//             }
//         },
//         data: {
//             access: CONSTANTS.AccessLevels.user,
//             screen_title:'FPO Management'
//           }
//     })
//     .state('newRegister', {
//         url: '/new_registation',
//         views: {
//             'content@': {
//                 templateUrl: '/fpoApp/views/user/user-registration.html',
//                 controller: 'UserRegistrationCtrl'
//             }
//         },
//         data: {
//             access: CONSTANTS.AccessLevels.anon,
//             screen_title: 'User Registration'
//           }
//     })
//     .state('update-user-profile', {
//         url: '/update-user-profile/:user',
//         parent: 'master',
//         params:{user:null},
//         views: {
//             'content@': {
//                 templateUrl: '/fpoApp/views/user/update-user-profile.html',
//                 controller: 'UpdateUserProfileCtrl'
//             }
//         },
//         data: {
//             access: CONSTANTS.AccessLevels.user,
//             screen_title: 'Update User Profile'
//           }
//     })

//     .state('add-user', {
//         url: '/add-user/:role',
//         parent: 'master',
//         views: {
//             'content@': {
//                 templateUrl: '/fpoApp/views/user/inviteUser.html',
//                 controller: 'UserListCtrl'
//             }
//         },
//         data: {
//             access: CONSTANTS.AccessLevels.user,
//             screen_title:'Invite FPO'
//           }
//     })


//     .state('userAddSuccess', {
//         url: '/userAddSuccess',
//         parent: 'master',
//         views: {
//             'content@': {
//                 templateUrl: '/fpoApp/views/user/user-list.html',
//                 controller: 'UserListCtrl',
//                 template: '<span style="color: green;">Registered Successfully!</span>'
//             }
//         },
//         data: {
//             access: CONSTANTS.AccessLevels.user
//           }
//     })

    .state('feedback', {
                url: '/feedback',
                parent:'master',
                params:{screen_title:null, stateIdentifier:null },
                views: {
                  'content@': {
                    templateUrl: '/fpoApp/views/feedback/add-feedback.html',
                    controller: 'FeedbackCtrl'
                  }
                },
                 data: {
            access: CONSTANTS.AccessLevels.user,
            screen_title:'Submit Feedback'
          }
            })

       .state('feedbackList', {
                url: '/feedbackList/:userId',
                parent:'master',
                views: {
                  'content@': {
                    templateUrl: '/fpoApp/views/feedback/list-past-feedbacks.html',
                    controller: 'FeedbackListCtrl'
                  }
                },
                 data: {
            access: CONSTANTS.AccessLevels.user,
            screen_title:'Past Feedback'
          }
            });


}]);
