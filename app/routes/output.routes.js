angular.module('fpoApp.routes')
.config(['$stateProvider', '$urlRouterProvider', 'CONSTANTS',
	function($stateProvider, $urlRouterProvider, CONSTANTS) {
		
		$stateProvider

		.state('output-purchase-invoice',{
			url:'/output-purchase-invoice',
			parent: 'master',
			views: {
		        'content@': {
		            templateUrl: '/fpoApp/views/outputs/output_purchase_invoice.html',
		            controller: 'OutputPurchaseInvoiceCtrl'
		        }
		    },
		    data:{
		        access: CONSTANTS.AccessLevels.user
		    },
		})

		.state('output-sale-invoice',{
			url: '/output-sale-invoice',
			parent:'master',
			views:{
				'content@':{
					templateUrl:'/fpoApp/views/outputs/output_sale_invoice.html',
					controller: 'OutputSaleInvoiceCtrl'
				}
			},
			data:{
				access:CONSTANTS.AccessLevels.user
			},
		})
	}]);