angular.module('fpoApp.routes')
    .config(['$stateProvider', '$urlRouterProvider', 'CONSTANTS',
        function($stateProvider, $urlRouterProvider, CONSTANTS) {
            $stateProvider
                .state('fpoSelect', {
                    url: '/fpoSelect',
                    parent: 'master',
                    views: {
                        'content@': {
                            templateUrl: '/fpoApp/views/fpo/fpo-select.html',
                            controller: 'FpoSelectCtrl'
                        }
                    },
                    data: {
                        access: CONSTANTS.AccessLevels.user,
                        screen_title: 'Welcome'
                    },
                })
                //FPO Controller......
                .state('fpoCreate', {
                    url: '/fpoCreate',
                    parent: 'master',
                    views: {
                        'content@': {
                            templateUrl: '/fpoApp/views/fpo/create-fpo.html',
                            controller: 'fpoCreateCtrl'
                        }
                    },
                    data: {
                        access: CONSTANTS.AccessLevels.user,
                        screen_title: 'Add New FPO'
                    }
                })
                .state('fpoHome', {
                    url: '/fpoHome',
                    parent: 'master',
                    views: {
                        'content@': {
                            templateUrl: '/fpoApp/views/fpo/fpo-home.html',
                            controller: 'FpoHomeCtrl'
                        }
                    },
                    data: {
                        access: CONSTANTS.AccessLevels.user,
                        screen_title: 'Dashboard'
                    }
                })
        
                .state('fpoDetails', {
                    url: '/fpoDetails',
                    parent: 'master',
                    views: {
                        'content@': {
                            templateUrl: '/fpoApp/views/fpo/fpo-details.html',
                            controller: 'FpoDetailsCtrl'
                        }
                    },
                    data: {
                        access: CONSTANTS.AccessLevels.user,
                        screen_title: 'Details'
                    }
                })
                .state('fpoMoreDetails', {
                    url: '/fpoMoreDetails/:fpoId',
                    parent: 'master',
                    views: {
                        'content@': {
                            templateUrl: '/fpoApp/views/fpo/fpo-more-details.html',
                            controller: 'FpoMoreDetailsCtrl'
                        }
                    },
                    data: {
                        access: CONSTANTS.AccessLevels.user,
                        screen_title: 'Details'
                    }
                })
                .state('fpoDetailsUpdate', {
                    url: '/fpoDetailsUpdate/:fpo_details',
                    parent: 'master',
                    views: {
                        'content@': {
                            templateUrl: '/fpoApp/views/fpo/fpo-details-update.html',
                            controller: 'FpoDetailsUpdateCtrl'
                        }
                    },
                    data: {
                        access: CONSTANTS.AccessLevels.user,
                        screen_title: 'Update FPO Details'
                    }
                })
                .state('fpoMemberList', {
                    url: '/fpoMemberList/:fpoId',
                    parent: 'master',
                    views: {
                        'content@': {
                            templateUrl: '/fpoApp/views/fpo/fpo-member-list.html',
                            controller: 'FpoMembersListCtrl'
                        }
                    },
                    data: {
                        access: CONSTANTS.AccessLevels.user,
                        screen_title: 'Members'
                    }
                })
                .state('addFpoMember', {
                    url: '/addFpoMember',
                    parent: 'master',
                    views: {
                        'content@': {
                            templateUrl: '/fpoApp/views/fpo/fpo-add-members.html',
                            controller: 'FpoMembersAddCtrl'
                        }
                    },
                    data: {
                        access: CONSTANTS.AccessLevels.user,
                        screen_title: 'Add New FPO Farmer/Member'
                    }
                })
                .state('fpoAddAddress', {
                    url: '/fpoAddAddress/:fpoId',
                    parent: 'master',
                    views: {
                        'content@': {
                            // templateUrl: '/fpoApp/views/fpo/fpo-add-address.html',
                            templateUrl: '/fpoApp/views/address/address.html',
                            controller: 'AddFpoAddressCtrl'
                        }
                    },
                    data: {
                        access: CONSTANTS.AccessLevels.user,
                        screen_title: 'Add Address for'
                    }
                })
                .state('fpoUpdateAddress', {
                    url: '/fpoUpdateAddress/:fpoId',
                    parent: 'master',
                    views: {
                        'content@': {
                            // templateUrl: '/fpoApp/views/fpo/fpo-update-address.html',
                            templateUrl: '/fpoApp/views/address/address.html',
                            controller: 'UpdateFpoAddressCtrl'
                        }
                    },
                    data: {
                        access: CONSTANTS.AccessLevels.user,
                        screen_title: 'Update Address for'
                    }
                })
                //this state is used for adding address for fpo shops and warehouses...
                .state('addFpoAddress',{
                  url:'/fpoAddress/:fpoId/:address_type',
                  parent:'master',
                  /*params:{'fpo_address_type':null},*/
                  views:{
                    'content@':{
                      templateUrl:'/fpoApp/views/address/address.html',
                      controller:'AddOtherFPOAddressCtrl'
                    }
                  },
                  data:{
                    access:CONSTANTS.AccessLevels.user,
                    screen_title:'Add Address for'
                  }
                })
                .state('fpoUpdateShare', {
                    url: '/fpoUpdateShare/:farmerShare',
                    parent: 'master',
                    views: {
                        'content@': {
                            templateUrl: '/fpoApp/views/fpo/fpo-member-update-share-details.html',
                            controller: 'FpoShareCtrl'
                        }
                    },
                    data: {
                        access: CONSTANTS.AccessLevels.user,
                        screen_title: 'Update Share Details'
                    }
                })
        }
    ]);
