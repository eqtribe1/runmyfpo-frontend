angular.module('fpoApp.routes')
.config(['$stateProvider', '$urlRouterProvider', 'CONSTANTS',
function($stateProvider, $urlRouterProvider, CONSTANTS) {
$stateProvider
.state('logout', {
    url: '/logout',
    controller: 'loginCtrl',
    data: {
          access: CONSTANTS.AccessLevels.anon
      },
})
.state('login', {
    url: '/login',
    views: {
      'content@': {
        templateUrl: '/fpoApp/views/login/login.html',
        controller: 'loginCtrl'
      }
    },
    data: {
          access: CONSTANTS.AccessLevels.anon,
          screen_title: 'Login To RunMyFPO'
      },
})
}]);
