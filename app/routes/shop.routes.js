angular.module('fpoApp.routes')
    .config(['$stateProvider', '$urlRouterProvider', 'CONSTANTS',
        function($stateProvider, $urlRouterProvider, CONSTANTS) {
            $stateProvider
            // ------For FPO Shop----------

            .state('shop-dashboard', {
                    url: '/shop-dashboard',
                    parent: 'master',
                    views: {
                        'content@': {
                            templateUrl: '/fpoApp/views/fposhop/fpo_shop_dashboard.html',
                            controller: 'FpoShopDashboardCtrl'
                        }
                    },
                    data: {
                        access: CONSTANTS.AccessLevels.user
                    },
                })

            .state('recordPurchase', {
                url: '/record-purchase',
                parent: 'master',
                views: {
                    'content@': {
                        templateUrl: '/fpoApp/views/fposhop/record_purchase.html',
                        controller: 'PurchaseCtrl'
                    }
                },
                data: {
                    access: CONSTANTS.AccessLevels.user,
                    screen_title: 'Record Purchase'
                }
            })

            .state('addRecordPurchase', {
                    url: '/record_purchase',
                    parent: 'master',
                    views: {
                        'content@': {
                            templateUrl: '/fpoApp/views/fposhop/payment_details.html',
                            controller: 'PurchaseCtrl'
                        }
                    },
                    data: {
                        access: CONSTANTS.AccessLevels.user,
                        screen_title: 'Fpo shop record purchase'
                    }
                })
                .state('list-purchases', {
                    url: '/list-purchases',
                    parent: 'master',
                    views: {
                        'content@': {
                            templateUrl: '/fpoApp/views/fposhop/list_purchase_records.html',
                            controller: 'PurchaseListCtrl'
                        }
                    },
                    data: {
                        access: CONSTANTS.AccessLevels.user,
                        screen_title: 'FPO Purchases List'
                    }
                })
                .state('purchaseDetails', {
                    url: '/purchaseDetails/:purchaseId',
                    parent: 'master',
                    params: { purchaseId: null },

                    views: {
                        'content@': {
                            templateUrl: '/fpoApp/views/fposhop/purchase_details_view.html',
                            controller: 'PurchaseDetailsViewCtrl'
                        }
                    },
                    data: {
                        access: CONSTANTS.AccessLevels.user,
                        screen_title: 'Purchase Details'
                    }
                })

            .state('addVendor', {
                url: '/addVendor/:fpoId',
                parent: 'master',
                views: {
                    'content@': {
                        templateUrl: '/fpoApp/views/fposhop/add_vendor.html',
                        controller: 'AddVendorCtrl'
                    }
                },
                data: {
                    access: CONSTANTS.AccessLevels.user,
                    screen_title: 'Add Vendor'
                }
            })

            .state('addVendorAddress', {
                url: '/addVendorAddress/:vendorId',
                parent: 'master',
                views: {
                    'content@': {
                        // templateUrl: '/fpoApp/views/fpo/fpo-add-address.html',
                        templateUrl: '/fpoApp/views/address/address.html',
                        controller: 'AddVendorAddressCtrl'
                    }
                },
                data: {
                    access: CONSTANTS.AccessLevels.user,
                    screen_title: 'Add Address for'
                }
            })

            .state('outputRecord', {
                url: '/outputRecord',
                parent: 'master',
                views: {
                    'content@': {
                        // templateUrl: '/fpoApp/views/fpo/fpo-add-address.html',
                        templateUrl: '/fpoApp/views/fposhop/output_record_purchase.html',
                        controller: 'outputPurchaseCtrl'
                    }
                },
                data: {
                    access: CONSTANTS.AccessLevels.user,
                    screen_title: ''
                }
            })

            .state('paymentDetails', {
                    url: '/paymentDetails/:purchaseId/:type/:id/:fpoId/',
                    parent: 'master',
                    views: {
                        'content@': {
                            templateUrl: '/fpoApp/views/fposhop/payment_details.html',
                            controller: 'paymentDetailsCtrl'
                        }
                    },
                    data: {
                        access: CONSTANTS.AccessLevels.user,
                        screen_title: ''
                    }
                })
                .state('purchased-goods-received', {
                    url: '/purchased-goods-received/:purchaseId',
                    parent: 'master',
                    views: {
                        'content@': {
                            templateUrl: '/fpoApp/views/fposhop/record_goods_received.html',
                            controller: 'RecordGoodsReceivedCtrl'
                        }
                    },
                    data: {
                        access: CONSTANTS.AccessLevels.user,
                        screen_title: 'Record Goods Received'
                    }
                })
                .state('current-stock-level', {
                    url: '/current-stock-level/:fpoId',
                    parent: 'master',
                    views: {
                        'content@': {
                            templateUrl: '/fpoApp/views/fposhop/current_stock_level.html',
                            controller: 'CurrentStockLevelCtrl'
                        }
                    },
                    data: {
                        access: CONSTANTS.AccessLevels.user,
                        screen_title: 'Current Stock Level'
                    }
                })

                .state('finance-summary', {
                    url: '/finance-summary/:fpoId',
                    parent: 'master',
                    views: {
                        'content@': {
                            templateUrl: '/fpoApp/views/fposhop/finance_summary.html',
                            controller: 'FinanceSummaryCtrl'
                        }
                    },
                    data: {
                        access: CONSTANTS.AccessLevels.user,
                        screen_title: 'Current Stock Level'
                    }
                })
        }
    ])
