angular.module('fpoApp.routes')
.config(['$stateProvider', '$urlRouterProvider', 'CONSTANTS',
    function($stateProvider, $urlRouterProvider, CONSTANTS) {

            $stateProvider

            /* ----- User Registration ----- */
                .state('userList', {
                    url: '/userList',
                    parent: 'master',
                    params: { 'user': null },
                    views: {
                        'content@': {
                            templateUrl: '/fpoApp/views/user/user-list-fpo-wise.html',
                            controller: 'UserListCtrl'
                        }
                    },
                    data: {
                        access: CONSTANTS.AccessLevels.user,
                        screen_title: 'FPO Management'
                    }
                })
                .state('newRegister', {
                    url: '/new_registation',
                    views: {
                        'content@': {
                            templateUrl: '/fpoApp/views/user/user-registration.html',
                            controller: 'UserRegistrationCtrl'
                        }
                    },
                    data: {
                        access: CONSTANTS.AccessLevels.anon,
                        screen_title: 'User Registration'
                    }
                })
                .state('update-user-profile', {
                    url: '/update-user-profile/:user',
                    parent: 'master',
                    params: { user: null },
                    views: {
                        'content@': {
                            templateUrl: '/fpoApp/views/user/update-user-profile.html',
                            controller: 'UpdateUserProfileCtrl'
                        }
                    },
                    data: {
                        access: CONSTANTS.AccessLevels.user,
                        screen_title: 'Update User Profile'
                    }
                })

	            .state('add-user', {
	                url: '/add-user/:role',
	                parent: 'master',
	                views: {
	                    'content@': {
	                        templateUrl: '/fpoApp/views/user/inviteUser.html',
	                        controller: 'InviteUserCtrl'
	                    }
	                },
	                data: {
	                    access: CONSTANTS.AccessLevels.user,
	                    screen_title: 'Invite FPO'
	                }
	            })


	            // .state('userAddSuccess', {
	            //     url: '/userAddSuccess',
	            //     parent: 'master',
	            //     views: {
	            //         'content@': {
	            //             templateUrl: '/fpoApp/views/user/user-list.html',
	            //             controller: 'UserListCtrl',
	            //             template: '<span style="color: green;">Registered Successfully!</span>'
	            //         }
	            //     },
	            //     data: {
	            //         access: CONSTANTS.AccessLevels.user
	            //     }
	            // })

    }])
