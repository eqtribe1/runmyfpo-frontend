(function () { 
 return angular.module("fpoApp.env", [])
.constant("CONSTANTS", {"API_URL":"http://localhost:1337","backendUrl":"http://localhost:1337","ports":"3001","AccessLevels":{"anon":"FPO-FIELD","user":"FPO-MGMT","admin":"ADMIN"}});

})();
